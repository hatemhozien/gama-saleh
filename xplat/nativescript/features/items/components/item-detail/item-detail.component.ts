import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// libs
import { LogService } from '@malomatia/core';
import { ItemService, ItemDetailBaseComponent } from '@malomatia/features';
import { AppService } from '@malomatia/nativescript/core/services/app.service';

@Component({
  moduleId: module.id,
  selector: 'lib-item-detail',
  templateUrl: './item-detail.component.html'
})
export class ItemDetailComponent extends ItemDetailBaseComponent {
  constructor(
    log: LogService,
    itemService: ItemService,
    route: ActivatedRoute,
    public appService: AppService
  ) {
    super(log, itemService, route);
  }
}
