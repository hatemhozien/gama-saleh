import { Component } from '@angular/core';

// libs
import { LogService } from '@malomatia/core';
import { ItemsBaseComponent, ItemService } from '@malomatia/features';
import { AppService } from '@malomatia/nativescript/core';

@Component({
  moduleId: module.id,
  selector: 'lib-items',
  templateUrl: './items.component.html'
})
export class ItemsComponent extends ItemsBaseComponent {
  constructor(
    log: LogService,
    itemService: ItemService,
    public appService: AppService
  ) {
    super(log, itemService);
  }
}
