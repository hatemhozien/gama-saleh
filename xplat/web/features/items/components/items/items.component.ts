import { Component, OnInit } from '@angular/core';

// libs
import { LogService } from '@malomatia/core';
import { ItemsBaseComponent, ItemService } from '@malomatia/features';

@Component({
  selector: 'lib-items',
  templateUrl: './items.component.html'
})
export class ItemsComponent extends ItemsBaseComponent {
  constructor(log: LogService, itemService: ItemService) {
    super(log, itemService);
  }
}
