import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// libs
import { LogService } from '@malomatia/core';
import { ItemService, ItemDetailBaseComponent } from '@malomatia/features';

@Component({
  selector: 'lib-item-detail',
  templateUrl: './item-detail.component.html'
})
export class ItemDetailComponent extends ItemDetailBaseComponent {
  constructor(
    log: LogService,
    itemService: ItemService,
    route: ActivatedRoute
  ) {
    super(log, itemService, route);
  }
}
