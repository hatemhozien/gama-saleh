import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  Renderer2
} from '@angular/core';

@Directive({
  selector: '[libHighlight]'
})
export class HighlightDirective {
  constructor(private renderer: Renderer2, private el: ElementRef) {}

  @Input() defaultColor: string;
  @Input('libHighlight') highlightColor: string;

  @HostListener('mouseenter')
  onMouseEnter() {
    this.highlight(this.highlightColor || this.defaultColor || 'yellow');
  }
  @HostListener('mouseleave')
  onMouseLeave() {
    this.highlight(null);
  }

  private highlight(color: string): void {
    this.renderer.setStyle(this.el.nativeElement, 'backgroundColor', color);
  }
}
