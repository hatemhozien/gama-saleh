import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// libs
import { UISharedModule } from '@malomatia/features';
import { UI_DIRECTIVES } from './directives';

const MODULES = [
  CommonModule,
  RouterModule,
  FormsModule,
  ReactiveFormsModule,
  UISharedModule
];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES, ...UI_DIRECTIVES],
  declarations: [...UI_DIRECTIVES]
})
export class UIModule {}
