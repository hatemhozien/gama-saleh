import { Component } from '@angular/core';

import { HeaderBaseComponent } from '@malomatia/features';

@Component({
  selector: 'lib-ion-header',
  templateUrl: 'header.component.html'
})
export class HeaderComponent extends HeaderBaseComponent {}
