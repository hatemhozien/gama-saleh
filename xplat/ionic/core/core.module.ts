import { NgModule, Optional, SkipSelf } from '@angular/core';

import { IonicModule } from '@ionic/angular';
import { throwIfAlreadyLoaded } from '@malomatia/utils';
import { LibCoreModule } from '@malomatia/web';

@NgModule({
  imports: [LibCoreModule, IonicModule.forRoot()]
})
export class LibIonicCoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: LibIonicCoreModule
  ) {
    throwIfAlreadyLoaded(parentModule, 'LibIonicCoreModule');
  }
}
