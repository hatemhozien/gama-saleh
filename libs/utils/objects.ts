export const isString = function(arg: any) {
  return typeof arg === 'string';
};

export const isObject = function(arg: any) {
  return arg && typeof arg === 'object';
};

export const isNumber = function(arg: any) {
  return typeof arg === 'number';
};

export const formatNumber = function(arg: number) {
  return Math.round(arg * 100) / 100;
};
