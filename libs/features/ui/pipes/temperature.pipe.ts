import { Pipe, PipeTransform } from '@angular/core';

import { formatNumber, isNumber, invalidPipeArgumentError } from '@malomatia/utils';

const DEGREESYMBOL = '\u00B0';
const UNITS = ['C', 'F', 'K'];
const UNITS_LONG = ['Celsius', 'Fahrenheit', 'Kelvin'];

@Pipe({
  name: 'temperature'
})
export class TemperaturePipe implements PipeTransform {
  transform(
    value: number,
    unitFrom: string,
    unitTo: string,
    hasLongFormat: boolean
  ): string {
    if (isNumber(value)) {
      unitFrom = unitFrom.toUpperCase();
      unitTo = unitTo.toUpperCase();
      const index = UNITS.indexOf(unitTo);
      const unit = (hasLongFormat ? UNITS_LONG[index] : UNITS[index]) || unitTo;

      if (unitFrom !== unitTo) {
        if (unitFrom === 'C') {
          switch (unitTo) {
            case 'F':
              value = convertCelsiusToFahrenheit(value);
              break;

            case 'K':
              value = convertCelsiusToKelvin(value);
              break;

            default:
              throw invalidPipeArgumentError(TemperaturePipe, unitTo);
          }
        } else if (unitFrom === 'F') {
          switch (unitTo) {
            case 'C':
              value = convertFahrenheitToCelsius(value);
              break;

            case 'K':
              value = convertFahrenheitToKelvin(value);
              break;

            default:
              throw invalidPipeArgumentError(TemperaturePipe, unitTo);
          }
        } else if (unitFrom === 'K') {
          switch (unitTo) {
            case 'C':
              value = convertKelvinToCelsius(value);
              break;

            case 'F':
              value = convertKelvinToFahrenheit(value);
              break;

            default:
              throw invalidPipeArgumentError(TemperaturePipe, unitTo);
          }
        } else {
          throw invalidPipeArgumentError(TemperaturePipe, unitFrom);
        }
      }
      return `${formatNumber(value)} ${DEGREESYMBOL}${unit}`;
    }
    return;
  }
}

function convertCelsiusToFahrenheit(value: number) {
  return value * (9 / 5) + 32;
}
function convertFahrenheitToCelsius(value: number) {
  return (value - 32) * (5 / 9);
}
function convertCelsiusToKelvin(value: number) {
  return value + 273.15;
}
function convertKelvinToCelsius(value: number) {
  return value - 273.15;
}
function convertFahrenheitToKelvin(value: number) {
  return convertCelsiusToKelvin(convertFahrenheitToCelsius(value));
}
function convertKelvinToFahrenheit(value: number) {
  return convertCelsiusToFahrenheit(convertKelvinToCelsius(value));
}
