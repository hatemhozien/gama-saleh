import { Pipe, PipeTransform } from '@angular/core';
import { formatNumber } from '@malomatia/utils';

const { floor, log, min, pow, round } = Math;

const UNITS = ['B', 'KB', 'MB', 'GB', 'TB'];
const UNITS_LONG = [
  'Bytes',
  'Kilobytes',
  'Megabytes',
  'Gigabytes',
  'Terabytes'
];

@Pipe({
  name: 'filesize'
})
export class FilesizePipe implements PipeTransform {
  transform(value: any, hasLongFormat: boolean): string {
    const power = floor(log(value) / log(1024));
    const index = min(power, UNITS.length - 1);
    const unit = hasLongFormat ? UNITS_LONG[index] : UNITS[index];
    const size = value / pow(1024, index);
    const formattedSize = formatNumber(size);

    return `${formattedSize} ${unit}`;
  }
}
