export * from './default.pipe';
export * from './filesize.pipe';
export * from './temperature.pipe';

import { DateOrderPipe } from './date-order.pipe';
import { DefaultPipe } from './default.pipe';
import { FilesizePipe } from './filesize.pipe';
import { TemperaturePipe } from './temperature.pipe';

export const UI_PIPES = [
  DateOrderPipe,
  DefaultPipe,
  FilesizePipe,
  TemperaturePipe
];
