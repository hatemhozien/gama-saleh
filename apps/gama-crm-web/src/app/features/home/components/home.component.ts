import { Component } from '@angular/core';

import { BaseComponent } from '@malomatia/core';

@Component({
  selector: 'lib-home',
  templateUrl: 'home.component.html'
})
export class HomeComponent extends BaseComponent {}
