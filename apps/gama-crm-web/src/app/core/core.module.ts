import { NgModule } from '@angular/core';

// xplat
import { LibCoreModule } from '@malomatia/web';

@NgModule({
  imports: [LibCoreModule]
})
export class CoreModule {}
