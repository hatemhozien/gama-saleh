module.exports = {
  name: 'gama-crm-web',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/gama-crm-web/',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
