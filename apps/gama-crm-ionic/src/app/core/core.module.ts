import { NgModule } from '@angular/core';

// libs
import { LibIonicCoreModule } from '@malomatia/ionic';

@NgModule({
  imports: [LibIonicCoreModule]
})
export class CoreModule {}
