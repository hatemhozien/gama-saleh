import { Component, Input } from '@angular/core';

import { BaseComponent } from '@malomatia/core';

@Component({
  selector: 'lib-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.scss']
})
export class HeaderComponent extends BaseComponent {
  constructor() {
    super();
  }
  @Input() title: string;
}
