import { NgModule } from '@angular/core';

// xplat
import { UIModule as IonicUIModule } from '@malomatia/ionic';

import { UI_COMPONENTS } from './components';


const MODULES = [IonicUIModule];

@NgModule({
  declarations: [UI_COMPONENTS],
  imports: [...MODULES],
  exports: [...MODULES, UI_COMPONENTS]
})
export class SharedModule {}
