import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { Event } from '@angular/router';

import { BaseComponent } from '@malomatia/core';

// TODO: Move to gama-crm shared lib
interface ReactiveSchemaForm {
  [key: string]: any;
}
const loginSchema: ReactiveSchemaForm = {
  username: ['', [Validators.required, Validators.minLength(3)]],
  password: ['', [Validators.required, Validators.minLength(8)]]
};

@Component({
  selector: 'page-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage extends BaseComponent implements OnInit {
  LoginForm: FormGroup;
  // TODO: add reacted to change validation
  message: string;

  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.LoginForm = this.fb.group(loginSchema);
  }

  onFocus(event: Event) {
    // console.log(event)
  }
}
