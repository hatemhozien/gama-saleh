import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EservicesPage } from './eservices.page';

describe('EservicesPage', () => {
  let component: EservicesPage;
  let fixture: ComponentFixture<EservicesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EservicesPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EservicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
