import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HeritageDeliveryPage } from './heritage-delivery.page';

const routes: Routes = [
  {
    path: '',
    component: HeritageDeliveryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeritageDeliveryRoutingModule {}
