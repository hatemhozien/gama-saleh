import { Component } from '@angular/core';
import { BaseComponent } from '@malomatia/core';

@Component({
  selector: 'page-heritage-delivery',
  templateUrl: './heritage-delivery.page.html',
  styleUrls: ['./heritage-delivery.page.scss']
})
export class HeritageDeliveryPage extends BaseComponent {}
