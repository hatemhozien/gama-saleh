import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { SharedModule } from '../../../features/shared/shared.module';
import { HeritageDeliveryRoutingModule } from './heritage-delivery-routing.module';
import { HeritageDeliveryPage } from './heritage-delivery.page';

@NgModule({
  imports: [SharedModule, HeritageDeliveryRoutingModule],
  declarations: [HeritageDeliveryPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HeritageDeliveryModule {}
