import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SharedModule } from '../../features/shared/shared.module';
import { EservicesRoutingModule } from './eservices-routing.module';
import { EservicesPage } from './eservices.page';

@NgModule({
  imports: [SharedModule, EservicesRoutingModule],
  declarations: [EservicesPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EservicesModule {}
