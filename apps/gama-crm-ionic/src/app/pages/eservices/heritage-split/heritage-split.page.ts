import { Component } from '@angular/core';
import { BaseComponent } from '@malomatia/core';

@Component({
  selector: 'page-heritage-split',
  templateUrl: './heritage-split.page.html',
  styleUrls: ['./heritage-split.page.scss']
})
export class HeritageSplitPage extends BaseComponent {}
