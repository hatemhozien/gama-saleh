import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeritageSplitPage } from './heritage-split.page';

describe('HeritageSplitPage', () => {
  let component: HeritageSplitPage;
  let fixture: ComponentFixture<HeritageSplitPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeritageSplitPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeritageSplitPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
