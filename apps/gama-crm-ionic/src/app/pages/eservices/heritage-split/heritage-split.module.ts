import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SharedModule } from '../../../features/shared/shared.module';
import { HeritageSplitRoutingModule } from './heritage-split-routing.module';
import { HeritageSplitPage } from './heritage-split.page';

@NgModule({
  imports: [SharedModule, HeritageSplitRoutingModule],
  declarations: [HeritageSplitPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HeritageSplitModule {}
