import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HeritageSplitPage } from './heritage-split.page';

const routes: Routes = [
  {
    path: '',
    component: HeritageSplitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeritageSplitRoutingModule {}
