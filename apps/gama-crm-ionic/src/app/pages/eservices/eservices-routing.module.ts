import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EservicesPage } from './eservices.page';

export const EservicesRoutes: Routes = [
  {
    path: '',
    component: EservicesPage,
    children: [
      {
        path: 'heritage-split',
        loadChildren:
          './heritage-split/heritage-split.module#HeritageSplitModule'
      },
      {
        path: 'heritage-delivery',
        loadChildren:
          './heritage-delivery/heritage-delivery.module#HeritageDeliveryModule'
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(EservicesRoutes)],
  exports: [RouterModule]
})
export class EservicesRoutingModule {}
