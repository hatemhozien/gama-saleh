// TODO: move to shared gama-crm lib
const root = 'eservices';

export interface Eservice {
  name: string;
  route: string;
}

export const eservices: Eservice[] = [
  {
    name: 'Heritage Split',
    route: `/${root}/heritage-split`
  },
  {
    name: 'Delegation Renewal/Management',
    route: `/${root}/delegation-management`
  },
  {
    name: 'Monthly Expenditures',
    route: `/${root}/monthly-expenditures`
  },
  {
    name: 'Selling assets',
    route: `/${root}/selling-assets`
  },
  {
    name: 'Investment buying',
    route: `/${root}/investment-buying`
  },
  {
    name: 'Expenses',
    route: `/${root}/expenses`
  },
  {
    name: 'Properties maintenance',
    route: `/${root}/properties-maintenance`
  },
  {
    name: 'Buy/Develop a Residence',
    route: `/${root}/develop-residence`
  },
  {
    name: 'Properties Rental',
    route: `/${root}/properties-rental`
  },
  {
    name: 'Update File',
    route: `/${root}/update-file`
  },
  {
    name: 'Complains',
    route: `/${root}/complains`
  },
  {
    name: 'Disclosure of a Movable Heritage',
    route: `/${root}/heritage-disclosure`
  },
  {
    name: 'Delivery of Movable Heritage',
    route: `/${root}/heritage-delivery`
  }
];
