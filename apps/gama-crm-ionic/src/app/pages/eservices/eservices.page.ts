import { Component } from '@angular/core';
import { BaseComponent } from '@malomatia/core';

import { eservices, Eservice } from './eservices';

@Component({
  selector: 'page-eservices',
  templateUrl: './eservices.page.html',
  styleUrls: ['./eservices.page.scss']
})
export class EservicesPage extends BaseComponent {
  eservices: Eservice[] = eservices;
}
