// TODO: separate interface
// TODO: Modve to shared gama-crm lib
export interface Contact {
  name: string;
  description: string;
  icon: string;
}

export const contact: Contact[] = [
  {
    name: 'Authority address',
    description: 'this is a simple description',
    icon: 'location'
  },
  {
    name: 'Phone number',
    description: '0101010101',
    icon: 'phone'
  },
  {
    name: 'Fax number',
    description: '010010101010',
    icon: 'fax'
  }
];
