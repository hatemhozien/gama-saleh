import { Component } from '@angular/core';
import { BaseComponent } from '@malomatia/core';

import { contact, Contact } from './contact';

@Component({
  selector: 'page-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss']
})
export class ContactPage extends BaseComponent {
  items: Contact[] = contact;

  ionViewDidEnter() {
    this.clearFooterBackground();
  }

  private clearFooterBackground() {
    // TOOD: Move to xplat ui service
    const toolbars = document.querySelectorAll('ion-toolbar');
    const footerToolbar = toolbars[toolbars.length - 1];
    footerToolbar.shadowRoot.querySelector('.toolbar-background').remove();
  }
}
