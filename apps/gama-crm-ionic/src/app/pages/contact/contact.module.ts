import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SharedModule } from '../../features/shared/shared.module';
import { ContactRoutingModule } from './contact-routing.module';
import { ContactPage } from './contact.page';

@NgModule({
  imports: [SharedModule, ContactRoutingModule],
  declarations: [ContactPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ContactPageModule {}
