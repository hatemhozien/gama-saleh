import { Component } from '@angular/core';
import { BaseComponent } from '@malomatia/core';

@Component({
  selector: 'page-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage extends BaseComponent {}
