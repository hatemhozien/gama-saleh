(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["styles"],{

/***/ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/global.scss":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src??embedded!./node_modules/sass-loader/lib/loader.js??ref--14-3!./src/global.scss ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = [[module.i, "/**\n * The following are shared across all platforms and apps\n */\n/**\n * Shared across all platforms and apps\n * You may define a set of global variables accessible across entire workspace here\n */\n/**\n * The following are web specific (used with any web app targets)\n */\n/**\n * Convenient spacing classes\n */\n/**\n * Margin and Padding\n * The following creates this pattern:\n * .m-0{margin:0}.m-t-0{margin-top:0}.m-r-0{margin-right:0}.m-b-0{margin-bottom:0}.m-l-0{margin-left:0}.m-x-0{margin-right:0;margin-left:0}.m-y-0{margin-top:0;margin-bottom:0}\n * Same for Padding (using the 'p' abbreviation)\n * From 0, 2, 5, 10, 15, 20, 25, 30\n**/\nhtml.ios{--ion-default-font: -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", \"Roboto\", sans-serif}\nhtml.md{--ion-default-font: \"Roboto\", \"Helvetica Neue\", sans-serif}\nhtml{--ion-font-family: var(--ion-default-font)}\nbody{background:var(--ion-background-color)}\nbody.backdrop-no-scroll{overflow:hidden}\n.ion-color-primary{--ion-color-base: var(--ion-color-primary, #3880ff) !important;--ion-color-base-rgb: var(--ion-color-primary-rgb, 56,128,255) !important;--ion-color-contrast: var(--ion-color-primary-contrast, #fff) !important;--ion-color-contrast-rgb: var(--ion-color-primary-contrast-rgb, 255,255,255) !important;--ion-color-shade: var(--ion-color-primary-shade, #3171e0) !important;--ion-color-tint: var(--ion-color-primary-tint, #4c8dff) !important}\n.ion-color-secondary{--ion-color-base: var(--ion-color-secondary, #0cd1e8) !important;--ion-color-base-rgb: var(--ion-color-secondary-rgb, 12,209,232) !important;--ion-color-contrast: var(--ion-color-secondary-contrast, #fff) !important;--ion-color-contrast-rgb: var(--ion-color-secondary-contrast-rgb, 255,255,255) !important;--ion-color-shade: var(--ion-color-secondary-shade, #0bb8cc) !important;--ion-color-tint: var(--ion-color-secondary-tint, #24d6ea) !important}\n.ion-color-tertiary{--ion-color-base: var(--ion-color-tertiary, #7044ff) !important;--ion-color-base-rgb: var(--ion-color-tertiary-rgb, 112,68,255) !important;--ion-color-contrast: var(--ion-color-tertiary-contrast, #fff) !important;--ion-color-contrast-rgb: var(--ion-color-tertiary-contrast-rgb, 255,255,255) !important;--ion-color-shade: var(--ion-color-tertiary-shade, #633ce0) !important;--ion-color-tint: var(--ion-color-tertiary-tint, #7e57ff) !important}\n.ion-color-success{--ion-color-base: var(--ion-color-success, #10dc60) !important;--ion-color-base-rgb: var(--ion-color-success-rgb, 16,220,96) !important;--ion-color-contrast: var(--ion-color-success-contrast, #fff) !important;--ion-color-contrast-rgb: var(--ion-color-success-contrast-rgb, 255,255,255) !important;--ion-color-shade: var(--ion-color-success-shade, #0ec254) !important;--ion-color-tint: var(--ion-color-success-tint, #28e070) !important}\n.ion-color-warning{--ion-color-base: var(--ion-color-warning, #ffce00) !important;--ion-color-base-rgb: var(--ion-color-warning-rgb, 255,206,0) !important;--ion-color-contrast: var(--ion-color-warning-contrast, #fff) !important;--ion-color-contrast-rgb: var(--ion-color-warning-contrast-rgb, 255,255,255) !important;--ion-color-shade: var(--ion-color-warning-shade, #e0b500) !important;--ion-color-tint: var(--ion-color-warning-tint, #ffd31a) !important}\n.ion-color-danger{--ion-color-base: var(--ion-color-danger, #f04141) !important;--ion-color-base-rgb: var(--ion-color-danger-rgb, 240,65,65) !important;--ion-color-contrast: var(--ion-color-danger-contrast, #fff) !important;--ion-color-contrast-rgb: var(--ion-color-danger-contrast-rgb, 255,255,255) !important;--ion-color-shade: var(--ion-color-danger-shade, #d33939) !important;--ion-color-tint: var(--ion-color-danger-tint, #f25454) !important}\n.ion-color-light{--ion-color-base: var(--ion-color-light, #f4f5f8) !important;--ion-color-base-rgb: var(--ion-color-light-rgb, 244,245,248) !important;--ion-color-contrast: var(--ion-color-light-contrast, #000) !important;--ion-color-contrast-rgb: var(--ion-color-light-contrast-rgb, 0,0,0) !important;--ion-color-shade: var(--ion-color-light-shade, #d7d8da) !important;--ion-color-tint: var(--ion-color-light-tint, #f5f6f9) !important}\n.ion-color-medium{--ion-color-base: var(--ion-color-medium, #989aa2) !important;--ion-color-base-rgb: var(--ion-color-medium-rgb, 152,154,162) !important;--ion-color-contrast: var(--ion-color-medium-contrast, #fff) !important;--ion-color-contrast-rgb: var(--ion-color-medium-contrast-rgb, 255,255,255) !important;--ion-color-shade: var(--ion-color-medium-shade, #86888f) !important;--ion-color-tint: var(--ion-color-medium-tint, #a2a4ab) !important}\n.ion-color-dark{--ion-color-base: var(--ion-color-dark, #222428) !important;--ion-color-base-rgb: var(--ion-color-dark-rgb, 34,36,40) !important;--ion-color-contrast: var(--ion-color-dark-contrast, #fff) !important;--ion-color-contrast-rgb: var(--ion-color-dark-contrast-rgb, 255,255,255) !important;--ion-color-shade: var(--ion-color-dark-shade, #1e2023) !important;--ion-color-tint: var(--ion-color-dark-tint, #383a3e) !important}\n.ion-page{left:0;right:0;top:0;bottom:0;display:flex;position:absolute;flex-direction:column;justify-content:space-between;contain:layout size style;overflow:hidden;z-index:0}\nion-route,ion-route-redirect,ion-router,ion-select-option,ion-nav-controller,ion-menu-controller,ion-action-sheet-controller,ion-alert-controller,ion-loading-controller,ion-modal-controller,ion-picker-controller,ion-popover-controller,ion-toast-controller,.ion-page-hidden,[hidden]{display:none !important}\n.ion-page-invisible{opacity:0}\nhtml.plt-ios.plt-hybrid,html.plt-ios.plt-pwa{--ion-statusbar-padding: 20px}\n@supports (padding-top: 20px){html{--ion-safe-area-top: var(--ion-statusbar-padding)}}\n@supports (padding-top: constant(safe-area-inset-top)){html{--ion-safe-area-top: constant(safe-area-inset-top);--ion-safe-area-bottom: constant(safe-area-inset-bottom);--ion-safe-area-left: constant(safe-area-inset-left);--ion-safe-area-right: constant(safe-area-inset-right)}}\n@supports (padding-top: env(safe-area-inset-top)){html{--ion-safe-area-top: env(safe-area-inset-top);--ion-safe-area-bottom: env(safe-area-inset-bottom);--ion-safe-area-left: env(safe-area-inset-left);--ion-safe-area-right: env(safe-area-inset-right)}}\naudio,canvas,progress,video{vertical-align:baseline}\naudio:not([controls]){display:none;height:0}\nb,strong{font-weight:bold}\nimg{max-width:100%;border:0}\nsvg:not(:root){overflow:hidden}\nfigure{margin:1em 40px}\nhr{height:1px;border-width:0;box-sizing:content-box}\npre{overflow:auto}\ncode,kbd,pre,samp{font-family:monospace, monospace;font-size:1em}\nlabel,input,select,textarea{font-family:inherit;line-height:normal}\ntextarea{overflow:auto;height:auto;font:inherit;color:inherit}\ntextarea::-webkit-input-placeholder{padding-left:2px}\ntextarea:-ms-input-placeholder{padding-left:2px}\ntextarea::-ms-input-placeholder{padding-left:2px}\ntextarea::placeholder{padding-left:2px}\nform,input,optgroup,select{margin:0;font:inherit;color:inherit}\nhtml input[type=\"button\"],input[type=\"reset\"],input[type=\"submit\"]{cursor:pointer;-webkit-appearance:button}\na,a div,a span,a ion-icon,a ion-label,button,button div,button span,button ion-icon,button ion-label,.ion-tappable,[tappable],[tappable] div,[tappable] span,[tappable] ion-icon,[tappable] ion-label,input,textarea{touch-action:manipulation}\na ion-label,button ion-label{pointer-events:none}\nbutton{border:0;border-radius:0;font-family:inherit;font-style:inherit;font-variant:inherit;line-height:1;text-transform:none;cursor:pointer;-webkit-appearance:button}\n[tappable]{cursor:pointer}\na[disabled],button[disabled],html input[disabled]{cursor:default}\nbutton::-moz-focus-inner,input::-moz-focus-inner{padding:0;border:0}\ninput[type=\"checkbox\"],input[type=\"radio\"]{padding:0;box-sizing:border-box}\ninput[type=\"number\"]::-webkit-inner-spin-button,input[type=\"number\"]::-webkit-outer-spin-button{height:auto}\ninput[type=\"search\"]::-webkit-search-cancel-button,input[type=\"search\"]::-webkit-search-decoration{-webkit-appearance:none}\ntable{border-collapse:collapse;border-spacing:0}\ntd,th{padding:0}\n*{box-sizing:border-box;-webkit-tap-highlight-color:rgba(0,0,0,0);-webkit-tap-highlight-color:transparent;-webkit-touch-callout:none}\nhtml{width:100%;height:100%;-webkit-text-size-adjust:100%;-moz-text-size-adjust:100%;-ms-text-size-adjust:100%;text-size-adjust:100%}\nhtml.plt-pwa{height:100vh}\nbody{-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;margin-left:0;margin-right:0;margin-top:0;margin-bottom:0;padding-left:0;padding-right:0;padding-top:0;padding-bottom:0;position:fixed;width:100%;max-width:100%;height:100%;max-height:100%;text-rendering:optimizeLegibility;overflow:hidden;touch-action:manipulation;-webkit-user-drag:none;-ms-content-zooming:none;word-wrap:break-word;overscroll-behavior-y:none;-webkit-text-size-adjust:none;-moz-text-size-adjust:none;-ms-text-size-adjust:none;text-size-adjust:none}\nhtml{font-family:var(--ion-font-family)}\na{background-color:transparent;color:var(--ion-color-primary, #3880ff)}\nh1,h2,h3,h4,h5,h6{margin-top:16px;margin-bottom:10px;font-weight:500;line-height:1.2}\nh1{margin-top:20px;font-size:26px}\nh2{margin-top:18px;font-size:24px}\nh3{font-size:22px}\nh4{font-size:20px}\nh5{font-size:18px}\nh6{font-size:16px}\nsmall{font-size:75%}\nsub,sup{position:relative;font-size:75%;line-height:0;vertical-align:baseline}\nsup{top:-.5em}\nsub{bottom:-.25em}\n.ion-no-padding,[no-padding]{--padding-start: 0;--padding-end: 0;--padding-top: 0;--padding-bottom: 0;padding-left:0;padding-right:0;padding-top:0;padding-bottom:0}\n.ion-padding,[padding]{--padding-start: var(--ion-padding, 16px);--padding-end: var(--ion-padding, 16px);--padding-top: var(--ion-padding, 16px);--padding-bottom: var(--ion-padding, 16px);padding-left:var(--ion-padding, 16px);padding-right:var(--ion-padding, 16px);padding-top:var(--ion-padding, 16px);padding-bottom:var(--ion-padding, 16px)}\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0){.ion-padding,[padding]{padding-left:unset;padding-right:unset;-webkit-padding-start:var(--ion-padding, 16px);padding-inline-start:var(--ion-padding, 16px);-webkit-padding-end:var(--ion-padding, 16px);padding-inline-end:var(--ion-padding, 16px)}}\n.ion-padding-top,[padding-top]{--padding-top: var(--ion-padding, 16px);padding-top:var(--ion-padding, 16px)}\n.ion-padding-start,[padding-start]{--padding-start: var(--ion-padding, 16px);padding-left:var(--ion-padding, 16px)}\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0){.ion-padding-start,[padding-start]{padding-left:unset;-webkit-padding-start:var(--ion-padding, 16px);padding-inline-start:var(--ion-padding, 16px)}}\n.ion-padding-end,[padding-end]{--padding-end: var(--ion-padding, 16px);padding-right:var(--ion-padding, 16px)}\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0){.ion-padding-end,[padding-end]{padding-right:unset;-webkit-padding-end:var(--ion-padding, 16px);padding-inline-end:var(--ion-padding, 16px)}}\n.ion-padding-bottom,[padding-bottom]{--padding-bottom: var(--ion-padding, 16px);padding-bottom:var(--ion-padding, 16px)}\n.ion-padding-vertical,[padding-vertical]{--padding-top: var(--ion-padding, 16px);--padding-bottom: var(--ion-padding, 16px);padding-top:var(--ion-padding, 16px);padding-bottom:var(--ion-padding, 16px)}\n.ion-padding-horizontal,[padding-horizontal]{--padding-start: var(--ion-padding, 16px);--padding-end: var(--ion-padding, 16px);padding-left:var(--ion-padding, 16px);padding-right:var(--ion-padding, 16px)}\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0){.ion-padding-horizontal,[padding-horizontal]{padding-left:unset;padding-right:unset;-webkit-padding-start:var(--ion-padding, 16px);padding-inline-start:var(--ion-padding, 16px);-webkit-padding-end:var(--ion-padding, 16px);padding-inline-end:var(--ion-padding, 16px)}}\n.ion-no-margin,[no-margin]{--margin-start: 0;--margin-end: 0;--margin-top: 0;--margin-bottom: 0;margin-left:0;margin-right:0;margin-top:0;margin-bottom:0}\n.ion-margin,[margin]{--margin-start: var(--ion-margin, 16px);--margin-end: var(--ion-margin, 16px);--margin-top: var(--ion-margin, 16px);--margin-bottom: var(--ion-margin, 16px);margin-left:var(--ion-margin, 16px);margin-right:var(--ion-margin, 16px);margin-top:var(--ion-margin, 16px);margin-bottom:var(--ion-margin, 16px)}\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0){.ion-margin,[margin]{margin-left:unset;margin-right:unset;-webkit-margin-start:var(--ion-margin, 16px);margin-inline-start:var(--ion-margin, 16px);-webkit-margin-end:var(--ion-margin, 16px);margin-inline-end:var(--ion-margin, 16px)}}\n.ion-margin-top,[margin-top]{--margin-top: var(--ion-margin, 16px);margin-top:var(--ion-margin, 16px)}\n.ion-margin-start,[margin-start]{--margin-start: var(--ion-margin, 16px);margin-left:var(--ion-margin, 16px)}\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0){.ion-margin-start,[margin-start]{margin-left:unset;-webkit-margin-start:var(--ion-margin, 16px);margin-inline-start:var(--ion-margin, 16px)}}\n.ion-margin-end,[margin-end]{--margin-end: var(--ion-margin, 16px);margin-right:var(--ion-margin, 16px)}\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0){.ion-margin-end,[margin-end]{margin-right:unset;-webkit-margin-end:var(--ion-margin, 16px);margin-inline-end:var(--ion-margin, 16px)}}\n.ion-margin-bottom,[margin-bottom]{--margin-bottom: var(--ion-margin, 16px);margin-bottom:var(--ion-margin, 16px)}\n.ion-margin-vertical,[margin-vertical]{--margin-top: var(--ion-margin, 16px);--margin-bottom: var(--ion-margin, 16px);margin-top:var(--ion-margin, 16px);margin-bottom:var(--ion-margin, 16px)}\n.ion-margin-horizontal,[margin-horizontal]{--margin-start: var(--ion-margin, 16px);--margin-end: var(--ion-margin, 16px);margin-left:var(--ion-margin, 16px);margin-right:var(--ion-margin, 16px)}\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0){.ion-margin-horizontal,[margin-horizontal]{margin-left:unset;margin-right:unset;-webkit-margin-start:var(--ion-margin, 16px);margin-inline-start:var(--ion-margin, 16px);-webkit-margin-end:var(--ion-margin, 16px);margin-inline-end:var(--ion-margin, 16px)}}\n.ion-float-left,[float-left]{float:left !important}\n.ion-float-right,[float-right]{float:right !important}\n.ion-float-start,[float-start]{float:left !important}\n:host-context([dir=rtl]) .ion-float-start,:host-context([dir=rtl]) [float-start]{float:right !important}\n.ion-float-end,[float-end]{float:right !important}\n:host-context([dir=rtl]) .ion-float-end,:host-context([dir=rtl]) [float-end]{float:left !important}\n@media (min-width: 576px){.ion-float-sm-left,[float-sm-left]{float:left !important}.ion-float-sm-right,[float-sm-right]{float:right !important}.ion-float-sm-start,[float-sm-start]{float:left !important}:host-context([dir=rtl]) .ion-float-sm-start,:host-context([dir=rtl]) [float-sm-start]{float:right !important}.ion-float-sm-end,[float-sm-end]{float:right !important}:host-context([dir=rtl]) .ion-float-sm-end,:host-context([dir=rtl]) [float-sm-end]{float:left !important}}\n@media (min-width: 768px){.ion-float-md-left,[float-md-left]{float:left !important}.ion-float-md-right,[float-md-right]{float:right !important}.ion-float-md-start,[float-md-start]{float:left !important}:host-context([dir=rtl]) .ion-float-md-start,:host-context([dir=rtl]) [float-md-start]{float:right !important}.ion-float-md-end,[float-md-end]{float:right !important}:host-context([dir=rtl]) .ion-float-md-end,:host-context([dir=rtl]) [float-md-end]{float:left !important}}\n@media (min-width: 992px){.ion-float-lg-left,[float-lg-left]{float:left !important}.ion-float-lg-right,[float-lg-right]{float:right !important}.ion-float-lg-start,[float-lg-start]{float:left !important}:host-context([dir=rtl]) .ion-float-lg-start,:host-context([dir=rtl]) [float-lg-start]{float:right !important}.ion-float-lg-end,[float-lg-end]{float:right !important}:host-context([dir=rtl]) .ion-float-lg-end,:host-context([dir=rtl]) [float-lg-end]{float:left !important}}\n@media (min-width: 1200px){.ion-float-xl-left,[float-xl-left]{float:left !important}.ion-float-xl-right,[float-xl-right]{float:right !important}.ion-float-xl-start,[float-xl-start]{float:left !important}:host-context([dir=rtl]) .ion-float-xl-start,:host-context([dir=rtl]) [float-xl-start]{float:right !important}.ion-float-xl-end,[float-xl-end]{float:right !important}:host-context([dir=rtl]) .ion-float-xl-end,:host-context([dir=rtl]) [float-xl-end]{float:left !important}}\n.ion-text-center,[text-center]{text-align:center !important}\n.ion-text-justify,[text-justify]{text-align:justify !important}\n.ion-text-start,[text-start]{text-align:start !important}\n.ion-text-end,[text-end]{text-align:end !important}\n.ion-text-left,[text-left]{text-align:left !important}\n.ion-text-right,[text-right]{text-align:right !important}\n.ion-text-nowrap,[text-nowrap]{white-space:nowrap !important}\n.ion-text-wrap,[text-wrap]{white-space:normal !important}\n@media (min-width: 576px){.ion-text-sm-center,[text-sm-center]{text-align:center !important}.ion-text-sm-justify,[text-sm-justify]{text-align:justify !important}.ion-text-sm-start,[text-sm-start]{text-align:start !important}.ion-text-sm-end,[text-sm-end]{text-align:end !important}.ion-text-sm-left,[text-sm-left]{text-align:left !important}.ion-text-sm-right,[text-sm-right]{text-align:right !important}.ion-text-sm-nowrap,[text-sm-nowrap]{white-space:nowrap !important}.ion-text-sm-wrap,[text-sm-wrap]{white-space:normal !important}}\n@media (min-width: 768px){.ion-text-md-center,[text-md-center]{text-align:center !important}.ion-text-md-justify,[text-md-justify]{text-align:justify !important}.ion-text-md-start,[text-md-start]{text-align:start !important}.ion-text-md-end,[text-md-end]{text-align:end !important}.ion-text-md-left,[text-md-left]{text-align:left !important}.ion-text-md-right,[text-md-right]{text-align:right !important}.ion-text-md-nowrap,[text-md-nowrap]{white-space:nowrap !important}.ion-text-md-wrap,[text-md-wrap]{white-space:normal !important}}\n@media (min-width: 992px){.ion-text-lg-center,[text-lg-center]{text-align:center !important}.ion-text-lg-justify,[text-lg-justify]{text-align:justify !important}.ion-text-lg-start,[text-lg-start]{text-align:start !important}.ion-text-lg-end,[text-lg-end]{text-align:end !important}.ion-text-lg-left,[text-lg-left]{text-align:left !important}.ion-text-lg-right,[text-lg-right]{text-align:right !important}.ion-text-lg-nowrap,[text-lg-nowrap]{white-space:nowrap !important}.ion-text-lg-wrap,[text-lg-wrap]{white-space:normal !important}}\n@media (min-width: 1200px){.ion-text-xl-center,[text-xl-center]{text-align:center !important}.ion-text-xl-justify,[text-xl-justify]{text-align:justify !important}.ion-text-xl-start,[text-xl-start]{text-align:start !important}.ion-text-xl-end,[text-xl-end]{text-align:end !important}.ion-text-xl-left,[text-xl-left]{text-align:left !important}.ion-text-xl-right,[text-xl-right]{text-align:right !important}.ion-text-xl-nowrap,[text-xl-nowrap]{white-space:nowrap !important}.ion-text-xl-wrap,[text-xl-wrap]{white-space:normal !important}}\n.ion-text-uppercase,[text-uppercase]{text-transform:uppercase !important}\n.ion-text-lowercase,[text-lowercase]{text-transform:lowercase !important}\n.ion-text-capitalize,[text-capitalize]{text-transform:capitalize !important}\n@media (min-width: 576px){.ion-text-sm-uppercase,[text-sm-uppercase]{text-transform:uppercase !important}.ion-text-sm-lowercase,[text-sm-lowercase]{text-transform:lowercase !important}.ion-text-sm-capitalize,[text-sm-capitalize]{text-transform:capitalize !important}}\n@media (min-width: 768px){.ion-text-md-uppercase,[text-md-uppercase]{text-transform:uppercase !important}.ion-text-md-lowercase,[text-md-lowercase]{text-transform:lowercase !important}.ion-text-md-capitalize,[text-md-capitalize]{text-transform:capitalize !important}}\n@media (min-width: 992px){.ion-text-lg-uppercase,[text-lg-uppercase]{text-transform:uppercase !important}.ion-text-lg-lowercase,[text-lg-lowercase]{text-transform:lowercase !important}.ion-text-lg-capitalize,[text-lg-capitalize]{text-transform:capitalize !important}}\n@media (min-width: 1200px){.ion-text-xl-uppercase,[text-xl-uppercase]{text-transform:uppercase !important}.ion-text-xl-lowercase,[text-xl-lowercase]{text-transform:lowercase !important}.ion-text-xl-capitalize,[text-xl-capitalize]{text-transform:capitalize !important}}\n.ion-align-self-start,[align-self-start]{align-self:flex-start !important}\n.ion-align-self-end,[align-self-end]{align-self:flex-end !important}\n.ion-align-self-center,[align-self-center]{align-self:center !important}\n.ion-align-self-stretch,[align-self-stretch]{align-self:stretch !important}\n.ion-align-self-baseline,[align-self-baseline]{align-self:baseline !important}\n.ion-align-self-auto,[align-self-auto]{align-self:auto !important}\n.ion-wrap,[wrap]{flex-wrap:wrap !important}\n.ion-nowrap,[nowrap]{flex-wrap:nowrap !important}\n.ion-wrap-reverse,[wrap-reverse]{flex-wrap:wrap-reverse !important}\n.ion-justify-content-start,[justify-content-start]{justify-content:flex-start !important}\n.ion-justify-content-center,[justify-content-center]{justify-content:center !important}\n.ion-justify-content-end,[justify-content-end]{justify-content:flex-end !important}\n.ion-justify-content-around,[justify-content-around]{justify-content:space-around !important}\n.ion-justify-content-between,[justify-content-between]{justify-content:space-between !important}\n.ion-justify-content-evenly,[justify-content-evenly]{justify-content:space-evenly !important}\n.ion-align-items-start,[align-items-start]{align-items:flex-start !important}\n.ion-align-items-center,[align-items-center]{align-items:center !important}\n.ion-align-items-end,[align-items-end]{align-items:flex-end !important}\n.ion-align-items-stretch,[align-items-stretch]{align-items:stretch !important}\n.ion-align-items-baseline,[align-items-baseline]{align-items:baseline !important}\n.m-0 {\n  margin: 0px; }\n.m-t-0 {\n  margin-top: 0px; }\n.m-r-0 {\n  margin-right: 0px; }\n.m-b-0 {\n  margin-bottom: 0px; }\n.m-l-0 {\n  margin-left: 0px; }\n.m-x-0 {\n  margin-right: 0px;\n  margin-left: 0px; }\n.m-y-0 {\n  margin-top: 0px;\n  margin-bottom: 0px; }\n.m-2 {\n  margin: 2px; }\n.m-t-2 {\n  margin-top: 2px; }\n.m-r-2 {\n  margin-right: 2px; }\n.m-b-2 {\n  margin-bottom: 2px; }\n.m-l-2 {\n  margin-left: 2px; }\n.m-x-2 {\n  margin-right: 2px;\n  margin-left: 2px; }\n.m-y-2 {\n  margin-top: 2px;\n  margin-bottom: 2px; }\n.m-4 {\n  margin: 4px; }\n.m-t-4 {\n  margin-top: 4px; }\n.m-r-4 {\n  margin-right: 4px; }\n.m-b-4 {\n  margin-bottom: 4px; }\n.m-l-4 {\n  margin-left: 4px; }\n.m-x-4 {\n  margin-right: 4px;\n  margin-left: 4px; }\n.m-y-4 {\n  margin-top: 4px;\n  margin-bottom: 4px; }\n.m-5 {\n  margin: 5px; }\n.m-t-5 {\n  margin-top: 5px; }\n.m-r-5 {\n  margin-right: 5px; }\n.m-b-5 {\n  margin-bottom: 5px; }\n.m-l-5 {\n  margin-left: 5px; }\n.m-x-5 {\n  margin-right: 5px;\n  margin-left: 5px; }\n.m-y-5 {\n  margin-top: 5px;\n  margin-bottom: 5px; }\n.m-8 {\n  margin: 8px; }\n.m-t-8 {\n  margin-top: 8px; }\n.m-r-8 {\n  margin-right: 8px; }\n.m-b-8 {\n  margin-bottom: 8px; }\n.m-l-8 {\n  margin-left: 8px; }\n.m-x-8 {\n  margin-right: 8px;\n  margin-left: 8px; }\n.m-y-8 {\n  margin-top: 8px;\n  margin-bottom: 8px; }\n.m-10 {\n  margin: 10px; }\n.m-t-10 {\n  margin-top: 10px; }\n.m-r-10 {\n  margin-right: 10px; }\n.m-b-10 {\n  margin-bottom: 10px; }\n.m-l-10 {\n  margin-left: 10px; }\n.m-x-10 {\n  margin-right: 10px;\n  margin-left: 10px; }\n.m-y-10 {\n  margin-top: 10px;\n  margin-bottom: 10px; }\n.m-12 {\n  margin: 12px; }\n.m-t-12 {\n  margin-top: 12px; }\n.m-r-12 {\n  margin-right: 12px; }\n.m-b-12 {\n  margin-bottom: 12px; }\n.m-l-12 {\n  margin-left: 12px; }\n.m-x-12 {\n  margin-right: 12px;\n  margin-left: 12px; }\n.m-y-12 {\n  margin-top: 12px;\n  margin-bottom: 12px; }\n.m-15 {\n  margin: 15px; }\n.m-t-15 {\n  margin-top: 15px; }\n.m-r-15 {\n  margin-right: 15px; }\n.m-b-15 {\n  margin-bottom: 15px; }\n.m-l-15 {\n  margin-left: 15px; }\n.m-x-15 {\n  margin-right: 15px;\n  margin-left: 15px; }\n.m-y-15 {\n  margin-top: 15px;\n  margin-bottom: 15px; }\n.m-16 {\n  margin: 16px; }\n.m-t-16 {\n  margin-top: 16px; }\n.m-r-16 {\n  margin-right: 16px; }\n.m-b-16 {\n  margin-bottom: 16px; }\n.m-l-16 {\n  margin-left: 16px; }\n.m-x-16 {\n  margin-right: 16px;\n  margin-left: 16px; }\n.m-y-16 {\n  margin-top: 16px;\n  margin-bottom: 16px; }\n.m-20 {\n  margin: 20px; }\n.m-t-20 {\n  margin-top: 20px; }\n.m-r-20 {\n  margin-right: 20px; }\n.m-b-20 {\n  margin-bottom: 20px; }\n.m-l-20 {\n  margin-left: 20px; }\n.m-x-20 {\n  margin-right: 20px;\n  margin-left: 20px; }\n.m-y-20 {\n  margin-top: 20px;\n  margin-bottom: 20px; }\n.m-24 {\n  margin: 24px; }\n.m-t-24 {\n  margin-top: 24px; }\n.m-r-24 {\n  margin-right: 24px; }\n.m-b-24 {\n  margin-bottom: 24px; }\n.m-l-24 {\n  margin-left: 24px; }\n.m-x-24 {\n  margin-right: 24px;\n  margin-left: 24px; }\n.m-y-24 {\n  margin-top: 24px;\n  margin-bottom: 24px; }\n.m-25 {\n  margin: 25px; }\n.m-t-25 {\n  margin-top: 25px; }\n.m-r-25 {\n  margin-right: 25px; }\n.m-b-25 {\n  margin-bottom: 25px; }\n.m-l-25 {\n  margin-left: 25px; }\n.m-x-25 {\n  margin-right: 25px;\n  margin-left: 25px; }\n.m-y-25 {\n  margin-top: 25px;\n  margin-bottom: 25px; }\n.m-28 {\n  margin: 28px; }\n.m-t-28 {\n  margin-top: 28px; }\n.m-r-28 {\n  margin-right: 28px; }\n.m-b-28 {\n  margin-bottom: 28px; }\n.m-l-28 {\n  margin-left: 28px; }\n.m-x-28 {\n  margin-right: 28px;\n  margin-left: 28px; }\n.m-y-28 {\n  margin-top: 28px;\n  margin-bottom: 28px; }\n.m-30 {\n  margin: 30px; }\n.m-t-30 {\n  margin-top: 30px; }\n.m-r-30 {\n  margin-right: 30px; }\n.m-b-30 {\n  margin-bottom: 30px; }\n.m-l-30 {\n  margin-left: 30px; }\n.m-x-30 {\n  margin-right: 30px;\n  margin-left: 30px; }\n.m-y-30 {\n  margin-top: 30px;\n  margin-bottom: 30px; }\n.p-0 {\n  padding: 0px; }\n.p-t-0 {\n  padding-top: 0px; }\n.p-r-0 {\n  padding-right: 0px; }\n.p-b-0 {\n  padding-bottom: 0px; }\n.p-l-0 {\n  padding-left: 0px; }\n.p-x-0 {\n  padding-right: 0px;\n  padding-left: 0px; }\n.p-y-0 {\n  padding-top: 0px;\n  padding-bottom: 0px; }\n.p-2 {\n  padding: 2px; }\n.p-t-2 {\n  padding-top: 2px; }\n.p-r-2 {\n  padding-right: 2px; }\n.p-b-2 {\n  padding-bottom: 2px; }\n.p-l-2 {\n  padding-left: 2px; }\n.p-x-2 {\n  padding-right: 2px;\n  padding-left: 2px; }\n.p-y-2 {\n  padding-top: 2px;\n  padding-bottom: 2px; }\n.p-4 {\n  padding: 4px; }\n.p-t-4 {\n  padding-top: 4px; }\n.p-r-4 {\n  padding-right: 4px; }\n.p-b-4 {\n  padding-bottom: 4px; }\n.p-l-4 {\n  padding-left: 4px; }\n.p-x-4 {\n  padding-right: 4px;\n  padding-left: 4px; }\n.p-y-4 {\n  padding-top: 4px;\n  padding-bottom: 4px; }\n.p-5 {\n  padding: 5px; }\n.p-t-5 {\n  padding-top: 5px; }\n.p-r-5 {\n  padding-right: 5px; }\n.p-b-5 {\n  padding-bottom: 5px; }\n.p-l-5 {\n  padding-left: 5px; }\n.p-x-5 {\n  padding-right: 5px;\n  padding-left: 5px; }\n.p-y-5 {\n  padding-top: 5px;\n  padding-bottom: 5px; }\n.p-8 {\n  padding: 8px; }\n.p-t-8 {\n  padding-top: 8px; }\n.p-r-8 {\n  padding-right: 8px; }\n.p-b-8 {\n  padding-bottom: 8px; }\n.p-l-8 {\n  padding-left: 8px; }\n.p-x-8 {\n  padding-right: 8px;\n  padding-left: 8px; }\n.p-y-8 {\n  padding-top: 8px;\n  padding-bottom: 8px; }\n.p-10 {\n  padding: 10px; }\n.p-t-10 {\n  padding-top: 10px; }\n.p-r-10 {\n  padding-right: 10px; }\n.p-b-10 {\n  padding-bottom: 10px; }\n.p-l-10 {\n  padding-left: 10px; }\n.p-x-10 {\n  padding-right: 10px;\n  padding-left: 10px; }\n.p-y-10 {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n.p-12 {\n  padding: 12px; }\n.p-t-12 {\n  padding-top: 12px; }\n.p-r-12 {\n  padding-right: 12px; }\n.p-b-12 {\n  padding-bottom: 12px; }\n.p-l-12 {\n  padding-left: 12px; }\n.p-x-12 {\n  padding-right: 12px;\n  padding-left: 12px; }\n.p-y-12 {\n  padding-top: 12px;\n  padding-bottom: 12px; }\n.p-15 {\n  padding: 15px; }\n.p-t-15 {\n  padding-top: 15px; }\n.p-r-15 {\n  padding-right: 15px; }\n.p-b-15 {\n  padding-bottom: 15px; }\n.p-l-15 {\n  padding-left: 15px; }\n.p-x-15 {\n  padding-right: 15px;\n  padding-left: 15px; }\n.p-y-15 {\n  padding-top: 15px;\n  padding-bottom: 15px; }\n.p-16 {\n  padding: 16px; }\n.p-t-16 {\n  padding-top: 16px; }\n.p-r-16 {\n  padding-right: 16px; }\n.p-b-16 {\n  padding-bottom: 16px; }\n.p-l-16 {\n  padding-left: 16px; }\n.p-x-16 {\n  padding-right: 16px;\n  padding-left: 16px; }\n.p-y-16 {\n  padding-top: 16px;\n  padding-bottom: 16px; }\n.p-20 {\n  padding: 20px; }\n.p-t-20 {\n  padding-top: 20px; }\n.p-r-20 {\n  padding-right: 20px; }\n.p-b-20 {\n  padding-bottom: 20px; }\n.p-l-20 {\n  padding-left: 20px; }\n.p-x-20 {\n  padding-right: 20px;\n  padding-left: 20px; }\n.p-y-20 {\n  padding-top: 20px;\n  padding-bottom: 20px; }\n.p-24 {\n  padding: 24px; }\n.p-t-24 {\n  padding-top: 24px; }\n.p-r-24 {\n  padding-right: 24px; }\n.p-b-24 {\n  padding-bottom: 24px; }\n.p-l-24 {\n  padding-left: 24px; }\n.p-x-24 {\n  padding-right: 24px;\n  padding-left: 24px; }\n.p-y-24 {\n  padding-top: 24px;\n  padding-bottom: 24px; }\n.p-25 {\n  padding: 25px; }\n.p-t-25 {\n  padding-top: 25px; }\n.p-r-25 {\n  padding-right: 25px; }\n.p-b-25 {\n  padding-bottom: 25px; }\n.p-l-25 {\n  padding-left: 25px; }\n.p-x-25 {\n  padding-right: 25px;\n  padding-left: 25px; }\n.p-y-25 {\n  padding-top: 25px;\n  padding-bottom: 25px; }\n.p-28 {\n  padding: 28px; }\n.p-t-28 {\n  padding-top: 28px; }\n.p-r-28 {\n  padding-right: 28px; }\n.p-b-28 {\n  padding-bottom: 28px; }\n.p-l-28 {\n  padding-left: 28px; }\n.p-x-28 {\n  padding-right: 28px;\n  padding-left: 28px; }\n.p-y-28 {\n  padding-top: 28px;\n  padding-bottom: 28px; }\n.p-30 {\n  padding: 30px; }\n.p-t-30 {\n  padding-top: 30px; }\n.p-r-30 {\n  padding-right: 30px; }\n.p-b-30 {\n  padding-bottom: 30px; }\n.p-l-30 {\n  padding-left: 30px; }\n.p-x-30 {\n  padding-right: 30px;\n  padding-left: 30px; }\n.p-y-30 {\n  padding-top: 30px;\n  padding-bottom: 30px; }\n/**\n * Element tag overrides\n */\nbody {\n  padding: 0;\n  margin: 0; }\n/**\n * The following are Ionic specific\n */\nbody {\n  padding-top: constant(safe-area-inset-top);\n  padding-top: env(safe-area-inset-top); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbm9ueW1vdXMvRGVza3RvcC9Xb3JrL21hbG9tYXRpYS9saWJzL3Njc3MvX2luZGV4LnNjc3MiLCJzcmMvZ2xvYmFsLnNjc3MiLCIvVXNlcnMvYW5vbnltb3VzL0Rlc2t0b3AvV29yay9tYWxvbWF0aWEvbGlicy9zY3NzL192YXJpYWJsZXMuc2NzcyIsIi9Vc2Vycy9hbm9ueW1vdXMvRGVza3RvcC9Xb3JrL21hbG9tYXRpYS94cGxhdC93ZWIvc2Nzcy9faW5kZXguc2NzcyIsIi9Vc2Vycy9hbm9ueW1vdXMvRGVza3RvcC9Xb3JrL21hbG9tYXRpYS94cGxhdC93ZWIvc2Nzcy9fc3BhY2luZy5zY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL2Nzcy9jb3JlLmNzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9jc3Mvbm9ybWFsaXplLmNzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9jc3Mvc3RydWN0dXJlLmNzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9jc3MvdHlwb2dyYXBoeS5jc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvY3NzL3BhZGRpbmcuY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL2Nzcy9mbG9hdC1lbGVtZW50cy5jc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvY3NzL3RleHQtYWxpZ25tZW50LmNzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9jc3MvdGV4dC10cmFuc2Zvcm1hdGlvbi5jc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvY3NzL2ZsZXgtdXRpbHMuY3NzIiwiL1VzZXJzL2Fub255bW91cy9EZXNrdG9wL1dvcmsvbWFsb21hdGlhL3hwbGF0L3dlYi9zY3NzL190YWdzLnNjc3MiLCIvVXNlcnMvYW5vbnltb3VzL0Rlc2t0b3AvV29yay9tYWxvbWF0aWEveHBsYXQvaW9uaWMvc2Nzcy9faW5kZXguc2NzcyIsIi9Vc2Vycy9hbm9ueW1vdXMvRGVza3RvcC9Xb3JrL21hbG9tYXRpYS94cGxhdC9pb25pYy9zY3NzL190YWdzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VDRUU7QUNGRjs7O0VETUU7QUVIRjs7RUZNRTtBR1RGOztFSFlFO0FHNEVGOzs7Ozs7RUhyRUU7QUluQkYsU0FBUyw2RkFBNkY7QUFBQyxRQUFRLDBEQUEwRDtBQUFDLEtBQUssMENBQTBDO0FBQUMsS0FBSyxzQ0FBc0M7QUFBQyx3QkFBd0IsZUFBZTtBQUFDLG1CQUFtQiw4REFBOEQsQ0FBQyx5RUFBeUUsQ0FBQyx3RUFBd0UsQ0FBQyx1RkFBdUYsQ0FBQyxxRUFBcUUsQ0FBQyxtRUFBbUU7QUFBQyxxQkFBcUIsZ0VBQWdFLENBQUMsMkVBQTJFLENBQUMsMEVBQTBFLENBQUMseUZBQXlGLENBQUMsdUVBQXVFLENBQUMscUVBQXFFO0FBQUMsb0JBQW9CLCtEQUErRCxDQUFDLDBFQUEwRSxDQUFDLHlFQUF5RSxDQUFDLHdGQUF3RixDQUFDLHNFQUFzRSxDQUFDLG9FQUFvRTtBQUFDLG1CQUFtQiw4REFBOEQsQ0FBQyx3RUFBd0UsQ0FBQyx3RUFBd0UsQ0FBQyx1RkFBdUYsQ0FBQyxxRUFBcUUsQ0FBQyxtRUFBbUU7QUFBQyxtQkFBbUIsOERBQThELENBQUMsd0VBQXdFLENBQUMsd0VBQXdFLENBQUMsdUZBQXVGLENBQUMscUVBQXFFLENBQUMsbUVBQW1FO0FBQUMsa0JBQWtCLDZEQUE2RCxDQUFDLHVFQUF1RSxDQUFDLHVFQUF1RSxDQUFDLHNGQUFzRixDQUFDLG9FQUFvRSxDQUFDLGtFQUFrRTtBQUFDLGlCQUFpQiw0REFBNEQsQ0FBQyx3RUFBd0UsQ0FBQyxzRUFBc0UsQ0FBQywrRUFBK0UsQ0FBQyxtRUFBbUUsQ0FBQyxpRUFBaUU7QUFBQyxrQkFBa0IsNkRBQTZELENBQUMseUVBQXlFLENBQUMsdUVBQXVFLENBQUMsc0ZBQXNGLENBQUMsb0VBQW9FLENBQUMsa0VBQWtFO0FBQUMsZ0JBQWdCLDJEQUEyRCxDQUFDLG9FQUFvRSxDQUFDLHFFQUFxRSxDQUFDLG9GQUFvRixDQUFDLGtFQUFrRSxDQUFDLGdFQUFnRTtBQUFDLFVBQVUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyw2QkFBNkIsQ0FBQyx5QkFBeUIsQ0FBQyxlQUFlLENBQUMsU0FBUztBQUFDLDBSQUEwUix1QkFBdUI7QUFBQyxvQkFBb0IsU0FBUztBQUFDLDZDQUE2Qyw2QkFBNkI7QUFBQyw4QkFBOEIsS0FBSyxpREFBaUQsQ0FBQztBQUFDLHVEQUF1RCxLQUFLLGtEQUFrRCxDQUFDLHdEQUF3RCxDQUFDLG9EQUFvRCxDQUFDLHNEQUFzRCxDQUFDO0FBQUMsa0RBQWtELEtBQUssNkNBQTZDLENBQUMsbURBQW1ELENBQUMsK0NBQStDLENBQUMsaURBQWlELENBQUM7QUNBeDdLLDRCQUE0Qix1QkFBdUI7QUFBQyxzQkFBc0IsWUFBWSxDQUFDLFFBQVE7QUFBQyxTQUFTLGdCQUFnQjtBQUFDLElBQUksY0FBYyxDQUFDLFFBQVE7QUFBQyxlQUFlLGVBQWU7QUFBQyxPQUFPLGVBQWU7QUFBQyxHQUFHLFVBQVUsQ0FBQyxjQUFjLENBQUMsc0JBQXNCO0FBQUMsSUFBSSxhQUFhO0FBQUMsa0JBQWtCLGdDQUFnQyxDQUFDLGFBQWE7QUFBQyw0QkFBNEIsbUJBQW1CLENBQUMsa0JBQWtCO0FBQUMsU0FBUyxhQUFhLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxhQUFhO0FBQUMsb0NBQXNCLGdCQUFnQjtBQUF0QywrQkFBc0IsZ0JBQWdCO0FBQXRDLGdDQUFzQixnQkFBZ0I7QUFBdEMsc0JBQXNCLGdCQUFnQjtBQUFDLDJCQUEyQixRQUFRLENBQUMsWUFBWSxDQUFDLGFBQWE7QUFBQyxtRUFBbUUsY0FBYyxDQUFDLHlCQUF5QjtBQUFDLHFOQUFxTix5QkFBeUI7QUFBQyw2QkFBNkIsbUJBQW1CO0FBQUMsT0FBTyxRQUFRLENBQUMsZUFBZSxDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLG9CQUFvQixDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMseUJBQXlCO0FBQUMsV0FBVyxjQUFjO0FBQUMsa0RBQWtELGNBQWM7QUFBQyxpREFBaUQsU0FBUyxDQUFDLFFBQVE7QUFBQywyQ0FBMkMsU0FBUyxDQUFDLHFCQUFxQjtBQUFDLGdHQUFnRyxXQUFXO0FBQUMsbUdBQW1HLHVCQUF1QjtBQUFDLE1BQU0sd0JBQXdCLENBQUMsZ0JBQWdCO0FBQUMsTUFBTSxTQUFTO0FDQTduRCxFQUFFLHFCQUFxQixDQUFDLHlDQUF5QyxDQUFDLHVDQUF1QyxDQUFDLDBCQUEwQjtBQUFDLEtBQUssVUFBVSxDQUFDLFdBQVcsQ0FBQyw2QkFBb0IsQ0FBcEIsMEJBQW9CLENBQXBCLHlCQUFvQixDQUFwQixxQkFBcUI7QUFBQyxhQUFhLFlBQVk7QUFBQyxLQUFLLGlDQUFpQyxDQUFDLGtDQUFrQyxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLGlDQUFpQyxDQUFDLGVBQWUsQ0FBQyx5QkFBeUIsQ0FBQyxzQkFBc0IsQ0FBQyx3QkFBd0IsQ0FBQyxvQkFBb0IsQ0FBQywwQkFBMEIsQ0FBQyw2QkFBb0IsQ0FBcEIsMEJBQW9CLENBQXBCLHlCQUFvQixDQUFwQixxQkFBcUI7QUNBenBCLEtBQUssa0NBQWtDO0FBQUMsRUFBRSw0QkFBNEIsQ0FBQyx1Q0FBdUM7QUFBQyxrQkFBa0IsZUFBZSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxlQUFlO0FBQUMsR0FBRyxlQUFlLENBQUMsY0FBYztBQUFDLEdBQUcsZUFBZSxDQUFDLGNBQWM7QUFBQyxHQUFHLGNBQWM7QUFBQyxHQUFHLGNBQWM7QUFBQyxHQUFHLGNBQWM7QUFBQyxHQUFHLGNBQWM7QUFBQyxNQUFNLGFBQWE7QUFBQyxRQUFRLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsdUJBQXVCO0FBQUMsSUFBSSxTQUFTO0FBQUMsSUFBSSxhQUFhO0FDQWpkLDZCQUE2QixrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0I7QUFBQyx1QkFBdUIseUNBQXlDLENBQUMsdUNBQXVDLENBQUMsdUNBQXVDLENBQUMsMENBQTBDLENBQUMscUNBQXFDLENBQUMsc0NBQXNDLENBQUMsb0NBQW9DLENBQUMsdUNBQXVDO0FBQUMsK0ZBQWdFLHVCQUF1QixrQkFBa0IsQ0FBQyxtQkFBbUIsQ0FBQyw4Q0FBOEMsQ0FBQyw2Q0FBNkMsQ0FBQyw0Q0FBNEMsQ0FBQywyQ0FBMkMsQ0FBQztBQUFDLCtCQUErQix1Q0FBdUMsQ0FBQyxvQ0FBb0M7QUFBQyxtQ0FBbUMseUNBQXlDLENBQUMscUNBQXFDO0FBQUMsK0ZBQWdFLG1DQUFtQyxrQkFBa0IsQ0FBQyw4Q0FBOEMsQ0FBQyw2Q0FBNkMsQ0FBQztBQUFDLCtCQUErQix1Q0FBdUMsQ0FBQyxzQ0FBc0M7QUFBQywrRkFBZ0UsK0JBQStCLG1CQUFtQixDQUFDLDRDQUE0QyxDQUFDLDJDQUEyQyxDQUFDO0FBQUMscUNBQXFDLDBDQUEwQyxDQUFDLHVDQUF1QztBQUFDLHlDQUF5Qyx1Q0FBdUMsQ0FBQywwQ0FBMEMsQ0FBQyxvQ0FBb0MsQ0FBQyx1Q0FBdUM7QUFBQyw2Q0FBNkMseUNBQXlDLENBQUMsdUNBQXVDLENBQUMscUNBQXFDLENBQUMsc0NBQXNDO0FBQUMsK0ZBQWdFLDZDQUE2QyxrQkFBa0IsQ0FBQyxtQkFBbUIsQ0FBQyw4Q0FBOEMsQ0FBQyw2Q0FBNkMsQ0FBQyw0Q0FBNEMsQ0FBQywyQ0FBMkMsQ0FBQztBQUFDLDJCQUEyQixpQkFBaUIsQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLGVBQWU7QUFBQyxxQkFBcUIsdUNBQXVDLENBQUMscUNBQXFDLENBQUMscUNBQXFDLENBQUMsd0NBQXdDLENBQUMsbUNBQW1DLENBQUMsb0NBQW9DLENBQUMsa0NBQWtDLENBQUMscUNBQXFDO0FBQUMsK0ZBQWdFLHFCQUFxQixpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyw0Q0FBNEMsQ0FBQywyQ0FBMkMsQ0FBQywwQ0FBMEMsQ0FBQyx5Q0FBeUMsQ0FBQztBQUFDLDZCQUE2QixxQ0FBcUMsQ0FBQyxrQ0FBa0M7QUFBQyxpQ0FBaUMsdUNBQXVDLENBQUMsbUNBQW1DO0FBQUMsK0ZBQWdFLGlDQUFpQyxpQkFBaUIsQ0FBQyw0Q0FBNEMsQ0FBQywyQ0FBMkMsQ0FBQztBQUFDLDZCQUE2QixxQ0FBcUMsQ0FBQyxvQ0FBb0M7QUFBQywrRkFBZ0UsNkJBQTZCLGtCQUFrQixDQUFDLDBDQUEwQyxDQUFDLHlDQUF5QyxDQUFDO0FBQUMsbUNBQW1DLHdDQUF3QyxDQUFDLHFDQUFxQztBQUFDLHVDQUF1QyxxQ0FBcUMsQ0FBQyx3Q0FBd0MsQ0FBQyxrQ0FBa0MsQ0FBQyxxQ0FBcUM7QUFBQywyQ0FBMkMsdUNBQXVDLENBQUMscUNBQXFDLENBQUMsbUNBQW1DLENBQUMsb0NBQW9DO0FBQUMsK0ZBQWdFLDJDQUEyQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyw0Q0FBNEMsQ0FBQywyQ0FBMkMsQ0FBQywwQ0FBMEMsQ0FBQyx5Q0FBeUMsQ0FBQztBQ0EzbkosNkJBQTZCLHFCQUFxQjtBQUFDLCtCQUErQixzQkFBc0I7QUFBQywrQkFBK0IscUJBQXFCO0FBQUMsaUZBQWlGLHNCQUFzQjtBQUFDLDJCQUEyQixzQkFBc0I7QUFBQyw2RUFBNkUscUJBQXFCO0FBQUMsMEJBQTBCLG1DQUFtQyxxQkFBcUIsQ0FBQyxxQ0FBcUMsc0JBQXNCLENBQUMscUNBQXFDLHFCQUFxQixDQUFDLHVGQUF1RixzQkFBc0IsQ0FBQyxpQ0FBaUMsc0JBQXNCLENBQUMsbUZBQW1GLHFCQUFxQixDQUFDO0FBQUMsMEJBQTBCLG1DQUFtQyxxQkFBcUIsQ0FBQyxxQ0FBcUMsc0JBQXNCLENBQUMscUNBQXFDLHFCQUFxQixDQUFDLHVGQUF1RixzQkFBc0IsQ0FBQyxpQ0FBaUMsc0JBQXNCLENBQUMsbUZBQW1GLHFCQUFxQixDQUFDO0FBQUMsMEJBQTBCLG1DQUFtQyxxQkFBcUIsQ0FBQyxxQ0FBcUMsc0JBQXNCLENBQUMscUNBQXFDLHFCQUFxQixDQUFDLHVGQUF1RixzQkFBc0IsQ0FBQyxpQ0FBaUMsc0JBQXNCLENBQUMsbUZBQW1GLHFCQUFxQixDQUFDO0FBQUMsMkJBQTJCLG1DQUFtQyxxQkFBcUIsQ0FBQyxxQ0FBcUMsc0JBQXNCLENBQUMscUNBQXFDLHFCQUFxQixDQUFDLHVGQUF1RixzQkFBc0IsQ0FBQyxpQ0FBaUMsc0JBQXNCLENBQUMsbUZBQW1GLHFCQUFxQixDQUFDO0FDQW53RSwrQkFBK0IsNEJBQTRCO0FBQUMsaUNBQWlDLDZCQUE2QjtBQUFDLDZCQUE2QiwyQkFBMkI7QUFBQyx5QkFBeUIseUJBQXlCO0FBQUMsMkJBQTJCLDBCQUEwQjtBQUFDLDZCQUE2QiwyQkFBMkI7QUFBQywrQkFBK0IsNkJBQTZCO0FBQUMsMkJBQTJCLDZCQUE2QjtBQUFDLDBCQUEwQixxQ0FBcUMsNEJBQTRCLENBQUMsdUNBQXVDLDZCQUE2QixDQUFDLG1DQUFtQywyQkFBMkIsQ0FBQywrQkFBK0IseUJBQXlCLENBQUMsaUNBQWlDLDBCQUEwQixDQUFDLG1DQUFtQywyQkFBMkIsQ0FBQyxxQ0FBcUMsNkJBQTZCLENBQUMsaUNBQWlDLDZCQUE2QixDQUFDO0FBQUMsMEJBQTBCLHFDQUFxQyw0QkFBNEIsQ0FBQyx1Q0FBdUMsNkJBQTZCLENBQUMsbUNBQW1DLDJCQUEyQixDQUFDLCtCQUErQix5QkFBeUIsQ0FBQyxpQ0FBaUMsMEJBQTBCLENBQUMsbUNBQW1DLDJCQUEyQixDQUFDLHFDQUFxQyw2QkFBNkIsQ0FBQyxpQ0FBaUMsNkJBQTZCLENBQUM7QUFBQywwQkFBMEIscUNBQXFDLDRCQUE0QixDQUFDLHVDQUF1Qyw2QkFBNkIsQ0FBQyxtQ0FBbUMsMkJBQTJCLENBQUMsK0JBQStCLHlCQUF5QixDQUFDLGlDQUFpQywwQkFBMEIsQ0FBQyxtQ0FBbUMsMkJBQTJCLENBQUMscUNBQXFDLDZCQUE2QixDQUFDLGlDQUFpQyw2QkFBNkIsQ0FBQztBQUFDLDJCQUEyQixxQ0FBcUMsNEJBQTRCLENBQUMsdUNBQXVDLDZCQUE2QixDQUFDLG1DQUFtQywyQkFBMkIsQ0FBQywrQkFBK0IseUJBQXlCLENBQUMsaUNBQWlDLDBCQUEwQixDQUFDLG1DQUFtQywyQkFBMkIsQ0FBQyxxQ0FBcUMsNkJBQTZCLENBQUMsaUNBQWlDLDZCQUE2QixDQUFDO0FDQXhpRixxQ0FBcUMsbUNBQW1DO0FBQUMscUNBQXFDLG1DQUFtQztBQUFDLHVDQUF1QyxvQ0FBb0M7QUFBQywwQkFBMEIsMkNBQTJDLG1DQUFtQyxDQUFDLDJDQUEyQyxtQ0FBbUMsQ0FBQyw2Q0FBNkMsb0NBQW9DLENBQUM7QUFBQywwQkFBMEIsMkNBQTJDLG1DQUFtQyxDQUFDLDJDQUEyQyxtQ0FBbUMsQ0FBQyw2Q0FBNkMsb0NBQW9DLENBQUM7QUFBQywwQkFBMEIsMkNBQTJDLG1DQUFtQyxDQUFDLDJDQUEyQyxtQ0FBbUMsQ0FBQyw2Q0FBNkMsb0NBQW9DLENBQUM7QUFBQywyQkFBMkIsMkNBQTJDLG1DQUFtQyxDQUFDLDJDQUEyQyxtQ0FBbUMsQ0FBQyw2Q0FBNkMsb0NBQW9DLENBQUM7QUNBMXdDLHlDQUF5QyxnQ0FBZ0M7QUFBQyxxQ0FBcUMsOEJBQThCO0FBQUMsMkNBQTJDLDRCQUE0QjtBQUFDLDZDQUE2Qyw2QkFBNkI7QUFBQywrQ0FBK0MsOEJBQThCO0FBQUMsdUNBQXVDLDBCQUEwQjtBQUFDLGlCQUFpQix5QkFBeUI7QUFBQyxxQkFBcUIsMkJBQTJCO0FBQUMsaUNBQWlDLGlDQUFpQztBQUFDLG1EQUFtRCxxQ0FBcUM7QUFBQyxxREFBcUQsaUNBQWlDO0FBQUMsK0NBQStDLG1DQUFtQztBQUFDLHFEQUFxRCx1Q0FBdUM7QUFBQyx1REFBdUQsd0NBQXdDO0FBQUMscURBQXFELHVDQUF1QztBQUFDLDJDQUEyQyxpQ0FBaUM7QUFBQyw2Q0FBNkMsNkJBQTZCO0FBQUMsdUNBQXVDLCtCQUErQjtBQUFDLCtDQUErQyw4QkFBOEI7QUFBQyxpREFBaUQsK0JBQStCO0FUdUd0K0M7RUFDRSxXQUFVLEVBQUE7QUFHWjtFQUNFLGVBQWtCLEVBQUE7QUFFcEI7RUFDRSxpQkFBc0IsRUFBQTtBQUV4QjtFQUNFLGtCQUF3QixFQUFBO0FBRTFCO0VBQ0UsZ0JBQW9CLEVBQUE7QUFJdEI7RUFDRSxpQkFBc0I7RUFDdEIsZ0JBQW9CLEVBQUE7QUFHdEI7RUFDRSxlQUFrQjtFQUNsQixrQkFBd0IsRUFBQTtBQXpCMUI7RUFDRSxXQUFVLEVBQUE7QUFHWjtFQUNFLGVBQWtCLEVBQUE7QUFFcEI7RUFDRSxpQkFBc0IsRUFBQTtBQUV4QjtFQUNFLGtCQUF3QixFQUFBO0FBRTFCO0VBQ0UsZ0JBQW9CLEVBQUE7QUFJdEI7RUFDRSxpQkFBc0I7RUFDdEIsZ0JBQW9CLEVBQUE7QUFHdEI7RUFDRSxlQUFrQjtFQUNsQixrQkFBd0IsRUFBQTtBQXpCMUI7RUFDRSxXQUFVLEVBQUE7QUFHWjtFQUNFLGVBQWtCLEVBQUE7QUFFcEI7RUFDRSxpQkFBc0IsRUFBQTtBQUV4QjtFQUNFLGtCQUF3QixFQUFBO0FBRTFCO0VBQ0UsZ0JBQW9CLEVBQUE7QUFJdEI7RUFDRSxpQkFBc0I7RUFDdEIsZ0JBQW9CLEVBQUE7QUFHdEI7RUFDRSxlQUFrQjtFQUNsQixrQkFBd0IsRUFBQTtBQXpCMUI7RUFDRSxXQUFVLEVBQUE7QUFHWjtFQUNFLGVBQWtCLEVBQUE7QUFFcEI7RUFDRSxpQkFBc0IsRUFBQTtBQUV4QjtFQUNFLGtCQUF3QixFQUFBO0FBRTFCO0VBQ0UsZ0JBQW9CLEVBQUE7QUFJdEI7RUFDRSxpQkFBc0I7RUFDdEIsZ0JBQW9CLEVBQUE7QUFHdEI7RUFDRSxlQUFrQjtFQUNsQixrQkFBd0IsRUFBQTtBQXpCMUI7RUFDRSxXQUFVLEVBQUE7QUFHWjtFQUNFLGVBQWtCLEVBQUE7QUFFcEI7RUFDRSxpQkFBc0IsRUFBQTtBQUV4QjtFQUNFLGtCQUF3QixFQUFBO0FBRTFCO0VBQ0UsZ0JBQW9CLEVBQUE7QUFJdEI7RUFDRSxpQkFBc0I7RUFDdEIsZ0JBQW9CLEVBQUE7QUFHdEI7RUFDRSxlQUFrQjtFQUNsQixrQkFBd0IsRUFBQTtBQXpCMUI7RUFDRSxZQUFVLEVBQUE7QUFHWjtFQUNFLGdCQUFrQixFQUFBO0FBRXBCO0VBQ0Usa0JBQXNCLEVBQUE7QUFFeEI7RUFDRSxtQkFBd0IsRUFBQTtBQUUxQjtFQUNFLGlCQUFvQixFQUFBO0FBSXRCO0VBQ0Usa0JBQXNCO0VBQ3RCLGlCQUFvQixFQUFBO0FBR3RCO0VBQ0UsZ0JBQWtCO0VBQ2xCLG1CQUF3QixFQUFBO0FBekIxQjtFQUNFLFlBQVUsRUFBQTtBQUdaO0VBQ0UsZ0JBQWtCLEVBQUE7QUFFcEI7RUFDRSxrQkFBc0IsRUFBQTtBQUV4QjtFQUNFLG1CQUF3QixFQUFBO0FBRTFCO0VBQ0UsaUJBQW9CLEVBQUE7QUFJdEI7RUFDRSxrQkFBc0I7RUFDdEIsaUJBQW9CLEVBQUE7QUFHdEI7RUFDRSxnQkFBa0I7RUFDbEIsbUJBQXdCLEVBQUE7QUF6QjFCO0VBQ0UsWUFBVSxFQUFBO0FBR1o7RUFDRSxnQkFBa0IsRUFBQTtBQUVwQjtFQUNFLGtCQUFzQixFQUFBO0FBRXhCO0VBQ0UsbUJBQXdCLEVBQUE7QUFFMUI7RUFDRSxpQkFBb0IsRUFBQTtBQUl0QjtFQUNFLGtCQUFzQjtFQUN0QixpQkFBb0IsRUFBQTtBQUd0QjtFQUNFLGdCQUFrQjtFQUNsQixtQkFBd0IsRUFBQTtBQXpCMUI7RUFDRSxZQUFVLEVBQUE7QUFHWjtFQUNFLGdCQUFrQixFQUFBO0FBRXBCO0VBQ0Usa0JBQXNCLEVBQUE7QUFFeEI7RUFDRSxtQkFBd0IsRUFBQTtBQUUxQjtFQUNFLGlCQUFvQixFQUFBO0FBSXRCO0VBQ0Usa0JBQXNCO0VBQ3RCLGlCQUFvQixFQUFBO0FBR3RCO0VBQ0UsZ0JBQWtCO0VBQ2xCLG1CQUF3QixFQUFBO0FBekIxQjtFQUNFLFlBQVUsRUFBQTtBQUdaO0VBQ0UsZ0JBQWtCLEVBQUE7QUFFcEI7RUFDRSxrQkFBc0IsRUFBQTtBQUV4QjtFQUNFLG1CQUF3QixFQUFBO0FBRTFCO0VBQ0UsaUJBQW9CLEVBQUE7QUFJdEI7RUFDRSxrQkFBc0I7RUFDdEIsaUJBQW9CLEVBQUE7QUFHdEI7RUFDRSxnQkFBa0I7RUFDbEIsbUJBQXdCLEVBQUE7QUF6QjFCO0VBQ0UsWUFBVSxFQUFBO0FBR1o7RUFDRSxnQkFBa0IsRUFBQTtBQUVwQjtFQUNFLGtCQUFzQixFQUFBO0FBRXhCO0VBQ0UsbUJBQXdCLEVBQUE7QUFFMUI7RUFDRSxpQkFBb0IsRUFBQTtBQUl0QjtFQUNFLGtCQUFzQjtFQUN0QixpQkFBb0IsRUFBQTtBQUd0QjtFQUNFLGdCQUFrQjtFQUNsQixtQkFBd0IsRUFBQTtBQXpCMUI7RUFDRSxZQUFVLEVBQUE7QUFHWjtFQUNFLGdCQUFrQixFQUFBO0FBRXBCO0VBQ0Usa0JBQXNCLEVBQUE7QUFFeEI7RUFDRSxtQkFBd0IsRUFBQTtBQUUxQjtFQUNFLGlCQUFvQixFQUFBO0FBSXRCO0VBQ0Usa0JBQXNCO0VBQ3RCLGlCQUFvQixFQUFBO0FBR3RCO0VBQ0UsZ0JBQWtCO0VBQ2xCLG1CQUF3QixFQUFBO0FBekIxQjtFQUNFLFlBQVUsRUFBQTtBQUdaO0VBQ0UsZ0JBQWtCLEVBQUE7QUFFcEI7RUFDRSxrQkFBc0IsRUFBQTtBQUV4QjtFQUNFLG1CQUF3QixFQUFBO0FBRTFCO0VBQ0UsaUJBQW9CLEVBQUE7QUFJdEI7RUFDRSxrQkFBc0I7RUFDdEIsaUJBQW9CLEVBQUE7QUFHdEI7RUFDRSxnQkFBa0I7RUFDbEIsbUJBQXdCLEVBQUE7QUF6QjFCO0VBQ0UsWUFBVSxFQUFBO0FBR1o7RUFDRSxnQkFBa0IsRUFBQTtBQUVwQjtFQUNFLGtCQUFzQixFQUFBO0FBRXhCO0VBQ0UsbUJBQXdCLEVBQUE7QUFFMUI7RUFDRSxpQkFBb0IsRUFBQTtBQUl0QjtFQUNFLGtCQUFzQjtFQUN0QixpQkFBb0IsRUFBQTtBQUd0QjtFQUNFLGdCQUFrQjtFQUNsQixtQkFBd0IsRUFBQTtBQXpCMUI7RUFDRSxZQUFVLEVBQUE7QUFHWjtFQUNFLGdCQUFrQixFQUFBO0FBRXBCO0VBQ0Usa0JBQXNCLEVBQUE7QUFFeEI7RUFDRSxtQkFBd0IsRUFBQTtBQUUxQjtFQUNFLGlCQUFvQixFQUFBO0FBSXRCO0VBQ0Usa0JBQXNCO0VBQ3RCLGlCQUFvQixFQUFBO0FBR3RCO0VBQ0UsZ0JBQWtCO0VBQ2xCLG1CQUF3QixFQUFBO0FBekIxQjtFQUNFLFlBQVUsRUFBQTtBQUdaO0VBQ0UsZ0JBQWtCLEVBQUE7QUFFcEI7RUFDRSxrQkFBc0IsRUFBQTtBQUV4QjtFQUNFLG1CQUF3QixFQUFBO0FBRTFCO0VBQ0UsaUJBQW9CLEVBQUE7QUFJdEI7RUFDRSxrQkFBc0I7RUFDdEIsaUJBQW9CLEVBQUE7QUFHdEI7RUFDRSxnQkFBa0I7RUFDbEIsbUJBQXdCLEVBQUE7QUF6QjFCO0VBQ0UsWUFBVSxFQUFBO0FBR1o7RUFDRSxnQkFBa0IsRUFBQTtBQUVwQjtFQUNFLGtCQUFzQixFQUFBO0FBRXhCO0VBQ0UsbUJBQXdCLEVBQUE7QUFFMUI7RUFDRSxpQkFBb0IsRUFBQTtBQUl0QjtFQUNFLGtCQUFzQjtFQUN0QixpQkFBb0IsRUFBQTtBQUd0QjtFQUNFLGdCQUFrQjtFQUNsQixtQkFBd0IsRUFBQTtBQXpCMUI7RUFDRSxZQUFVLEVBQUE7QUFHWjtFQUNFLGdCQUFrQixFQUFBO0FBRXBCO0VBQ0Usa0JBQXNCLEVBQUE7QUFFeEI7RUFDRSxtQkFBd0IsRUFBQTtBQUUxQjtFQUNFLGlCQUFvQixFQUFBO0FBSXRCO0VBQ0Usa0JBQXNCO0VBQ3RCLGlCQUFvQixFQUFBO0FBR3RCO0VBQ0UsZ0JBQWtCO0VBQ2xCLG1CQUF3QixFQUFBO0FBekIxQjtFQUNFLFlBQVUsRUFBQTtBQUdaO0VBQ0UsZ0JBQWtCLEVBQUE7QUFFcEI7RUFDRSxrQkFBc0IsRUFBQTtBQUV4QjtFQUNFLG1CQUF3QixFQUFBO0FBRTFCO0VBQ0UsaUJBQW9CLEVBQUE7QUFJdEI7RUFDRSxrQkFBc0I7RUFDdEIsaUJBQW9CLEVBQUE7QUFHdEI7RUFDRSxnQkFBa0I7RUFDbEIsbUJBQXdCLEVBQUE7QUF6QjFCO0VBQ0UsYUFBVSxFQUFBO0FBR1o7RUFDRSxpQkFBa0IsRUFBQTtBQUVwQjtFQUNFLG1CQUFzQixFQUFBO0FBRXhCO0VBQ0Usb0JBQXdCLEVBQUE7QUFFMUI7RUFDRSxrQkFBb0IsRUFBQTtBQUl0QjtFQUNFLG1CQUFzQjtFQUN0QixrQkFBb0IsRUFBQTtBQUd0QjtFQUNFLGlCQUFrQjtFQUNsQixvQkFBd0IsRUFBQTtBQXpCMUI7RUFDRSxhQUFVLEVBQUE7QUFHWjtFQUNFLGlCQUFrQixFQUFBO0FBRXBCO0VBQ0UsbUJBQXNCLEVBQUE7QUFFeEI7RUFDRSxvQkFBd0IsRUFBQTtBQUUxQjtFQUNFLGtCQUFvQixFQUFBO0FBSXRCO0VBQ0UsbUJBQXNCO0VBQ3RCLGtCQUFvQixFQUFBO0FBR3RCO0VBQ0UsaUJBQWtCO0VBQ2xCLG9CQUF3QixFQUFBO0FBekIxQjtFQUNFLGFBQVUsRUFBQTtBQUdaO0VBQ0UsaUJBQWtCLEVBQUE7QUFFcEI7RUFDRSxtQkFBc0IsRUFBQTtBQUV4QjtFQUNFLG9CQUF3QixFQUFBO0FBRTFCO0VBQ0Usa0JBQW9CLEVBQUE7QUFJdEI7RUFDRSxtQkFBc0I7RUFDdEIsa0JBQW9CLEVBQUE7QUFHdEI7RUFDRSxpQkFBa0I7RUFDbEIsb0JBQXdCLEVBQUE7QUF6QjFCO0VBQ0UsYUFBVSxFQUFBO0FBR1o7RUFDRSxpQkFBa0IsRUFBQTtBQUVwQjtFQUNFLG1CQUFzQixFQUFBO0FBRXhCO0VBQ0Usb0JBQXdCLEVBQUE7QUFFMUI7RUFDRSxrQkFBb0IsRUFBQTtBQUl0QjtFQUNFLG1CQUFzQjtFQUN0QixrQkFBb0IsRUFBQTtBQUd0QjtFQUNFLGlCQUFrQjtFQUNsQixvQkFBd0IsRUFBQTtBQXpCMUI7RUFDRSxhQUFVLEVBQUE7QUFHWjtFQUNFLGlCQUFrQixFQUFBO0FBRXBCO0VBQ0UsbUJBQXNCLEVBQUE7QUFFeEI7RUFDRSxvQkFBd0IsRUFBQTtBQUUxQjtFQUNFLGtCQUFvQixFQUFBO0FBSXRCO0VBQ0UsbUJBQXNCO0VBQ3RCLGtCQUFvQixFQUFBO0FBR3RCO0VBQ0UsaUJBQWtCO0VBQ2xCLG9CQUF3QixFQUFBO0FBekIxQjtFQUNFLGFBQVUsRUFBQTtBQUdaO0VBQ0UsaUJBQWtCLEVBQUE7QUFFcEI7RUFDRSxtQkFBc0IsRUFBQTtBQUV4QjtFQUNFLG9CQUF3QixFQUFBO0FBRTFCO0VBQ0Usa0JBQW9CLEVBQUE7QUFJdEI7RUFDRSxtQkFBc0I7RUFDdEIsa0JBQW9CLEVBQUE7QUFHdEI7RUFDRSxpQkFBa0I7RUFDbEIsb0JBQXdCLEVBQUE7QUF6QjFCO0VBQ0UsYUFBVSxFQUFBO0FBR1o7RUFDRSxpQkFBa0IsRUFBQTtBQUVwQjtFQUNFLG1CQUFzQixFQUFBO0FBRXhCO0VBQ0Usb0JBQXdCLEVBQUE7QUFFMUI7RUFDRSxrQkFBb0IsRUFBQTtBQUl0QjtFQUNFLG1CQUFzQjtFQUN0QixrQkFBb0IsRUFBQTtBQUd0QjtFQUNFLGlCQUFrQjtFQUNsQixvQkFBd0IsRUFBQTtBQXpCMUI7RUFDRSxhQUFVLEVBQUE7QUFHWjtFQUNFLGlCQUFrQixFQUFBO0FBRXBCO0VBQ0UsbUJBQXNCLEVBQUE7QUFFeEI7RUFDRSxvQkFBd0IsRUFBQTtBQUUxQjtFQUNFLGtCQUFvQixFQUFBO0FBSXRCO0VBQ0UsbUJBQXNCO0VBQ3RCLGtCQUFvQixFQUFBO0FBR3RCO0VBQ0UsaUJBQWtCO0VBQ2xCLG9CQUF3QixFQUFBO0FBekIxQjtFQUNFLGFBQVUsRUFBQTtBQUdaO0VBQ0UsaUJBQWtCLEVBQUE7QUFFcEI7RUFDRSxtQkFBc0IsRUFBQTtBQUV4QjtFQUNFLG9CQUF3QixFQUFBO0FBRTFCO0VBQ0Usa0JBQW9CLEVBQUE7QUFJdEI7RUFDRSxtQkFBc0I7RUFDdEIsa0JBQW9CLEVBQUE7QUFHdEI7RUFDRSxpQkFBa0I7RUFDbEIsb0JBQXdCLEVBQUE7QVVoSTlCOztFYm1xQkU7QWFocUJGO0VBQ0UsVUFBVTtFQUNWLFNBQVMsRUFBQTtBQ1VYOztFZDJwQkU7QWUxcUJGO0VBQ0UsMENBQTBDO0VBQzFDLHFDQUFxQyxFQUFBIiwiZmlsZSI6InNyYy9nbG9iYWwuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogVGhlIGZvbGxvd2luZyBhcmUgc2hhcmVkIGFjcm9zcyBhbGwgcGxhdGZvcm1zIGFuZCBhcHBzXG4gKi9cblxuLy8gc2hhcmVkIHZhcmlhYmxlc1xuQGltcG9ydCAndmFyaWFibGVzJztcblxuLy8gY3JlYXRlL2ltcG9ydCBvdGhlciBzY3NzIGZpbGVzIG9yIGRlZmluZSBhcyBuZWVkZWQuLi5cbiIsIi8qKlxuICogVGhlIGZvbGxvd2luZyBhcmUgc2hhcmVkIGFjcm9zcyBhbGwgcGxhdGZvcm1zIGFuZCBhcHBzXG4gKi9cbi8qKlxuICogU2hhcmVkIGFjcm9zcyBhbGwgcGxhdGZvcm1zIGFuZCBhcHBzXG4gKiBZb3UgbWF5IGRlZmluZSBhIHNldCBvZiBnbG9iYWwgdmFyaWFibGVzIGFjY2Vzc2libGUgYWNyb3NzIGVudGlyZSB3b3Jrc3BhY2UgaGVyZVxuICovXG4vKipcbiAqIFRoZSBmb2xsb3dpbmcgYXJlIHdlYiBzcGVjaWZpYyAodXNlZCB3aXRoIGFueSB3ZWIgYXBwIHRhcmdldHMpXG4gKi9cbi8qKlxuICogQ29udmVuaWVudCBzcGFjaW5nIGNsYXNzZXNcbiAqL1xuLyoqXG4gKiBNYXJnaW4gYW5kIFBhZGRpbmdcbiAqIFRoZSBmb2xsb3dpbmcgY3JlYXRlcyB0aGlzIHBhdHRlcm46XG4gKiAubS0we21hcmdpbjowfS5tLXQtMHttYXJnaW4tdG9wOjB9Lm0tci0we21hcmdpbi1yaWdodDowfS5tLWItMHttYXJnaW4tYm90dG9tOjB9Lm0tbC0we21hcmdpbi1sZWZ0OjB9Lm0teC0we21hcmdpbi1yaWdodDowO21hcmdpbi1sZWZ0OjB9Lm0teS0we21hcmdpbi10b3A6MDttYXJnaW4tYm90dG9tOjB9XG4gKiBTYW1lIGZvciBQYWRkaW5nICh1c2luZyB0aGUgJ3AnIGFiYnJldmlhdGlvbilcbiAqIEZyb20gMCwgMiwgNSwgMTAsIDE1LCAyMCwgMjUsIDMwXG4qKi9cbkBpbXBvcnQgdXJsKH5AaW9uaWMvYW5ndWxhci9jc3MvY29yZS5jc3MpO1xuQGltcG9ydCB1cmwofkBpb25pYy9hbmd1bGFyL2Nzcy9ub3JtYWxpemUuY3NzKTtcbkBpbXBvcnQgdXJsKH5AaW9uaWMvYW5ndWxhci9jc3Mvc3RydWN0dXJlLmNzcyk7XG5AaW1wb3J0IHVybCh+QGlvbmljL2FuZ3VsYXIvY3NzL3R5cG9ncmFwaHkuY3NzKTtcbkBpbXBvcnQgdXJsKH5AaW9uaWMvYW5ndWxhci9jc3MvcGFkZGluZy5jc3MpO1xuQGltcG9ydCB1cmwofkBpb25pYy9hbmd1bGFyL2Nzcy9mbG9hdC1lbGVtZW50cy5jc3MpO1xuQGltcG9ydCB1cmwofkBpb25pYy9hbmd1bGFyL2Nzcy90ZXh0LWFsaWdubWVudC5jc3MpO1xuQGltcG9ydCB1cmwofkBpb25pYy9hbmd1bGFyL2Nzcy90ZXh0LXRyYW5zZm9ybWF0aW9uLmNzcyk7XG5AaW1wb3J0IHVybCh+QGlvbmljL2FuZ3VsYXIvY3NzL2ZsZXgtdXRpbHMuY3NzKTtcbi5tLTAge1xuICBtYXJnaW46IDBweDsgfVxuXG4ubS10LTAge1xuICBtYXJnaW4tdG9wOiAwcHg7IH1cblxuLm0tci0wIHtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7IH1cblxuLm0tYi0wIHtcbiAgbWFyZ2luLWJvdHRvbTogMHB4OyB9XG5cbi5tLWwtMCB7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7IH1cblxuLm0teC0wIHtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7IH1cblxuLm0teS0wIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7IH1cblxuLm0tMiB7XG4gIG1hcmdpbjogMnB4OyB9XG5cbi5tLXQtMiB7XG4gIG1hcmdpbi10b3A6IDJweDsgfVxuXG4ubS1yLTIge1xuICBtYXJnaW4tcmlnaHQ6IDJweDsgfVxuXG4ubS1iLTIge1xuICBtYXJnaW4tYm90dG9tOiAycHg7IH1cblxuLm0tbC0yIHtcbiAgbWFyZ2luLWxlZnQ6IDJweDsgfVxuXG4ubS14LTIge1xuICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDsgfVxuXG4ubS15LTIge1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDsgfVxuXG4ubS00IHtcbiAgbWFyZ2luOiA0cHg7IH1cblxuLm0tdC00IHtcbiAgbWFyZ2luLXRvcDogNHB4OyB9XG5cbi5tLXItNCB7XG4gIG1hcmdpbi1yaWdodDogNHB4OyB9XG5cbi5tLWItNCB7XG4gIG1hcmdpbi1ib3R0b206IDRweDsgfVxuXG4ubS1sLTQge1xuICBtYXJnaW4tbGVmdDogNHB4OyB9XG5cbi5tLXgtNCB7XG4gIG1hcmdpbi1yaWdodDogNHB4O1xuICBtYXJnaW4tbGVmdDogNHB4OyB9XG5cbi5tLXktNCB7XG4gIG1hcmdpbi10b3A6IDRweDtcbiAgbWFyZ2luLWJvdHRvbTogNHB4OyB9XG5cbi5tLTUge1xuICBtYXJnaW46IDVweDsgfVxuXG4ubS10LTUge1xuICBtYXJnaW4tdG9wOiA1cHg7IH1cblxuLm0tci01IHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7IH1cblxuLm0tYi01IHtcbiAgbWFyZ2luLWJvdHRvbTogNXB4OyB9XG5cbi5tLWwtNSB7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7IH1cblxuLm0teC01IHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7IH1cblxuLm0teS01IHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7IH1cblxuLm0tOCB7XG4gIG1hcmdpbjogOHB4OyB9XG5cbi5tLXQtOCB7XG4gIG1hcmdpbi10b3A6IDhweDsgfVxuXG4ubS1yLTgge1xuICBtYXJnaW4tcmlnaHQ6IDhweDsgfVxuXG4ubS1iLTgge1xuICBtYXJnaW4tYm90dG9tOiA4cHg7IH1cblxuLm0tbC04IHtcbiAgbWFyZ2luLWxlZnQ6IDhweDsgfVxuXG4ubS14LTgge1xuICBtYXJnaW4tcmlnaHQ6IDhweDtcbiAgbWFyZ2luLWxlZnQ6IDhweDsgfVxuXG4ubS15LTgge1xuICBtYXJnaW4tdG9wOiA4cHg7XG4gIG1hcmdpbi1ib3R0b206IDhweDsgfVxuXG4ubS0xMCB7XG4gIG1hcmdpbjogMTBweDsgfVxuXG4ubS10LTEwIHtcbiAgbWFyZ2luLXRvcDogMTBweDsgfVxuXG4ubS1yLTEwIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4OyB9XG5cbi5tLWItMTAge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4OyB9XG5cbi5tLWwtMTAge1xuICBtYXJnaW4tbGVmdDogMTBweDsgfVxuXG4ubS14LTEwIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICBtYXJnaW4tbGVmdDogMTBweDsgfVxuXG4ubS15LTEwIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDsgfVxuXG4ubS0xMiB7XG4gIG1hcmdpbjogMTJweDsgfVxuXG4ubS10LTEyIHtcbiAgbWFyZ2luLXRvcDogMTJweDsgfVxuXG4ubS1yLTEyIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMnB4OyB9XG5cbi5tLWItMTIge1xuICBtYXJnaW4tYm90dG9tOiAxMnB4OyB9XG5cbi5tLWwtMTIge1xuICBtYXJnaW4tbGVmdDogMTJweDsgfVxuXG4ubS14LTEyIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xuICBtYXJnaW4tbGVmdDogMTJweDsgfVxuXG4ubS15LTEyIHtcbiAgbWFyZ2luLXRvcDogMTJweDtcbiAgbWFyZ2luLWJvdHRvbTogMTJweDsgfVxuXG4ubS0xNSB7XG4gIG1hcmdpbjogMTVweDsgfVxuXG4ubS10LTE1IHtcbiAgbWFyZ2luLXRvcDogMTVweDsgfVxuXG4ubS1yLTE1IHtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4OyB9XG5cbi5tLWItMTUge1xuICBtYXJnaW4tYm90dG9tOiAxNXB4OyB9XG5cbi5tLWwtMTUge1xuICBtYXJnaW4tbGVmdDogMTVweDsgfVxuXG4ubS14LTE1IHtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICBtYXJnaW4tbGVmdDogMTVweDsgfVxuXG4ubS15LTE1IHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDsgfVxuXG4ubS0xNiB7XG4gIG1hcmdpbjogMTZweDsgfVxuXG4ubS10LTE2IHtcbiAgbWFyZ2luLXRvcDogMTZweDsgfVxuXG4ubS1yLTE2IHtcbiAgbWFyZ2luLXJpZ2h0OiAxNnB4OyB9XG5cbi5tLWItMTYge1xuICBtYXJnaW4tYm90dG9tOiAxNnB4OyB9XG5cbi5tLWwtMTYge1xuICBtYXJnaW4tbGVmdDogMTZweDsgfVxuXG4ubS14LTE2IHtcbiAgbWFyZ2luLXJpZ2h0OiAxNnB4O1xuICBtYXJnaW4tbGVmdDogMTZweDsgfVxuXG4ubS15LTE2IHtcbiAgbWFyZ2luLXRvcDogMTZweDtcbiAgbWFyZ2luLWJvdHRvbTogMTZweDsgfVxuXG4ubS0yMCB7XG4gIG1hcmdpbjogMjBweDsgfVxuXG4ubS10LTIwIHtcbiAgbWFyZ2luLXRvcDogMjBweDsgfVxuXG4ubS1yLTIwIHtcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4OyB9XG5cbi5tLWItMjAge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4OyB9XG5cbi5tLWwtMjAge1xuICBtYXJnaW4tbGVmdDogMjBweDsgfVxuXG4ubS14LTIwIHtcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xuICBtYXJnaW4tbGVmdDogMjBweDsgfVxuXG4ubS15LTIwIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDsgfVxuXG4ubS0yNCB7XG4gIG1hcmdpbjogMjRweDsgfVxuXG4ubS10LTI0IHtcbiAgbWFyZ2luLXRvcDogMjRweDsgfVxuXG4ubS1yLTI0IHtcbiAgbWFyZ2luLXJpZ2h0OiAyNHB4OyB9XG5cbi5tLWItMjQge1xuICBtYXJnaW4tYm90dG9tOiAyNHB4OyB9XG5cbi5tLWwtMjQge1xuICBtYXJnaW4tbGVmdDogMjRweDsgfVxuXG4ubS14LTI0IHtcbiAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xuICBtYXJnaW4tbGVmdDogMjRweDsgfVxuXG4ubS15LTI0IHtcbiAgbWFyZ2luLXRvcDogMjRweDtcbiAgbWFyZ2luLWJvdHRvbTogMjRweDsgfVxuXG4ubS0yNSB7XG4gIG1hcmdpbjogMjVweDsgfVxuXG4ubS10LTI1IHtcbiAgbWFyZ2luLXRvcDogMjVweDsgfVxuXG4ubS1yLTI1IHtcbiAgbWFyZ2luLXJpZ2h0OiAyNXB4OyB9XG5cbi5tLWItMjUge1xuICBtYXJnaW4tYm90dG9tOiAyNXB4OyB9XG5cbi5tLWwtMjUge1xuICBtYXJnaW4tbGVmdDogMjVweDsgfVxuXG4ubS14LTI1IHtcbiAgbWFyZ2luLXJpZ2h0OiAyNXB4O1xuICBtYXJnaW4tbGVmdDogMjVweDsgfVxuXG4ubS15LTI1IHtcbiAgbWFyZ2luLXRvcDogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogMjVweDsgfVxuXG4ubS0yOCB7XG4gIG1hcmdpbjogMjhweDsgfVxuXG4ubS10LTI4IHtcbiAgbWFyZ2luLXRvcDogMjhweDsgfVxuXG4ubS1yLTI4IHtcbiAgbWFyZ2luLXJpZ2h0OiAyOHB4OyB9XG5cbi5tLWItMjgge1xuICBtYXJnaW4tYm90dG9tOiAyOHB4OyB9XG5cbi5tLWwtMjgge1xuICBtYXJnaW4tbGVmdDogMjhweDsgfVxuXG4ubS14LTI4IHtcbiAgbWFyZ2luLXJpZ2h0OiAyOHB4O1xuICBtYXJnaW4tbGVmdDogMjhweDsgfVxuXG4ubS15LTI4IHtcbiAgbWFyZ2luLXRvcDogMjhweDtcbiAgbWFyZ2luLWJvdHRvbTogMjhweDsgfVxuXG4ubS0zMCB7XG4gIG1hcmdpbjogMzBweDsgfVxuXG4ubS10LTMwIHtcbiAgbWFyZ2luLXRvcDogMzBweDsgfVxuXG4ubS1yLTMwIHtcbiAgbWFyZ2luLXJpZ2h0OiAzMHB4OyB9XG5cbi5tLWItMzAge1xuICBtYXJnaW4tYm90dG9tOiAzMHB4OyB9XG5cbi5tLWwtMzAge1xuICBtYXJnaW4tbGVmdDogMzBweDsgfVxuXG4ubS14LTMwIHtcbiAgbWFyZ2luLXJpZ2h0OiAzMHB4O1xuICBtYXJnaW4tbGVmdDogMzBweDsgfVxuXG4ubS15LTMwIHtcbiAgbWFyZ2luLXRvcDogMzBweDtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDsgfVxuXG4ucC0wIHtcbiAgcGFkZGluZzogMHB4OyB9XG5cbi5wLXQtMCB7XG4gIHBhZGRpbmctdG9wOiAwcHg7IH1cblxuLnAtci0wIHtcbiAgcGFkZGluZy1yaWdodDogMHB4OyB9XG5cbi5wLWItMCB7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7IH1cblxuLnAtbC0wIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7IH1cblxuLnAteC0wIHtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweDsgfVxuXG4ucC15LTAge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4OyB9XG5cbi5wLTIge1xuICBwYWRkaW5nOiAycHg7IH1cblxuLnAtdC0yIHtcbiAgcGFkZGluZy10b3A6IDJweDsgfVxuXG4ucC1yLTIge1xuICBwYWRkaW5nLXJpZ2h0OiAycHg7IH1cblxuLnAtYi0yIHtcbiAgcGFkZGluZy1ib3R0b206IDJweDsgfVxuXG4ucC1sLTIge1xuICBwYWRkaW5nLWxlZnQ6IDJweDsgfVxuXG4ucC14LTIge1xuICBwYWRkaW5nLXJpZ2h0OiAycHg7XG4gIHBhZGRpbmctbGVmdDogMnB4OyB9XG5cbi5wLXktMiB7XG4gIHBhZGRpbmctdG9wOiAycHg7XG4gIHBhZGRpbmctYm90dG9tOiAycHg7IH1cblxuLnAtNCB7XG4gIHBhZGRpbmc6IDRweDsgfVxuXG4ucC10LTQge1xuICBwYWRkaW5nLXRvcDogNHB4OyB9XG5cbi5wLXItNCB7XG4gIHBhZGRpbmctcmlnaHQ6IDRweDsgfVxuXG4ucC1iLTQge1xuICBwYWRkaW5nLWJvdHRvbTogNHB4OyB9XG5cbi5wLWwtNCB7XG4gIHBhZGRpbmctbGVmdDogNHB4OyB9XG5cbi5wLXgtNCB7XG4gIHBhZGRpbmctcmlnaHQ6IDRweDtcbiAgcGFkZGluZy1sZWZ0OiA0cHg7IH1cblxuLnAteS00IHtcbiAgcGFkZGluZy10b3A6IDRweDtcbiAgcGFkZGluZy1ib3R0b206IDRweDsgfVxuXG4ucC01IHtcbiAgcGFkZGluZzogNXB4OyB9XG5cbi5wLXQtNSB7XG4gIHBhZGRpbmctdG9wOiA1cHg7IH1cblxuLnAtci01IHtcbiAgcGFkZGluZy1yaWdodDogNXB4OyB9XG5cbi5wLWItNSB7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7IH1cblxuLnAtbC01IHtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7IH1cblxuLnAteC01IHtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xuICBwYWRkaW5nLWxlZnQ6IDVweDsgfVxuXG4ucC15LTUge1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4OyB9XG5cbi5wLTgge1xuICBwYWRkaW5nOiA4cHg7IH1cblxuLnAtdC04IHtcbiAgcGFkZGluZy10b3A6IDhweDsgfVxuXG4ucC1yLTgge1xuICBwYWRkaW5nLXJpZ2h0OiA4cHg7IH1cblxuLnAtYi04IHtcbiAgcGFkZGluZy1ib3R0b206IDhweDsgfVxuXG4ucC1sLTgge1xuICBwYWRkaW5nLWxlZnQ6IDhweDsgfVxuXG4ucC14LTgge1xuICBwYWRkaW5nLXJpZ2h0OiA4cHg7XG4gIHBhZGRpbmctbGVmdDogOHB4OyB9XG5cbi5wLXktOCB7XG4gIHBhZGRpbmctdG9wOiA4cHg7XG4gIHBhZGRpbmctYm90dG9tOiA4cHg7IH1cblxuLnAtMTAge1xuICBwYWRkaW5nOiAxMHB4OyB9XG5cbi5wLXQtMTAge1xuICBwYWRkaW5nLXRvcDogMTBweDsgfVxuXG4ucC1yLTEwIHtcbiAgcGFkZGluZy1yaWdodDogMTBweDsgfVxuXG4ucC1iLTEwIHtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7IH1cblxuLnAtbC0xMCB7XG4gIHBhZGRpbmctbGVmdDogMTBweDsgfVxuXG4ucC14LTEwIHtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4OyB9XG5cbi5wLXktMTAge1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7IH1cblxuLnAtMTIge1xuICBwYWRkaW5nOiAxMnB4OyB9XG5cbi5wLXQtMTIge1xuICBwYWRkaW5nLXRvcDogMTJweDsgfVxuXG4ucC1yLTEyIHtcbiAgcGFkZGluZy1yaWdodDogMTJweDsgfVxuXG4ucC1iLTEyIHtcbiAgcGFkZGluZy1ib3R0b206IDEycHg7IH1cblxuLnAtbC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMTJweDsgfVxuXG4ucC14LTEyIHtcbiAgcGFkZGluZy1yaWdodDogMTJweDtcbiAgcGFkZGluZy1sZWZ0OiAxMnB4OyB9XG5cbi5wLXktMTIge1xuICBwYWRkaW5nLXRvcDogMTJweDtcbiAgcGFkZGluZy1ib3R0b206IDEycHg7IH1cblxuLnAtMTUge1xuICBwYWRkaW5nOiAxNXB4OyB9XG5cbi5wLXQtMTUge1xuICBwYWRkaW5nLXRvcDogMTVweDsgfVxuXG4ucC1yLTE1IHtcbiAgcGFkZGluZy1yaWdodDogMTVweDsgfVxuXG4ucC1iLTE1IHtcbiAgcGFkZGluZy1ib3R0b206IDE1cHg7IH1cblxuLnAtbC0xNSB7XG4gIHBhZGRpbmctbGVmdDogMTVweDsgfVxuXG4ucC14LTE1IHtcbiAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4OyB9XG5cbi5wLXktMTUge1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDE1cHg7IH1cblxuLnAtMTYge1xuICBwYWRkaW5nOiAxNnB4OyB9XG5cbi5wLXQtMTYge1xuICBwYWRkaW5nLXRvcDogMTZweDsgfVxuXG4ucC1yLTE2IHtcbiAgcGFkZGluZy1yaWdodDogMTZweDsgfVxuXG4ucC1iLTE2IHtcbiAgcGFkZGluZy1ib3R0b206IDE2cHg7IH1cblxuLnAtbC0xNiB7XG4gIHBhZGRpbmctbGVmdDogMTZweDsgfVxuXG4ucC14LTE2IHtcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4OyB9XG5cbi5wLXktMTYge1xuICBwYWRkaW5nLXRvcDogMTZweDtcbiAgcGFkZGluZy1ib3R0b206IDE2cHg7IH1cblxuLnAtMjAge1xuICBwYWRkaW5nOiAyMHB4OyB9XG5cbi5wLXQtMjAge1xuICBwYWRkaW5nLXRvcDogMjBweDsgfVxuXG4ucC1yLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDsgfVxuXG4ucC1iLTIwIHtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7IH1cblxuLnAtbC0yMCB7XG4gIHBhZGRpbmctbGVmdDogMjBweDsgfVxuXG4ucC14LTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4OyB9XG5cbi5wLXktMjAge1xuICBwYWRkaW5nLXRvcDogMjBweDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7IH1cblxuLnAtMjQge1xuICBwYWRkaW5nOiAyNHB4OyB9XG5cbi5wLXQtMjQge1xuICBwYWRkaW5nLXRvcDogMjRweDsgfVxuXG4ucC1yLTI0IHtcbiAgcGFkZGluZy1yaWdodDogMjRweDsgfVxuXG4ucC1iLTI0IHtcbiAgcGFkZGluZy1ib3R0b206IDI0cHg7IH1cblxuLnAtbC0yNCB7XG4gIHBhZGRpbmctbGVmdDogMjRweDsgfVxuXG4ucC14LTI0IHtcbiAgcGFkZGluZy1yaWdodDogMjRweDtcbiAgcGFkZGluZy1sZWZ0OiAyNHB4OyB9XG5cbi5wLXktMjQge1xuICBwYWRkaW5nLXRvcDogMjRweDtcbiAgcGFkZGluZy1ib3R0b206IDI0cHg7IH1cblxuLnAtMjUge1xuICBwYWRkaW5nOiAyNXB4OyB9XG5cbi5wLXQtMjUge1xuICBwYWRkaW5nLXRvcDogMjVweDsgfVxuXG4ucC1yLTI1IHtcbiAgcGFkZGluZy1yaWdodDogMjVweDsgfVxuXG4ucC1iLTI1IHtcbiAgcGFkZGluZy1ib3R0b206IDI1cHg7IH1cblxuLnAtbC0yNSB7XG4gIHBhZGRpbmctbGVmdDogMjVweDsgfVxuXG4ucC14LTI1IHtcbiAgcGFkZGluZy1yaWdodDogMjVweDtcbiAgcGFkZGluZy1sZWZ0OiAyNXB4OyB9XG5cbi5wLXktMjUge1xuICBwYWRkaW5nLXRvcDogMjVweDtcbiAgcGFkZGluZy1ib3R0b206IDI1cHg7IH1cblxuLnAtMjgge1xuICBwYWRkaW5nOiAyOHB4OyB9XG5cbi5wLXQtMjgge1xuICBwYWRkaW5nLXRvcDogMjhweDsgfVxuXG4ucC1yLTI4IHtcbiAgcGFkZGluZy1yaWdodDogMjhweDsgfVxuXG4ucC1iLTI4IHtcbiAgcGFkZGluZy1ib3R0b206IDI4cHg7IH1cblxuLnAtbC0yOCB7XG4gIHBhZGRpbmctbGVmdDogMjhweDsgfVxuXG4ucC14LTI4IHtcbiAgcGFkZGluZy1yaWdodDogMjhweDtcbiAgcGFkZGluZy1sZWZ0OiAyOHB4OyB9XG5cbi5wLXktMjgge1xuICBwYWRkaW5nLXRvcDogMjhweDtcbiAgcGFkZGluZy1ib3R0b206IDI4cHg7IH1cblxuLnAtMzAge1xuICBwYWRkaW5nOiAzMHB4OyB9XG5cbi5wLXQtMzAge1xuICBwYWRkaW5nLXRvcDogMzBweDsgfVxuXG4ucC1yLTMwIHtcbiAgcGFkZGluZy1yaWdodDogMzBweDsgfVxuXG4ucC1iLTMwIHtcbiAgcGFkZGluZy1ib3R0b206IDMwcHg7IH1cblxuLnAtbC0zMCB7XG4gIHBhZGRpbmctbGVmdDogMzBweDsgfVxuXG4ucC14LTMwIHtcbiAgcGFkZGluZy1yaWdodDogMzBweDtcbiAgcGFkZGluZy1sZWZ0OiAzMHB4OyB9XG5cbi5wLXktMzAge1xuICBwYWRkaW5nLXRvcDogMzBweDtcbiAgcGFkZGluZy1ib3R0b206IDMwcHg7IH1cblxuLyoqXG4gKiBFbGVtZW50IHRhZyBvdmVycmlkZXNcbiAqL1xuYm9keSB7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMDsgfVxuXG4vKipcbiAqIFRoZSBmb2xsb3dpbmcgYXJlIElvbmljIHNwZWNpZmljXG4gKi9cbmJvZHkge1xuICBwYWRkaW5nLXRvcDogY29uc3RhbnQoc2FmZS1hcmVhLWluc2V0LXRvcCk7XG4gIHBhZGRpbmctdG9wOiBlbnYoc2FmZS1hcmVhLWluc2V0LXRvcCk7IH1cbiIsIi8qKlxuICogU2hhcmVkIGFjcm9zcyBhbGwgcGxhdGZvcm1zIGFuZCBhcHBzXG4gKiBZb3UgbWF5IGRlZmluZSBhIHNldCBvZiBnbG9iYWwgdmFyaWFibGVzIGFjY2Vzc2libGUgYWNyb3NzIGVudGlyZSB3b3Jrc3BhY2UgaGVyZVxuICovXG5cbi8vIGNvdWxkIHVzZSBhIGJhc2UgdGhlbWUgaGVyZSwgZm9yIGV4YW1wbGU6XG4vLyBAaW1wb3J0ICd+QGFuZ3VsYXIvbWF0ZXJpYWwvdGhlbWluZyc7XG4iLCIvLyBzaGFyZWQgYWNyb3NzIGFsbCBwbGF0Zm9ybXMgYW5kIGFwcHNcbkBpbXBvcnQgJ35AbWFsb21hdGlhL3Njc3MvaW5kZXgnO1xuXG4vKipcbiAqIFRoZSBmb2xsb3dpbmcgYXJlIHdlYiBzcGVjaWZpYyAodXNlZCB3aXRoIGFueSB3ZWIgYXBwIHRhcmdldHMpXG4gKi9cblxuLy8gd2ViIHNwZWNpZmljIHZhcmlhYmxlc1xuQGltcG9ydCAndmFyaWFibGVzJztcblxuLy8gd2ViIHN0eWxlcyAoY3JlYXRlL2ltcG9ydCBvdGhlciBzY3NzIGZpbGVzIG9yIGRlZmluZSBhcyBuZWVkZWQpXG5AaW1wb3J0ICdzcGFjaW5nJztcbkBpbXBvcnQgJ3RhZ3MnO1xuIiwiLyoqXG4gKiBDb252ZW5pZW50IHNwYWNpbmcgY2xhc3Nlc1xuICovXG4kc3BhY2VyOiA1ICFkZWZhdWx0O1xuJHNwYWNlci14OiAkc3BhY2VyICFkZWZhdWx0O1xuJHNwYWNlci15OiAkc3BhY2VyICFkZWZhdWx0O1xuJHNwYWNlci1hbHQ6IDQgIWRlZmF1bHQ7XG4kc3BhY2VyLXgtYWx0OiAkc3BhY2VyLWFsdCAhZGVmYXVsdDtcbiRzcGFjZXIteS1hbHQ6ICRzcGFjZXItYWx0ICFkZWZhdWx0O1xuJHNwYWNlcnM6IChcbiAgMDogKFxuICAgIHg6IDAsXG4gICAgeTogMFxuICApLFxuICAyOiAoXG4gICAgeDogMixcbiAgICB5OiAyXG4gICksXG4gIDQ6IChcbiAgICB4OiAkc3BhY2VyLXgtYWx0LFxuICAgIHk6ICRzcGFjZXIteS1hbHRcbiAgKSxcbiAgNTogKFxuICAgIHg6ICRzcGFjZXIteCxcbiAgICB5OiAkc3BhY2VyLXlcbiAgKSxcbiAgODogKFxuICAgIHg6ICRzcGFjZXIteC1hbHQgKiAyLFxuICAgIHk6ICRzcGFjZXIteS1hbHQgKiAyXG4gICksXG4gIDEwOiAoXG4gICAgeDogKFxuICAgICAgJHNwYWNlci14ICogMlxuICAgICksXG4gICAgeTogKFxuICAgICAgJHNwYWNlci15ICogMlxuICAgIClcbiAgKSxcbiAgMTI6IChcbiAgICB4OiAkc3BhY2VyLXgtYWx0ICogMyxcbiAgICB5OiAkc3BhY2VyLXktYWx0ICogM1xuICApLFxuICAxNTogKFxuICAgIHg6IChcbiAgICAgICRzcGFjZXIteCAqIDNcbiAgICApLFxuICAgIHk6IChcbiAgICAgICRzcGFjZXIteSAqIDNcbiAgICApXG4gICksXG4gIDE2OiAoXG4gICAgeDogJHNwYWNlci14LWFsdCAqIDQsXG4gICAgeTogJHNwYWNlci15LWFsdCAqIDRcbiAgKSxcbiAgMjA6IChcbiAgICB4OiAoXG4gICAgICAkc3BhY2VyLXggKiA0XG4gICAgKSxcbiAgICB5OiAoXG4gICAgICAkc3BhY2VyLXkgKiA0XG4gICAgKVxuICApLFxuICAyNDogKFxuICAgIHg6ICRzcGFjZXIteC1hbHQgKiA2LFxuICAgIHk6ICRzcGFjZXIteS1hbHQgKiA2XG4gICksXG4gIDI1OiAoXG4gICAgeDogKFxuICAgICAgJHNwYWNlci14ICogNVxuICAgICksXG4gICAgeTogKFxuICAgICAgJHNwYWNlci15ICogNVxuICAgIClcbiAgKSxcbiAgMjg6IChcbiAgICB4OiAkc3BhY2VyLXgtYWx0ICogNyxcbiAgICB5OiAkc3BhY2VyLXktYWx0ICogN1xuICApLFxuICAzMDogKFxuICAgIHg6IChcbiAgICAgICRzcGFjZXIteCAqIDZcbiAgICApLFxuICAgIHk6IChcbiAgICAgICRzcGFjZXIteSAqIDZcbiAgICApXG4gIClcbikgIWRlZmF1bHQ7XG5cbi8qKlxuICogTWFyZ2luIGFuZCBQYWRkaW5nXG4gKiBUaGUgZm9sbG93aW5nIGNyZWF0ZXMgdGhpcyBwYXR0ZXJuOlxuICogLm0tMHttYXJnaW46MH0ubS10LTB7bWFyZ2luLXRvcDowfS5tLXItMHttYXJnaW4tcmlnaHQ6MH0ubS1iLTB7bWFyZ2luLWJvdHRvbTowfS5tLWwtMHttYXJnaW4tbGVmdDowfS5tLXgtMHttYXJnaW4tcmlnaHQ6MDttYXJnaW4tbGVmdDowfS5tLXktMHttYXJnaW4tdG9wOjA7bWFyZ2luLWJvdHRvbTowfVxuICogU2FtZSBmb3IgUGFkZGluZyAodXNpbmcgdGhlICdwJyBhYmJyZXZpYXRpb24pXG4gKiBGcm9tIDAsIDIsIDUsIDEwLCAxNSwgMjAsIDI1LCAzMFxuKiovXG4vLyBzYXNzLWxpbnQ6ZGlzYWJsZS1hbGxcbkBlYWNoICRwcm9wLCAkYWJicmV2IGluIChtYXJnaW46IG0sIHBhZGRpbmc6IHApIHtcbiAgLy8gc2Fzcy1saW50OmVuYWJsZS1hbGxcbiAgQGVhY2ggJHNpemUsICRsZW5ndGhzIGluICRzcGFjZXJzIHtcbiAgICAkbGVuZ3RoLXg6IG1hcC1nZXQoJGxlbmd0aHMsIHgpO1xuICAgICRsZW5ndGgteTogbWFwLWdldCgkbGVuZ3RocywgeSk7XG5cbiAgICAvLyBzYXNzLWxpbnQ6ZGlzYWJsZS1hbGxcbiAgICAuI3skYWJicmV2fS0jeyRzaXplfSB7XG4gICAgICAjeyRwcm9wfTogI3skbGVuZ3RoLXl9cHg7XG4gICAgfSAvLyBhID0gQWxsIHNpZGVzIChjYW4ganVzdCB1c2Ugb25lIGxlbmd0aClcbiAgICAvLyBzYXNzLWxpbnQ6ZW5hYmxlLWFsbFxuICAgIC4jeyRhYmJyZXZ9LXQtI3skc2l6ZX0ge1xuICAgICAgI3skcHJvcH0tdG9wOiAjeyRsZW5ndGgteX1weDtcbiAgICB9XG4gICAgLiN7JGFiYnJldn0tci0jeyRzaXplfSB7XG4gICAgICAjeyRwcm9wfS1yaWdodDogI3skbGVuZ3RoLXh9cHg7XG4gICAgfVxuICAgIC4jeyRhYmJyZXZ9LWItI3skc2l6ZX0ge1xuICAgICAgI3skcHJvcH0tYm90dG9tOiAjeyRsZW5ndGgteX1weDtcbiAgICB9XG4gICAgLiN7JGFiYnJldn0tbC0jeyRzaXplfSB7XG4gICAgICAjeyRwcm9wfS1sZWZ0OiAjeyRsZW5ndGgteH1weDtcbiAgICB9XG5cbiAgICAvLyBBeGVzXG4gICAgLiN7JGFiYnJldn0teC0jeyRzaXplfSB7XG4gICAgICAjeyRwcm9wfS1yaWdodDogI3skbGVuZ3RoLXh9cHg7XG4gICAgICAjeyRwcm9wfS1sZWZ0OiAjeyRsZW5ndGgteH1weDtcbiAgICB9XG5cbiAgICAuI3skYWJicmV2fS15LSN7JHNpemV9IHtcbiAgICAgICN7JHByb3B9LXRvcDogI3skbGVuZ3RoLXl9cHg7XG4gICAgICAjeyRwcm9wfS1ib3R0b206ICN7JGxlbmd0aC15fXB4O1xuICAgIH1cbiAgfVxufVxuIiwiaHRtbC5pb3N7LS1pb24tZGVmYXVsdC1mb250OiAtYXBwbGUtc3lzdGVtLCBCbGlua01hY1N5c3RlbUZvbnQsIFwiSGVsdmV0aWNhIE5ldWVcIiwgXCJSb2JvdG9cIiwgc2Fucy1zZXJpZn1odG1sLm1key0taW9uLWRlZmF1bHQtZm9udDogXCJSb2JvdG9cIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBzYW5zLXNlcmlmfWh0bWx7LS1pb24tZm9udC1mYW1pbHk6IHZhcigtLWlvbi1kZWZhdWx0LWZvbnQpfWJvZHl7YmFja2dyb3VuZDp2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcil9Ym9keS5iYWNrZHJvcC1uby1zY3JvbGx7b3ZlcmZsb3c6aGlkZGVufS5pb24tY29sb3ItcHJpbWFyeXstLWlvbi1jb2xvci1iYXNlOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSwgIzM4ODBmZikgIWltcG9ydGFudDstLWlvbi1jb2xvci1iYXNlLXJnYjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnktcmdiLCA1NiwxMjgsMjU1KSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLWNvbnRyYXN0OiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeS1jb250cmFzdCwgI2ZmZikgIWltcG9ydGFudDstLWlvbi1jb2xvci1jb250cmFzdC1yZ2I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LWNvbnRyYXN0LXJnYiwgMjU1LDI1NSwyNTUpICFpbXBvcnRhbnQ7LS1pb24tY29sb3Itc2hhZGU6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LXNoYWRlLCAjMzE3MWUwKSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLXRpbnQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LXRpbnQsICM0YzhkZmYpICFpbXBvcnRhbnR9Lmlvbi1jb2xvci1zZWNvbmRhcnl7LS1pb24tY29sb3ItYmFzZTogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSwgIzBjZDFlOCkgIWltcG9ydGFudDstLWlvbi1jb2xvci1iYXNlLXJnYjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeS1yZ2IsIDEyLDIwOSwyMzIpICFpbXBvcnRhbnQ7LS1pb24tY29sb3ItY29udHJhc3Q6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnktY29udHJhc3QsICNmZmYpICFpbXBvcnRhbnQ7LS1pb24tY29sb3ItY29udHJhc3QtcmdiOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5LWNvbnRyYXN0LXJnYiwgMjU1LDI1NSwyNTUpICFpbXBvcnRhbnQ7LS1pb24tY29sb3Itc2hhZGU6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnktc2hhZGUsICMwYmI4Y2MpICFpbXBvcnRhbnQ7LS1pb24tY29sb3ItdGludDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeS10aW50LCAjMjRkNmVhKSAhaW1wb3J0YW50fS5pb24tY29sb3ItdGVydGlhcnl7LS1pb24tY29sb3ItYmFzZTogdmFyKC0taW9uLWNvbG9yLXRlcnRpYXJ5LCAjNzA0NGZmKSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLWJhc2UtcmdiOiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnktcmdiLCAxMTIsNjgsMjU1KSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLWNvbnRyYXN0OiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnktY29udHJhc3QsICNmZmYpICFpbXBvcnRhbnQ7LS1pb24tY29sb3ItY29udHJhc3QtcmdiOiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnktY29udHJhc3QtcmdiLCAyNTUsMjU1LDI1NSkgIWltcG9ydGFudDstLWlvbi1jb2xvci1zaGFkZTogdmFyKC0taW9uLWNvbG9yLXRlcnRpYXJ5LXNoYWRlLCAjNjMzY2UwKSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLXRpbnQ6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeS10aW50LCAjN2U1N2ZmKSAhaW1wb3J0YW50fS5pb24tY29sb3Itc3VjY2Vzc3stLWlvbi1jb2xvci1iYXNlOiB2YXIoLS1pb24tY29sb3Itc3VjY2VzcywgIzEwZGM2MCkgIWltcG9ydGFudDstLWlvbi1jb2xvci1iYXNlLXJnYjogdmFyKC0taW9uLWNvbG9yLXN1Y2Nlc3MtcmdiLCAxNiwyMjAsOTYpICFpbXBvcnRhbnQ7LS1pb24tY29sb3ItY29udHJhc3Q6IHZhcigtLWlvbi1jb2xvci1zdWNjZXNzLWNvbnRyYXN0LCAjZmZmKSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLWNvbnRyYXN0LXJnYjogdmFyKC0taW9uLWNvbG9yLXN1Y2Nlc3MtY29udHJhc3QtcmdiLCAyNTUsMjU1LDI1NSkgIWltcG9ydGFudDstLWlvbi1jb2xvci1zaGFkZTogdmFyKC0taW9uLWNvbG9yLXN1Y2Nlc3Mtc2hhZGUsICMwZWMyNTQpICFpbXBvcnRhbnQ7LS1pb24tY29sb3ItdGludDogdmFyKC0taW9uLWNvbG9yLXN1Y2Nlc3MtdGludCwgIzI4ZTA3MCkgIWltcG9ydGFudH0uaW9uLWNvbG9yLXdhcm5pbmd7LS1pb24tY29sb3ItYmFzZTogdmFyKC0taW9uLWNvbG9yLXdhcm5pbmcsICNmZmNlMDApICFpbXBvcnRhbnQ7LS1pb24tY29sb3ItYmFzZS1yZ2I6IHZhcigtLWlvbi1jb2xvci13YXJuaW5nLXJnYiwgMjU1LDIwNiwwKSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLWNvbnRyYXN0OiB2YXIoLS1pb24tY29sb3Itd2FybmluZy1jb250cmFzdCwgI2ZmZikgIWltcG9ydGFudDstLWlvbi1jb2xvci1jb250cmFzdC1yZ2I6IHZhcigtLWlvbi1jb2xvci13YXJuaW5nLWNvbnRyYXN0LXJnYiwgMjU1LDI1NSwyNTUpICFpbXBvcnRhbnQ7LS1pb24tY29sb3Itc2hhZGU6IHZhcigtLWlvbi1jb2xvci13YXJuaW5nLXNoYWRlLCAjZTBiNTAwKSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLXRpbnQ6IHZhcigtLWlvbi1jb2xvci13YXJuaW5nLXRpbnQsICNmZmQzMWEpICFpbXBvcnRhbnR9Lmlvbi1jb2xvci1kYW5nZXJ7LS1pb24tY29sb3ItYmFzZTogdmFyKC0taW9uLWNvbG9yLWRhbmdlciwgI2YwNDE0MSkgIWltcG9ydGFudDstLWlvbi1jb2xvci1iYXNlLXJnYjogdmFyKC0taW9uLWNvbG9yLWRhbmdlci1yZ2IsIDI0MCw2NSw2NSkgIWltcG9ydGFudDstLWlvbi1jb2xvci1jb250cmFzdDogdmFyKC0taW9uLWNvbG9yLWRhbmdlci1jb250cmFzdCwgI2ZmZikgIWltcG9ydGFudDstLWlvbi1jb2xvci1jb250cmFzdC1yZ2I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXItY29udHJhc3QtcmdiLCAyNTUsMjU1LDI1NSkgIWltcG9ydGFudDstLWlvbi1jb2xvci1zaGFkZTogdmFyKC0taW9uLWNvbG9yLWRhbmdlci1zaGFkZSwgI2QzMzkzOSkgIWltcG9ydGFudDstLWlvbi1jb2xvci10aW50OiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyLXRpbnQsICNmMjU0NTQpICFpbXBvcnRhbnR9Lmlvbi1jb2xvci1saWdodHstLWlvbi1jb2xvci1iYXNlOiB2YXIoLS1pb24tY29sb3ItbGlnaHQsICNmNGY1ZjgpICFpbXBvcnRhbnQ7LS1pb24tY29sb3ItYmFzZS1yZ2I6IHZhcigtLWlvbi1jb2xvci1saWdodC1yZ2IsIDI0NCwyNDUsMjQ4KSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLWNvbnRyYXN0OiB2YXIoLS1pb24tY29sb3ItbGlnaHQtY29udHJhc3QsICMwMDApICFpbXBvcnRhbnQ7LS1pb24tY29sb3ItY29udHJhc3QtcmdiOiB2YXIoLS1pb24tY29sb3ItbGlnaHQtY29udHJhc3QtcmdiLCAwLDAsMCkgIWltcG9ydGFudDstLWlvbi1jb2xvci1zaGFkZTogdmFyKC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlLCAjZDdkOGRhKSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLXRpbnQ6IHZhcigtLWlvbi1jb2xvci1saWdodC10aW50LCAjZjVmNmY5KSAhaW1wb3J0YW50fS5pb24tY29sb3ItbWVkaXVtey0taW9uLWNvbG9yLWJhc2U6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0sICM5ODlhYTIpICFpbXBvcnRhbnQ7LS1pb24tY29sb3ItYmFzZS1yZ2I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tcmdiLCAxNTIsMTU0LDE2MikgIWltcG9ydGFudDstLWlvbi1jb2xvci1jb250cmFzdDogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1jb250cmFzdCwgI2ZmZikgIWltcG9ydGFudDstLWlvbi1jb2xvci1jb250cmFzdC1yZ2I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tY29udHJhc3QtcmdiLCAyNTUsMjU1LDI1NSkgIWltcG9ydGFudDstLWlvbi1jb2xvci1zaGFkZTogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1zaGFkZSwgIzg2ODg4ZikgIWltcG9ydGFudDstLWlvbi1jb2xvci10aW50OiB2YXIoLS1pb24tY29sb3ItbWVkaXVtLXRpbnQsICNhMmE0YWIpICFpbXBvcnRhbnR9Lmlvbi1jb2xvci1kYXJrey0taW9uLWNvbG9yLWJhc2U6IHZhcigtLWlvbi1jb2xvci1kYXJrLCAjMjIyNDI4KSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLWJhc2UtcmdiOiB2YXIoLS1pb24tY29sb3ItZGFyay1yZ2IsIDM0LDM2LDQwKSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLWNvbnRyYXN0OiB2YXIoLS1pb24tY29sb3ItZGFyay1jb250cmFzdCwgI2ZmZikgIWltcG9ydGFudDstLWlvbi1jb2xvci1jb250cmFzdC1yZ2I6IHZhcigtLWlvbi1jb2xvci1kYXJrLWNvbnRyYXN0LXJnYiwgMjU1LDI1NSwyNTUpICFpbXBvcnRhbnQ7LS1pb24tY29sb3Itc2hhZGU6IHZhcigtLWlvbi1jb2xvci1kYXJrLXNoYWRlLCAjMWUyMDIzKSAhaW1wb3J0YW50Oy0taW9uLWNvbG9yLXRpbnQ6IHZhcigtLWlvbi1jb2xvci1kYXJrLXRpbnQsICMzODNhM2UpICFpbXBvcnRhbnR9Lmlvbi1wYWdle2xlZnQ6MDtyaWdodDowO3RvcDowO2JvdHRvbTowO2Rpc3BsYXk6ZmxleDtwb3NpdGlvbjphYnNvbHV0ZTtmbGV4LWRpcmVjdGlvbjpjb2x1bW47anVzdGlmeS1jb250ZW50OnNwYWNlLWJldHdlZW47Y29udGFpbjpsYXlvdXQgc2l6ZSBzdHlsZTtvdmVyZmxvdzpoaWRkZW47ei1pbmRleDowfWlvbi1yb3V0ZSxpb24tcm91dGUtcmVkaXJlY3QsaW9uLXJvdXRlcixpb24tc2VsZWN0LW9wdGlvbixpb24tbmF2LWNvbnRyb2xsZXIsaW9uLW1lbnUtY29udHJvbGxlcixpb24tYWN0aW9uLXNoZWV0LWNvbnRyb2xsZXIsaW9uLWFsZXJ0LWNvbnRyb2xsZXIsaW9uLWxvYWRpbmctY29udHJvbGxlcixpb24tbW9kYWwtY29udHJvbGxlcixpb24tcGlja2VyLWNvbnRyb2xsZXIsaW9uLXBvcG92ZXItY29udHJvbGxlcixpb24tdG9hc3QtY29udHJvbGxlciwuaW9uLXBhZ2UtaGlkZGVuLFtoaWRkZW5de2Rpc3BsYXk6bm9uZSAhaW1wb3J0YW50fS5pb24tcGFnZS1pbnZpc2libGV7b3BhY2l0eTowfWh0bWwucGx0LWlvcy5wbHQtaHlicmlkLGh0bWwucGx0LWlvcy5wbHQtcHdhey0taW9uLXN0YXR1c2Jhci1wYWRkaW5nOiAyMHB4fUBzdXBwb3J0cyAocGFkZGluZy10b3A6IDIwcHgpe2h0bWx7LS1pb24tc2FmZS1hcmVhLXRvcDogdmFyKC0taW9uLXN0YXR1c2Jhci1wYWRkaW5nKX19QHN1cHBvcnRzIChwYWRkaW5nLXRvcDogY29uc3RhbnQoc2FmZS1hcmVhLWluc2V0LXRvcCkpe2h0bWx7LS1pb24tc2FmZS1hcmVhLXRvcDogY29uc3RhbnQoc2FmZS1hcmVhLWluc2V0LXRvcCk7LS1pb24tc2FmZS1hcmVhLWJvdHRvbTogY29uc3RhbnQoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7LS1pb24tc2FmZS1hcmVhLWxlZnQ6IGNvbnN0YW50KHNhZmUtYXJlYS1pbnNldC1sZWZ0KTstLWlvbi1zYWZlLWFyZWEtcmlnaHQ6IGNvbnN0YW50KHNhZmUtYXJlYS1pbnNldC1yaWdodCl9fUBzdXBwb3J0cyAocGFkZGluZy10b3A6IGVudihzYWZlLWFyZWEtaW5zZXQtdG9wKSl7aHRtbHstLWlvbi1zYWZlLWFyZWEtdG9wOiBlbnYoc2FmZS1hcmVhLWluc2V0LXRvcCk7LS1pb24tc2FmZS1hcmVhLWJvdHRvbTogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pOy0taW9uLXNhZmUtYXJlYS1sZWZ0OiBlbnYoc2FmZS1hcmVhLWluc2V0LWxlZnQpOy0taW9uLXNhZmUtYXJlYS1yaWdodDogZW52KHNhZmUtYXJlYS1pbnNldC1yaWdodCl9fVxuIiwiYXVkaW8sY2FudmFzLHByb2dyZXNzLHZpZGVve3ZlcnRpY2FsLWFsaWduOmJhc2VsaW5lfWF1ZGlvOm5vdChbY29udHJvbHNdKXtkaXNwbGF5Om5vbmU7aGVpZ2h0OjB9YixzdHJvbmd7Zm9udC13ZWlnaHQ6Ym9sZH1pbWd7bWF4LXdpZHRoOjEwMCU7Ym9yZGVyOjB9c3ZnOm5vdCg6cm9vdCl7b3ZlcmZsb3c6aGlkZGVufWZpZ3VyZXttYXJnaW46MWVtIDQwcHh9aHJ7aGVpZ2h0OjFweDtib3JkZXItd2lkdGg6MDtib3gtc2l6aW5nOmNvbnRlbnQtYm94fXByZXtvdmVyZmxvdzphdXRvfWNvZGUsa2JkLHByZSxzYW1we2ZvbnQtZmFtaWx5Om1vbm9zcGFjZSwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxZW19bGFiZWwsaW5wdXQsc2VsZWN0LHRleHRhcmVhe2ZvbnQtZmFtaWx5OmluaGVyaXQ7bGluZS1oZWlnaHQ6bm9ybWFsfXRleHRhcmVhe292ZXJmbG93OmF1dG87aGVpZ2h0OmF1dG87Zm9udDppbmhlcml0O2NvbG9yOmluaGVyaXR9dGV4dGFyZWE6OnBsYWNlaG9sZGVye3BhZGRpbmctbGVmdDoycHh9Zm9ybSxpbnB1dCxvcHRncm91cCxzZWxlY3R7bWFyZ2luOjA7Zm9udDppbmhlcml0O2NvbG9yOmluaGVyaXR9aHRtbCBpbnB1dFt0eXBlPVwiYnV0dG9uXCJdLGlucHV0W3R5cGU9XCJyZXNldFwiXSxpbnB1dFt0eXBlPVwic3VibWl0XCJde2N1cnNvcjpwb2ludGVyOy13ZWJraXQtYXBwZWFyYW5jZTpidXR0b259YSxhIGRpdixhIHNwYW4sYSBpb24taWNvbixhIGlvbi1sYWJlbCxidXR0b24sYnV0dG9uIGRpdixidXR0b24gc3BhbixidXR0b24gaW9uLWljb24sYnV0dG9uIGlvbi1sYWJlbCwuaW9uLXRhcHBhYmxlLFt0YXBwYWJsZV0sW3RhcHBhYmxlXSBkaXYsW3RhcHBhYmxlXSBzcGFuLFt0YXBwYWJsZV0gaW9uLWljb24sW3RhcHBhYmxlXSBpb24tbGFiZWwsaW5wdXQsdGV4dGFyZWF7dG91Y2gtYWN0aW9uOm1hbmlwdWxhdGlvbn1hIGlvbi1sYWJlbCxidXR0b24gaW9uLWxhYmVse3BvaW50ZXItZXZlbnRzOm5vbmV9YnV0dG9ue2JvcmRlcjowO2JvcmRlci1yYWRpdXM6MDtmb250LWZhbWlseTppbmhlcml0O2ZvbnQtc3R5bGU6aW5oZXJpdDtmb250LXZhcmlhbnQ6aW5oZXJpdDtsaW5lLWhlaWdodDoxO3RleHQtdHJhbnNmb3JtOm5vbmU7Y3Vyc29yOnBvaW50ZXI7LXdlYmtpdC1hcHBlYXJhbmNlOmJ1dHRvbn1bdGFwcGFibGVde2N1cnNvcjpwb2ludGVyfWFbZGlzYWJsZWRdLGJ1dHRvbltkaXNhYmxlZF0saHRtbCBpbnB1dFtkaXNhYmxlZF17Y3Vyc29yOmRlZmF1bHR9YnV0dG9uOjotbW96LWZvY3VzLWlubmVyLGlucHV0OjotbW96LWZvY3VzLWlubmVye3BhZGRpbmc6MDtib3JkZXI6MH1pbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0saW5wdXRbdHlwZT1cInJhZGlvXCJde3BhZGRpbmc6MDtib3gtc2l6aW5nOmJvcmRlci1ib3h9aW5wdXRbdHlwZT1cIm51bWJlclwiXTo6LXdlYmtpdC1pbm5lci1zcGluLWJ1dHRvbixpbnB1dFt0eXBlPVwibnVtYmVyXCJdOjotd2Via2l0LW91dGVyLXNwaW4tYnV0dG9ue2hlaWdodDphdXRvfWlucHV0W3R5cGU9XCJzZWFyY2hcIl06Oi13ZWJraXQtc2VhcmNoLWNhbmNlbC1idXR0b24saW5wdXRbdHlwZT1cInNlYXJjaFwiXTo6LXdlYmtpdC1zZWFyY2gtZGVjb3JhdGlvbnstd2Via2l0LWFwcGVhcmFuY2U6bm9uZX10YWJsZXtib3JkZXItY29sbGFwc2U6Y29sbGFwc2U7Ym9yZGVyLXNwYWNpbmc6MH10ZCx0aHtwYWRkaW5nOjB9XG4iLCIqe2JveC1zaXppbmc6Ym9yZGVyLWJveDstd2Via2l0LXRhcC1oaWdobGlnaHQtY29sb3I6cmdiYSgwLDAsMCwwKTstd2Via2l0LXRhcC1oaWdobGlnaHQtY29sb3I6dHJhbnNwYXJlbnQ7LXdlYmtpdC10b3VjaC1jYWxsb3V0Om5vbmV9aHRtbHt3aWR0aDoxMDAlO2hlaWdodDoxMDAlO3RleHQtc2l6ZS1hZGp1c3Q6MTAwJX1odG1sLnBsdC1wd2F7aGVpZ2h0OjEwMHZofWJvZHl7LW1vei1vc3gtZm9udC1zbW9vdGhpbmc6Z3JheXNjYWxlOy13ZWJraXQtZm9udC1zbW9vdGhpbmc6YW50aWFsaWFzZWQ7bWFyZ2luLWxlZnQ6MDttYXJnaW4tcmlnaHQ6MDttYXJnaW4tdG9wOjA7bWFyZ2luLWJvdHRvbTowO3BhZGRpbmctbGVmdDowO3BhZGRpbmctcmlnaHQ6MDtwYWRkaW5nLXRvcDowO3BhZGRpbmctYm90dG9tOjA7cG9zaXRpb246Zml4ZWQ7d2lkdGg6MTAwJTttYXgtd2lkdGg6MTAwJTtoZWlnaHQ6MTAwJTttYXgtaGVpZ2h0OjEwMCU7dGV4dC1yZW5kZXJpbmc6b3B0aW1pemVMZWdpYmlsaXR5O292ZXJmbG93OmhpZGRlbjt0b3VjaC1hY3Rpb246bWFuaXB1bGF0aW9uOy13ZWJraXQtdXNlci1kcmFnOm5vbmU7LW1zLWNvbnRlbnQtem9vbWluZzpub25lO3dvcmQtd3JhcDpicmVhay13b3JkO292ZXJzY3JvbGwtYmVoYXZpb3IteTpub25lO3RleHQtc2l6ZS1hZGp1c3Q6bm9uZX1cbiIsImh0bWx7Zm9udC1mYW1pbHk6dmFyKC0taW9uLWZvbnQtZmFtaWx5KX1he2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7Y29sb3I6dmFyKC0taW9uLWNvbG9yLXByaW1hcnksICMzODgwZmYpfWgxLGgyLGgzLGg0LGg1LGg2e21hcmdpbi10b3A6MTZweDttYXJnaW4tYm90dG9tOjEwcHg7Zm9udC13ZWlnaHQ6NTAwO2xpbmUtaGVpZ2h0OjEuMn1oMXttYXJnaW4tdG9wOjIwcHg7Zm9udC1zaXplOjI2cHh9aDJ7bWFyZ2luLXRvcDoxOHB4O2ZvbnQtc2l6ZToyNHB4fWgze2ZvbnQtc2l6ZToyMnB4fWg0e2ZvbnQtc2l6ZToyMHB4fWg1e2ZvbnQtc2l6ZToxOHB4fWg2e2ZvbnQtc2l6ZToxNnB4fXNtYWxse2ZvbnQtc2l6ZTo3NSV9c3ViLHN1cHtwb3NpdGlvbjpyZWxhdGl2ZTtmb250LXNpemU6NzUlO2xpbmUtaGVpZ2h0OjA7dmVydGljYWwtYWxpZ246YmFzZWxpbmV9c3Vwe3RvcDotLjVlbX1zdWJ7Ym90dG9tOi0uMjVlbX1cbiIsIi5pb24tbm8tcGFkZGluZyxbbm8tcGFkZGluZ117LS1wYWRkaW5nLXN0YXJ0OiAwOy0tcGFkZGluZy1lbmQ6IDA7LS1wYWRkaW5nLXRvcDogMDstLXBhZGRpbmctYm90dG9tOiAwO3BhZGRpbmctbGVmdDowO3BhZGRpbmctcmlnaHQ6MDtwYWRkaW5nLXRvcDowO3BhZGRpbmctYm90dG9tOjB9Lmlvbi1wYWRkaW5nLFtwYWRkaW5nXXstLXBhZGRpbmctc3RhcnQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTstLXBhZGRpbmctZW5kOiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7LS1wYWRkaW5nLXRvcDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpOy0tcGFkZGluZy1ib3R0b206IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtwYWRkaW5nLWxlZnQ6dmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO3BhZGRpbmctcmlnaHQ6dmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO3BhZGRpbmctdG9wOnZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtwYWRkaW5nLWJvdHRvbTp2YXIoLS1pb24tcGFkZGluZywgMTZweCl9QHN1cHBvcnRzIChtYXJnaW4taW5saW5lLXN0YXJ0OiAwKSBvciAoLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDApey5pb24tcGFkZGluZyxbcGFkZGluZ117cGFkZGluZy1sZWZ0OnVuc2V0O3BhZGRpbmctcmlnaHQ6dW5zZXQ7LXdlYmtpdC1wYWRkaW5nLXN0YXJ0OnZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtwYWRkaW5nLWlubGluZS1zdGFydDp2YXIoLS1pb24tcGFkZGluZywgMTZweCk7LXdlYmtpdC1wYWRkaW5nLWVuZDp2YXIoLS1pb24tcGFkZGluZywgMTZweCk7cGFkZGluZy1pbmxpbmUtZW5kOnZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KX19Lmlvbi1wYWRkaW5nLXRvcCxbcGFkZGluZy10b3Bdey0tcGFkZGluZy10b3A6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtwYWRkaW5nLXRvcDp2YXIoLS1pb24tcGFkZGluZywgMTZweCl9Lmlvbi1wYWRkaW5nLXN0YXJ0LFtwYWRkaW5nLXN0YXJ0XXstLXBhZGRpbmctc3RhcnQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtwYWRkaW5nLWxlZnQ6dmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpfUBzdXBwb3J0cyAobWFyZ2luLWlubGluZS1zdGFydDogMCkgb3IgKC13ZWJraXQtbWFyZ2luLXN0YXJ0OiAwKXsuaW9uLXBhZGRpbmctc3RhcnQsW3BhZGRpbmctc3RhcnRde3BhZGRpbmctbGVmdDp1bnNldDstd2Via2l0LXBhZGRpbmctc3RhcnQ6dmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO3BhZGRpbmctaW5saW5lLXN0YXJ0OnZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KX19Lmlvbi1wYWRkaW5nLWVuZCxbcGFkZGluZy1lbmRdey0tcGFkZGluZy1lbmQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtwYWRkaW5nLXJpZ2h0OnZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KX1Ac3VwcG9ydHMgKG1hcmdpbi1pbmxpbmUtc3RhcnQ6IDApIG9yICgtd2Via2l0LW1hcmdpbi1zdGFydDogMCl7Lmlvbi1wYWRkaW5nLWVuZCxbcGFkZGluZy1lbmRde3BhZGRpbmctcmlnaHQ6dW5zZXQ7LXdlYmtpdC1wYWRkaW5nLWVuZDp2YXIoLS1pb24tcGFkZGluZywgMTZweCk7cGFkZGluZy1pbmxpbmUtZW5kOnZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KX19Lmlvbi1wYWRkaW5nLWJvdHRvbSxbcGFkZGluZy1ib3R0b21dey0tcGFkZGluZy1ib3R0b206IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtwYWRkaW5nLWJvdHRvbTp2YXIoLS1pb24tcGFkZGluZywgMTZweCl9Lmlvbi1wYWRkaW5nLXZlcnRpY2FsLFtwYWRkaW5nLXZlcnRpY2FsXXstLXBhZGRpbmctdG9wOiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7LS1wYWRkaW5nLWJvdHRvbTogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO3BhZGRpbmctdG9wOnZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtwYWRkaW5nLWJvdHRvbTp2YXIoLS1pb24tcGFkZGluZywgMTZweCl9Lmlvbi1wYWRkaW5nLWhvcml6b250YWwsW3BhZGRpbmctaG9yaXpvbnRhbF17LS1wYWRkaW5nLXN0YXJ0OiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7LS1wYWRkaW5nLWVuZDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO3BhZGRpbmctbGVmdDp2YXIoLS1pb24tcGFkZGluZywgMTZweCk7cGFkZGluZy1yaWdodDp2YXIoLS1pb24tcGFkZGluZywgMTZweCl9QHN1cHBvcnRzIChtYXJnaW4taW5saW5lLXN0YXJ0OiAwKSBvciAoLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDApey5pb24tcGFkZGluZy1ob3Jpem9udGFsLFtwYWRkaW5nLWhvcml6b250YWxde3BhZGRpbmctbGVmdDp1bnNldDtwYWRkaW5nLXJpZ2h0OnVuc2V0Oy13ZWJraXQtcGFkZGluZy1zdGFydDp2YXIoLS1pb24tcGFkZGluZywgMTZweCk7cGFkZGluZy1pbmxpbmUtc3RhcnQ6dmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpOy13ZWJraXQtcGFkZGluZy1lbmQ6dmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO3BhZGRpbmctaW5saW5lLWVuZDp2YXIoLS1pb24tcGFkZGluZywgMTZweCl9fS5pb24tbm8tbWFyZ2luLFtuby1tYXJnaW5dey0tbWFyZ2luLXN0YXJ0OiAwOy0tbWFyZ2luLWVuZDogMDstLW1hcmdpbi10b3A6IDA7LS1tYXJnaW4tYm90dG9tOiAwO21hcmdpbi1sZWZ0OjA7bWFyZ2luLXJpZ2h0OjA7bWFyZ2luLXRvcDowO21hcmdpbi1ib3R0b206MH0uaW9uLW1hcmdpbixbbWFyZ2luXXstLW1hcmdpbi1zdGFydDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7LS1tYXJnaW4tZW5kOiB2YXIoLS1pb24tbWFyZ2luLCAxNnB4KTstLW1hcmdpbi10b3A6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpOy0tbWFyZ2luLWJvdHRvbTogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7bWFyZ2luLWxlZnQ6dmFyKC0taW9uLW1hcmdpbiwgMTZweCk7bWFyZ2luLXJpZ2h0OnZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO21hcmdpbi10b3A6dmFyKC0taW9uLW1hcmdpbiwgMTZweCk7bWFyZ2luLWJvdHRvbTp2YXIoLS1pb24tbWFyZ2luLCAxNnB4KX1Ac3VwcG9ydHMgKG1hcmdpbi1pbmxpbmUtc3RhcnQ6IDApIG9yICgtd2Via2l0LW1hcmdpbi1zdGFydDogMCl7Lmlvbi1tYXJnaW4sW21hcmdpbl17bWFyZ2luLWxlZnQ6dW5zZXQ7bWFyZ2luLXJpZ2h0OnVuc2V0Oy13ZWJraXQtbWFyZ2luLXN0YXJ0OnZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO21hcmdpbi1pbmxpbmUtc3RhcnQ6dmFyKC0taW9uLW1hcmdpbiwgMTZweCk7LXdlYmtpdC1tYXJnaW4tZW5kOnZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO21hcmdpbi1pbmxpbmUtZW5kOnZhcigtLWlvbi1tYXJnaW4sIDE2cHgpfX0uaW9uLW1hcmdpbi10b3AsW21hcmdpbi10b3Bdey0tbWFyZ2luLXRvcDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7bWFyZ2luLXRvcDp2YXIoLS1pb24tbWFyZ2luLCAxNnB4KX0uaW9uLW1hcmdpbi1zdGFydCxbbWFyZ2luLXN0YXJ0XXstLW1hcmdpbi1zdGFydDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7bWFyZ2luLWxlZnQ6dmFyKC0taW9uLW1hcmdpbiwgMTZweCl9QHN1cHBvcnRzIChtYXJnaW4taW5saW5lLXN0YXJ0OiAwKSBvciAoLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDApey5pb24tbWFyZ2luLXN0YXJ0LFttYXJnaW4tc3RhcnRde21hcmdpbi1sZWZ0OnVuc2V0Oy13ZWJraXQtbWFyZ2luLXN0YXJ0OnZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO21hcmdpbi1pbmxpbmUtc3RhcnQ6dmFyKC0taW9uLW1hcmdpbiwgMTZweCl9fS5pb24tbWFyZ2luLWVuZCxbbWFyZ2luLWVuZF17LS1tYXJnaW4tZW5kOiB2YXIoLS1pb24tbWFyZ2luLCAxNnB4KTttYXJnaW4tcmlnaHQ6dmFyKC0taW9uLW1hcmdpbiwgMTZweCl9QHN1cHBvcnRzIChtYXJnaW4taW5saW5lLXN0YXJ0OiAwKSBvciAoLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDApey5pb24tbWFyZ2luLWVuZCxbbWFyZ2luLWVuZF17bWFyZ2luLXJpZ2h0OnVuc2V0Oy13ZWJraXQtbWFyZ2luLWVuZDp2YXIoLS1pb24tbWFyZ2luLCAxNnB4KTttYXJnaW4taW5saW5lLWVuZDp2YXIoLS1pb24tbWFyZ2luLCAxNnB4KX19Lmlvbi1tYXJnaW4tYm90dG9tLFttYXJnaW4tYm90dG9tXXstLW1hcmdpbi1ib3R0b206IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO21hcmdpbi1ib3R0b206dmFyKC0taW9uLW1hcmdpbiwgMTZweCl9Lmlvbi1tYXJnaW4tdmVydGljYWwsW21hcmdpbi12ZXJ0aWNhbF17LS1tYXJnaW4tdG9wOiB2YXIoLS1pb24tbWFyZ2luLCAxNnB4KTstLW1hcmdpbi1ib3R0b206IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO21hcmdpbi10b3A6dmFyKC0taW9uLW1hcmdpbiwgMTZweCk7bWFyZ2luLWJvdHRvbTp2YXIoLS1pb24tbWFyZ2luLCAxNnB4KX0uaW9uLW1hcmdpbi1ob3Jpem9udGFsLFttYXJnaW4taG9yaXpvbnRhbF17LS1tYXJnaW4tc3RhcnQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpOy0tbWFyZ2luLWVuZDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7bWFyZ2luLWxlZnQ6dmFyKC0taW9uLW1hcmdpbiwgMTZweCk7bWFyZ2luLXJpZ2h0OnZhcigtLWlvbi1tYXJnaW4sIDE2cHgpfUBzdXBwb3J0cyAobWFyZ2luLWlubGluZS1zdGFydDogMCkgb3IgKC13ZWJraXQtbWFyZ2luLXN0YXJ0OiAwKXsuaW9uLW1hcmdpbi1ob3Jpem9udGFsLFttYXJnaW4taG9yaXpvbnRhbF17bWFyZ2luLWxlZnQ6dW5zZXQ7bWFyZ2luLXJpZ2h0OnVuc2V0Oy13ZWJraXQtbWFyZ2luLXN0YXJ0OnZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO21hcmdpbi1pbmxpbmUtc3RhcnQ6dmFyKC0taW9uLW1hcmdpbiwgMTZweCk7LXdlYmtpdC1tYXJnaW4tZW5kOnZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO21hcmdpbi1pbmxpbmUtZW5kOnZhcigtLWlvbi1tYXJnaW4sIDE2cHgpfX1cbiIsIi5pb24tZmxvYXQtbGVmdCxbZmxvYXQtbGVmdF17ZmxvYXQ6bGVmdCAhaW1wb3J0YW50fS5pb24tZmxvYXQtcmlnaHQsW2Zsb2F0LXJpZ2h0XXtmbG9hdDpyaWdodCAhaW1wb3J0YW50fS5pb24tZmxvYXQtc3RhcnQsW2Zsb2F0LXN0YXJ0XXtmbG9hdDpsZWZ0ICFpbXBvcnRhbnR9Omhvc3QtY29udGV4dChbZGlyPXJ0bF0pIC5pb24tZmxvYXQtc3RhcnQsOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIFtmbG9hdC1zdGFydF17ZmxvYXQ6cmlnaHQgIWltcG9ydGFudH0uaW9uLWZsb2F0LWVuZCxbZmxvYXQtZW5kXXtmbG9hdDpyaWdodCAhaW1wb3J0YW50fTpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSAuaW9uLWZsb2F0LWVuZCw6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgW2Zsb2F0LWVuZF17ZmxvYXQ6bGVmdCAhaW1wb3J0YW50fUBtZWRpYSAobWluLXdpZHRoOiA1NzZweCl7Lmlvbi1mbG9hdC1zbS1sZWZ0LFtmbG9hdC1zbS1sZWZ0XXtmbG9hdDpsZWZ0ICFpbXBvcnRhbnR9Lmlvbi1mbG9hdC1zbS1yaWdodCxbZmxvYXQtc20tcmlnaHRde2Zsb2F0OnJpZ2h0ICFpbXBvcnRhbnR9Lmlvbi1mbG9hdC1zbS1zdGFydCxbZmxvYXQtc20tc3RhcnRde2Zsb2F0OmxlZnQgIWltcG9ydGFudH06aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgLmlvbi1mbG9hdC1zbS1zdGFydCw6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgW2Zsb2F0LXNtLXN0YXJ0XXtmbG9hdDpyaWdodCAhaW1wb3J0YW50fS5pb24tZmxvYXQtc20tZW5kLFtmbG9hdC1zbS1lbmRde2Zsb2F0OnJpZ2h0ICFpbXBvcnRhbnR9Omhvc3QtY29udGV4dChbZGlyPXJ0bF0pIC5pb24tZmxvYXQtc20tZW5kLDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSBbZmxvYXQtc20tZW5kXXtmbG9hdDpsZWZ0ICFpbXBvcnRhbnR9fUBtZWRpYSAobWluLXdpZHRoOiA3NjhweCl7Lmlvbi1mbG9hdC1tZC1sZWZ0LFtmbG9hdC1tZC1sZWZ0XXtmbG9hdDpsZWZ0ICFpbXBvcnRhbnR9Lmlvbi1mbG9hdC1tZC1yaWdodCxbZmxvYXQtbWQtcmlnaHRde2Zsb2F0OnJpZ2h0ICFpbXBvcnRhbnR9Lmlvbi1mbG9hdC1tZC1zdGFydCxbZmxvYXQtbWQtc3RhcnRde2Zsb2F0OmxlZnQgIWltcG9ydGFudH06aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgLmlvbi1mbG9hdC1tZC1zdGFydCw6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgW2Zsb2F0LW1kLXN0YXJ0XXtmbG9hdDpyaWdodCAhaW1wb3J0YW50fS5pb24tZmxvYXQtbWQtZW5kLFtmbG9hdC1tZC1lbmRde2Zsb2F0OnJpZ2h0ICFpbXBvcnRhbnR9Omhvc3QtY29udGV4dChbZGlyPXJ0bF0pIC5pb24tZmxvYXQtbWQtZW5kLDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSBbZmxvYXQtbWQtZW5kXXtmbG9hdDpsZWZ0ICFpbXBvcnRhbnR9fUBtZWRpYSAobWluLXdpZHRoOiA5OTJweCl7Lmlvbi1mbG9hdC1sZy1sZWZ0LFtmbG9hdC1sZy1sZWZ0XXtmbG9hdDpsZWZ0ICFpbXBvcnRhbnR9Lmlvbi1mbG9hdC1sZy1yaWdodCxbZmxvYXQtbGctcmlnaHRde2Zsb2F0OnJpZ2h0ICFpbXBvcnRhbnR9Lmlvbi1mbG9hdC1sZy1zdGFydCxbZmxvYXQtbGctc3RhcnRde2Zsb2F0OmxlZnQgIWltcG9ydGFudH06aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgLmlvbi1mbG9hdC1sZy1zdGFydCw6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgW2Zsb2F0LWxnLXN0YXJ0XXtmbG9hdDpyaWdodCAhaW1wb3J0YW50fS5pb24tZmxvYXQtbGctZW5kLFtmbG9hdC1sZy1lbmRde2Zsb2F0OnJpZ2h0ICFpbXBvcnRhbnR9Omhvc3QtY29udGV4dChbZGlyPXJ0bF0pIC5pb24tZmxvYXQtbGctZW5kLDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSBbZmxvYXQtbGctZW5kXXtmbG9hdDpsZWZ0ICFpbXBvcnRhbnR9fUBtZWRpYSAobWluLXdpZHRoOiAxMjAwcHgpey5pb24tZmxvYXQteGwtbGVmdCxbZmxvYXQteGwtbGVmdF17ZmxvYXQ6bGVmdCAhaW1wb3J0YW50fS5pb24tZmxvYXQteGwtcmlnaHQsW2Zsb2F0LXhsLXJpZ2h0XXtmbG9hdDpyaWdodCAhaW1wb3J0YW50fS5pb24tZmxvYXQteGwtc3RhcnQsW2Zsb2F0LXhsLXN0YXJ0XXtmbG9hdDpsZWZ0ICFpbXBvcnRhbnR9Omhvc3QtY29udGV4dChbZGlyPXJ0bF0pIC5pb24tZmxvYXQteGwtc3RhcnQsOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIFtmbG9hdC14bC1zdGFydF17ZmxvYXQ6cmlnaHQgIWltcG9ydGFudH0uaW9uLWZsb2F0LXhsLWVuZCxbZmxvYXQteGwtZW5kXXtmbG9hdDpyaWdodCAhaW1wb3J0YW50fTpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSAuaW9uLWZsb2F0LXhsLWVuZCw6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgW2Zsb2F0LXhsLWVuZF17ZmxvYXQ6bGVmdCAhaW1wb3J0YW50fX1cbiIsIi5pb24tdGV4dC1jZW50ZXIsW3RleHQtY2VudGVyXXt0ZXh0LWFsaWduOmNlbnRlciAhaW1wb3J0YW50fS5pb24tdGV4dC1qdXN0aWZ5LFt0ZXh0LWp1c3RpZnlde3RleHQtYWxpZ246anVzdGlmeSAhaW1wb3J0YW50fS5pb24tdGV4dC1zdGFydCxbdGV4dC1zdGFydF17dGV4dC1hbGlnbjpzdGFydCAhaW1wb3J0YW50fS5pb24tdGV4dC1lbmQsW3RleHQtZW5kXXt0ZXh0LWFsaWduOmVuZCAhaW1wb3J0YW50fS5pb24tdGV4dC1sZWZ0LFt0ZXh0LWxlZnRde3RleHQtYWxpZ246bGVmdCAhaW1wb3J0YW50fS5pb24tdGV4dC1yaWdodCxbdGV4dC1yaWdodF17dGV4dC1hbGlnbjpyaWdodCAhaW1wb3J0YW50fS5pb24tdGV4dC1ub3dyYXAsW3RleHQtbm93cmFwXXt3aGl0ZS1zcGFjZTpub3dyYXAgIWltcG9ydGFudH0uaW9uLXRleHQtd3JhcCxbdGV4dC13cmFwXXt3aGl0ZS1zcGFjZTpub3JtYWwgIWltcG9ydGFudH1AbWVkaWEgKG1pbi13aWR0aDogNTc2cHgpey5pb24tdGV4dC1zbS1jZW50ZXIsW3RleHQtc20tY2VudGVyXXt0ZXh0LWFsaWduOmNlbnRlciAhaW1wb3J0YW50fS5pb24tdGV4dC1zbS1qdXN0aWZ5LFt0ZXh0LXNtLWp1c3RpZnlde3RleHQtYWxpZ246anVzdGlmeSAhaW1wb3J0YW50fS5pb24tdGV4dC1zbS1zdGFydCxbdGV4dC1zbS1zdGFydF17dGV4dC1hbGlnbjpzdGFydCAhaW1wb3J0YW50fS5pb24tdGV4dC1zbS1lbmQsW3RleHQtc20tZW5kXXt0ZXh0LWFsaWduOmVuZCAhaW1wb3J0YW50fS5pb24tdGV4dC1zbS1sZWZ0LFt0ZXh0LXNtLWxlZnRde3RleHQtYWxpZ246bGVmdCAhaW1wb3J0YW50fS5pb24tdGV4dC1zbS1yaWdodCxbdGV4dC1zbS1yaWdodF17dGV4dC1hbGlnbjpyaWdodCAhaW1wb3J0YW50fS5pb24tdGV4dC1zbS1ub3dyYXAsW3RleHQtc20tbm93cmFwXXt3aGl0ZS1zcGFjZTpub3dyYXAgIWltcG9ydGFudH0uaW9uLXRleHQtc20td3JhcCxbdGV4dC1zbS13cmFwXXt3aGl0ZS1zcGFjZTpub3JtYWwgIWltcG9ydGFudH19QG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KXsuaW9uLXRleHQtbWQtY2VudGVyLFt0ZXh0LW1kLWNlbnRlcl17dGV4dC1hbGlnbjpjZW50ZXIgIWltcG9ydGFudH0uaW9uLXRleHQtbWQtanVzdGlmeSxbdGV4dC1tZC1qdXN0aWZ5XXt0ZXh0LWFsaWduOmp1c3RpZnkgIWltcG9ydGFudH0uaW9uLXRleHQtbWQtc3RhcnQsW3RleHQtbWQtc3RhcnRde3RleHQtYWxpZ246c3RhcnQgIWltcG9ydGFudH0uaW9uLXRleHQtbWQtZW5kLFt0ZXh0LW1kLWVuZF17dGV4dC1hbGlnbjplbmQgIWltcG9ydGFudH0uaW9uLXRleHQtbWQtbGVmdCxbdGV4dC1tZC1sZWZ0XXt0ZXh0LWFsaWduOmxlZnQgIWltcG9ydGFudH0uaW9uLXRleHQtbWQtcmlnaHQsW3RleHQtbWQtcmlnaHRde3RleHQtYWxpZ246cmlnaHQgIWltcG9ydGFudH0uaW9uLXRleHQtbWQtbm93cmFwLFt0ZXh0LW1kLW5vd3JhcF17d2hpdGUtc3BhY2U6bm93cmFwICFpbXBvcnRhbnR9Lmlvbi10ZXh0LW1kLXdyYXAsW3RleHQtbWQtd3JhcF17d2hpdGUtc3BhY2U6bm9ybWFsICFpbXBvcnRhbnR9fUBtZWRpYSAobWluLXdpZHRoOiA5OTJweCl7Lmlvbi10ZXh0LWxnLWNlbnRlcixbdGV4dC1sZy1jZW50ZXJde3RleHQtYWxpZ246Y2VudGVyICFpbXBvcnRhbnR9Lmlvbi10ZXh0LWxnLWp1c3RpZnksW3RleHQtbGctanVzdGlmeV17dGV4dC1hbGlnbjpqdXN0aWZ5ICFpbXBvcnRhbnR9Lmlvbi10ZXh0LWxnLXN0YXJ0LFt0ZXh0LWxnLXN0YXJ0XXt0ZXh0LWFsaWduOnN0YXJ0ICFpbXBvcnRhbnR9Lmlvbi10ZXh0LWxnLWVuZCxbdGV4dC1sZy1lbmRde3RleHQtYWxpZ246ZW5kICFpbXBvcnRhbnR9Lmlvbi10ZXh0LWxnLWxlZnQsW3RleHQtbGctbGVmdF17dGV4dC1hbGlnbjpsZWZ0ICFpbXBvcnRhbnR9Lmlvbi10ZXh0LWxnLXJpZ2h0LFt0ZXh0LWxnLXJpZ2h0XXt0ZXh0LWFsaWduOnJpZ2h0ICFpbXBvcnRhbnR9Lmlvbi10ZXh0LWxnLW5vd3JhcCxbdGV4dC1sZy1ub3dyYXBde3doaXRlLXNwYWNlOm5vd3JhcCAhaW1wb3J0YW50fS5pb24tdGV4dC1sZy13cmFwLFt0ZXh0LWxnLXdyYXBde3doaXRlLXNwYWNlOm5vcm1hbCAhaW1wb3J0YW50fX1AbWVkaWEgKG1pbi13aWR0aDogMTIwMHB4KXsuaW9uLXRleHQteGwtY2VudGVyLFt0ZXh0LXhsLWNlbnRlcl17dGV4dC1hbGlnbjpjZW50ZXIgIWltcG9ydGFudH0uaW9uLXRleHQteGwtanVzdGlmeSxbdGV4dC14bC1qdXN0aWZ5XXt0ZXh0LWFsaWduOmp1c3RpZnkgIWltcG9ydGFudH0uaW9uLXRleHQteGwtc3RhcnQsW3RleHQteGwtc3RhcnRde3RleHQtYWxpZ246c3RhcnQgIWltcG9ydGFudH0uaW9uLXRleHQteGwtZW5kLFt0ZXh0LXhsLWVuZF17dGV4dC1hbGlnbjplbmQgIWltcG9ydGFudH0uaW9uLXRleHQteGwtbGVmdCxbdGV4dC14bC1sZWZ0XXt0ZXh0LWFsaWduOmxlZnQgIWltcG9ydGFudH0uaW9uLXRleHQteGwtcmlnaHQsW3RleHQteGwtcmlnaHRde3RleHQtYWxpZ246cmlnaHQgIWltcG9ydGFudH0uaW9uLXRleHQteGwtbm93cmFwLFt0ZXh0LXhsLW5vd3JhcF17d2hpdGUtc3BhY2U6bm93cmFwICFpbXBvcnRhbnR9Lmlvbi10ZXh0LXhsLXdyYXAsW3RleHQteGwtd3JhcF17d2hpdGUtc3BhY2U6bm9ybWFsICFpbXBvcnRhbnR9fVxuIiwiLmlvbi10ZXh0LXVwcGVyY2FzZSxbdGV4dC11cHBlcmNhc2Vde3RleHQtdHJhbnNmb3JtOnVwcGVyY2FzZSAhaW1wb3J0YW50fS5pb24tdGV4dC1sb3dlcmNhc2UsW3RleHQtbG93ZXJjYXNlXXt0ZXh0LXRyYW5zZm9ybTpsb3dlcmNhc2UgIWltcG9ydGFudH0uaW9uLXRleHQtY2FwaXRhbGl6ZSxbdGV4dC1jYXBpdGFsaXplXXt0ZXh0LXRyYW5zZm9ybTpjYXBpdGFsaXplICFpbXBvcnRhbnR9QG1lZGlhIChtaW4td2lkdGg6IDU3NnB4KXsuaW9uLXRleHQtc20tdXBwZXJjYXNlLFt0ZXh0LXNtLXVwcGVyY2FzZV17dGV4dC10cmFuc2Zvcm06dXBwZXJjYXNlICFpbXBvcnRhbnR9Lmlvbi10ZXh0LXNtLWxvd2VyY2FzZSxbdGV4dC1zbS1sb3dlcmNhc2Vde3RleHQtdHJhbnNmb3JtOmxvd2VyY2FzZSAhaW1wb3J0YW50fS5pb24tdGV4dC1zbS1jYXBpdGFsaXplLFt0ZXh0LXNtLWNhcGl0YWxpemVde3RleHQtdHJhbnNmb3JtOmNhcGl0YWxpemUgIWltcG9ydGFudH19QG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KXsuaW9uLXRleHQtbWQtdXBwZXJjYXNlLFt0ZXh0LW1kLXVwcGVyY2FzZV17dGV4dC10cmFuc2Zvcm06dXBwZXJjYXNlICFpbXBvcnRhbnR9Lmlvbi10ZXh0LW1kLWxvd2VyY2FzZSxbdGV4dC1tZC1sb3dlcmNhc2Vde3RleHQtdHJhbnNmb3JtOmxvd2VyY2FzZSAhaW1wb3J0YW50fS5pb24tdGV4dC1tZC1jYXBpdGFsaXplLFt0ZXh0LW1kLWNhcGl0YWxpemVde3RleHQtdHJhbnNmb3JtOmNhcGl0YWxpemUgIWltcG9ydGFudH19QG1lZGlhIChtaW4td2lkdGg6IDk5MnB4KXsuaW9uLXRleHQtbGctdXBwZXJjYXNlLFt0ZXh0LWxnLXVwcGVyY2FzZV17dGV4dC10cmFuc2Zvcm06dXBwZXJjYXNlICFpbXBvcnRhbnR9Lmlvbi10ZXh0LWxnLWxvd2VyY2FzZSxbdGV4dC1sZy1sb3dlcmNhc2Vde3RleHQtdHJhbnNmb3JtOmxvd2VyY2FzZSAhaW1wb3J0YW50fS5pb24tdGV4dC1sZy1jYXBpdGFsaXplLFt0ZXh0LWxnLWNhcGl0YWxpemVde3RleHQtdHJhbnNmb3JtOmNhcGl0YWxpemUgIWltcG9ydGFudH19QG1lZGlhIChtaW4td2lkdGg6IDEyMDBweCl7Lmlvbi10ZXh0LXhsLXVwcGVyY2FzZSxbdGV4dC14bC11cHBlcmNhc2Vde3RleHQtdHJhbnNmb3JtOnVwcGVyY2FzZSAhaW1wb3J0YW50fS5pb24tdGV4dC14bC1sb3dlcmNhc2UsW3RleHQteGwtbG93ZXJjYXNlXXt0ZXh0LXRyYW5zZm9ybTpsb3dlcmNhc2UgIWltcG9ydGFudH0uaW9uLXRleHQteGwtY2FwaXRhbGl6ZSxbdGV4dC14bC1jYXBpdGFsaXplXXt0ZXh0LXRyYW5zZm9ybTpjYXBpdGFsaXplICFpbXBvcnRhbnR9fVxuIiwiLmlvbi1hbGlnbi1zZWxmLXN0YXJ0LFthbGlnbi1zZWxmLXN0YXJ0XXthbGlnbi1zZWxmOmZsZXgtc3RhcnQgIWltcG9ydGFudH0uaW9uLWFsaWduLXNlbGYtZW5kLFthbGlnbi1zZWxmLWVuZF17YWxpZ24tc2VsZjpmbGV4LWVuZCAhaW1wb3J0YW50fS5pb24tYWxpZ24tc2VsZi1jZW50ZXIsW2FsaWduLXNlbGYtY2VudGVyXXthbGlnbi1zZWxmOmNlbnRlciAhaW1wb3J0YW50fS5pb24tYWxpZ24tc2VsZi1zdHJldGNoLFthbGlnbi1zZWxmLXN0cmV0Y2hde2FsaWduLXNlbGY6c3RyZXRjaCAhaW1wb3J0YW50fS5pb24tYWxpZ24tc2VsZi1iYXNlbGluZSxbYWxpZ24tc2VsZi1iYXNlbGluZV17YWxpZ24tc2VsZjpiYXNlbGluZSAhaW1wb3J0YW50fS5pb24tYWxpZ24tc2VsZi1hdXRvLFthbGlnbi1zZWxmLWF1dG9de2FsaWduLXNlbGY6YXV0byAhaW1wb3J0YW50fS5pb24td3JhcCxbd3JhcF17ZmxleC13cmFwOndyYXAgIWltcG9ydGFudH0uaW9uLW5vd3JhcCxbbm93cmFwXXtmbGV4LXdyYXA6bm93cmFwICFpbXBvcnRhbnR9Lmlvbi13cmFwLXJldmVyc2UsW3dyYXAtcmV2ZXJzZV17ZmxleC13cmFwOndyYXAtcmV2ZXJzZSAhaW1wb3J0YW50fS5pb24tanVzdGlmeS1jb250ZW50LXN0YXJ0LFtqdXN0aWZ5LWNvbnRlbnQtc3RhcnRde2p1c3RpZnktY29udGVudDpmbGV4LXN0YXJ0ICFpbXBvcnRhbnR9Lmlvbi1qdXN0aWZ5LWNvbnRlbnQtY2VudGVyLFtqdXN0aWZ5LWNvbnRlbnQtY2VudGVyXXtqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyICFpbXBvcnRhbnR9Lmlvbi1qdXN0aWZ5LWNvbnRlbnQtZW5kLFtqdXN0aWZ5LWNvbnRlbnQtZW5kXXtqdXN0aWZ5LWNvbnRlbnQ6ZmxleC1lbmQgIWltcG9ydGFudH0uaW9uLWp1c3RpZnktY29udGVudC1hcm91bmQsW2p1c3RpZnktY29udGVudC1hcm91bmRde2p1c3RpZnktY29udGVudDpzcGFjZS1hcm91bmQgIWltcG9ydGFudH0uaW9uLWp1c3RpZnktY29udGVudC1iZXR3ZWVuLFtqdXN0aWZ5LWNvbnRlbnQtYmV0d2Vlbl17anVzdGlmeS1jb250ZW50OnNwYWNlLWJldHdlZW4gIWltcG9ydGFudH0uaW9uLWp1c3RpZnktY29udGVudC1ldmVubHksW2p1c3RpZnktY29udGVudC1ldmVubHlde2p1c3RpZnktY29udGVudDpzcGFjZS1ldmVubHkgIWltcG9ydGFudH0uaW9uLWFsaWduLWl0ZW1zLXN0YXJ0LFthbGlnbi1pdGVtcy1zdGFydF17YWxpZ24taXRlbXM6ZmxleC1zdGFydCAhaW1wb3J0YW50fS5pb24tYWxpZ24taXRlbXMtY2VudGVyLFthbGlnbi1pdGVtcy1jZW50ZXJde2FsaWduLWl0ZW1zOmNlbnRlciAhaW1wb3J0YW50fS5pb24tYWxpZ24taXRlbXMtZW5kLFthbGlnbi1pdGVtcy1lbmRde2FsaWduLWl0ZW1zOmZsZXgtZW5kICFpbXBvcnRhbnR9Lmlvbi1hbGlnbi1pdGVtcy1zdHJldGNoLFthbGlnbi1pdGVtcy1zdHJldGNoXXthbGlnbi1pdGVtczpzdHJldGNoICFpbXBvcnRhbnR9Lmlvbi1hbGlnbi1pdGVtcy1iYXNlbGluZSxbYWxpZ24taXRlbXMtYmFzZWxpbmVde2FsaWduLWl0ZW1zOmJhc2VsaW5lICFpbXBvcnRhbnR9XG4iLCIvKipcbiAqIEVsZW1lbnQgdGFnIG92ZXJyaWRlc1xuICovXG5ib2R5IHtcbiAgcGFkZGluZzogMDtcbiAgbWFyZ2luOiAwO1xufVxuIiwiLy8gc2hhcmVkIGFjcm9zcyBhbGwgd2ViIGJhc2VkIHBsYXRmb3JtcyBhbmQgYXBwc1xuQGltcG9ydCAnfkBtYWxvbWF0aWEvd2ViL3Njc3MvaW5kZXgnO1xuXG4vLyBodHRwOi8vaW9uaWNmcmFtZXdvcmsuY29tL2RvY3MvdGhlbWluZy9cbkBpbXBvcnQgJ35AaW9uaWMvYW5ndWxhci9jc3MvY29yZS5jc3MnO1xuQGltcG9ydCAnfkBpb25pYy9hbmd1bGFyL2Nzcy9ub3JtYWxpemUuY3NzJztcbkBpbXBvcnQgJ35AaW9uaWMvYW5ndWxhci9jc3Mvc3RydWN0dXJlLmNzcyc7XG5AaW1wb3J0ICd+QGlvbmljL2FuZ3VsYXIvY3NzL3R5cG9ncmFwaHkuY3NzJztcblxuQGltcG9ydCAnfkBpb25pYy9hbmd1bGFyL2Nzcy9wYWRkaW5nLmNzcyc7XG5AaW1wb3J0ICd+QGlvbmljL2FuZ3VsYXIvY3NzL2Zsb2F0LWVsZW1lbnRzLmNzcyc7XG5AaW1wb3J0ICd+QGlvbmljL2FuZ3VsYXIvY3NzL3RleHQtYWxpZ25tZW50LmNzcyc7XG5AaW1wb3J0ICd+QGlvbmljL2FuZ3VsYXIvY3NzL3RleHQtdHJhbnNmb3JtYXRpb24uY3NzJztcbkBpbXBvcnQgJ35AaW9uaWMvYW5ndWxhci9jc3MvZmxleC11dGlscy5jc3MnO1xuXG4vKipcbiAqIFRoZSBmb2xsb3dpbmcgYXJlIElvbmljIHNwZWNpZmljXG4gKi9cbkBpbXBvcnQgJ3RhZ3MnO1xuIiwiYm9keSB7XG4gIHBhZGRpbmctdG9wOiBjb25zdGFudChzYWZlLWFyZWEtaW5zZXQtdG9wKTsgLy9mb3IgaU9TIDExLjJcbiAgcGFkZGluZy10b3A6IGVudihzYWZlLWFyZWEtaW5zZXQtdG9wKTsgLy9mb3IgaU9TIDExLjFcbn1cbiJdfQ== */", '', '']]

/***/ }),

/***/ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/theme/variables.scss":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src??embedded!./node_modules/sass-loader/lib/loader.js??ref--14-3!./src/theme/variables.scss ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = [[module.i, "/** Ionic CSS Variables **/\n:root {\n  /** primary **/\n  --ion-color-primary: #3880ff;\n  --ion-color-primary-rgb: 56, 128, 255;\n  --ion-color-primary-contrast: #ffffff;\n  --ion-color-primary-contrast-rgb: 255, 255, 255;\n  --ion-color-primary-shade: #3171e0;\n  --ion-color-primary-tint: #4c8dff;\n  /** secondary **/\n  --ion-color-secondary: #0cd1e8;\n  --ion-color-secondary-rgb: 12, 209, 232;\n  --ion-color-secondary-contrast: #ffffff;\n  --ion-color-secondary-contrast-rgb: 255, 255, 255;\n  --ion-color-secondary-shade: #0bb8cc;\n  --ion-color-secondary-tint: #24d6ea;\n  /** tertiary **/\n  --ion-color-tertiary: #7044ff;\n  --ion-color-tertiary-rgb: 112, 68, 255;\n  --ion-color-tertiary-contrast: #ffffff;\n  --ion-color-tertiary-contrast-rgb: 255, 255, 255;\n  --ion-color-tertiary-shade: #633ce0;\n  --ion-color-tertiary-tint: #7e57ff;\n  /** success **/\n  --ion-color-success: #10dc60;\n  --ion-color-success-rgb: 16, 220, 96;\n  --ion-color-success-contrast: #ffffff;\n  --ion-color-success-contrast-rgb: 255, 255, 255;\n  --ion-color-success-shade: #0ec254;\n  --ion-color-success-tint: #28e070;\n  /** warning **/\n  --ion-color-warning: #ffce00;\n  --ion-color-warning-rgb: 255, 206, 0;\n  --ion-color-warning-contrast: #ffffff;\n  --ion-color-warning-contrast-rgb: 255, 255, 255;\n  --ion-color-warning-shade: #e0b500;\n  --ion-color-warning-tint: #ffd31a;\n  /** danger **/\n  --ion-color-danger: #f04141;\n  --ion-color-danger-rgb: 245, 61, 61;\n  --ion-color-danger-contrast: #ffffff;\n  --ion-color-danger-contrast-rgb: 255, 255, 255;\n  --ion-color-danger-shade: #d33939;\n  --ion-color-danger-tint: #f25454;\n  /** dark **/\n  --ion-color-dark: #222428;\n  --ion-color-dark-rgb: 34, 34, 34;\n  --ion-color-dark-contrast: #ffffff;\n  --ion-color-dark-contrast-rgb: 255, 255, 255;\n  --ion-color-dark-shade: #1e2023;\n  --ion-color-dark-tint: #383a3e;\n  /** medium **/\n  --ion-color-medium: #989aa2;\n  --ion-color-medium-rgb: 152, 154, 162;\n  --ion-color-medium-contrast: #ffffff;\n  --ion-color-medium-contrast-rgb: 255, 255, 255;\n  --ion-color-medium-shade: #86888f;\n  --ion-color-medium-tint: #a2a4ab;\n  /** light **/\n  --ion-color-light: #f4f5f8;\n  --ion-color-light-rgb: 244, 244, 244;\n  --ion-color-light-contrast: #000000;\n  --ion-color-light-contrast-rgb: 0, 0, 0;\n  --ion-color-light-shade: #d7d8da;\n  --ion-color-light-tint: #f5f6f9; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbm9ueW1vdXMvRGVza3RvcC9Xb3JrL21hbG9tYXRpYS9hcHBzL2dhbWEtY3JtLWlvbmljL3NyYy90aGVtZS92YXJpYWJsZXMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQSwwQkFBQTtBQUNBO0VBQ0UsY0FBQTtFQUNBLDRCQUFvQjtFQUNwQixxQ0FBd0I7RUFDeEIscUNBQTZCO0VBQzdCLCtDQUFpQztFQUNqQyxrQ0FBMEI7RUFDMUIsaUNBQXlCO0VBRXpCLGdCQUFBO0VBQ0EsOEJBQXNCO0VBQ3RCLHVDQUEwQjtFQUMxQix1Q0FBK0I7RUFDL0IsaURBQW1DO0VBQ25DLG9DQUE0QjtFQUM1QixtQ0FBMkI7RUFFM0IsZUFBQTtFQUNBLDZCQUFxQjtFQUNyQixzQ0FBeUI7RUFDekIsc0NBQThCO0VBQzlCLGdEQUFrQztFQUNsQyxtQ0FBMkI7RUFDM0Isa0NBQTBCO0VBRTFCLGNBQUE7RUFDQSw0QkFBb0I7RUFDcEIsb0NBQXdCO0VBQ3hCLHFDQUE2QjtFQUM3QiwrQ0FBaUM7RUFDakMsa0NBQTBCO0VBQzFCLGlDQUF5QjtFQUV6QixjQUFBO0VBQ0EsNEJBQW9CO0VBQ3BCLG9DQUF3QjtFQUN4QixxQ0FBNkI7RUFDN0IsK0NBQWlDO0VBQ2pDLGtDQUEwQjtFQUMxQixpQ0FBeUI7RUFFekIsYUFBQTtFQUNBLDJCQUFtQjtFQUNuQixtQ0FBdUI7RUFDdkIsb0NBQTRCO0VBQzVCLDhDQUFnQztFQUNoQyxpQ0FBeUI7RUFDekIsZ0NBQXdCO0VBRXhCLFdBQUE7RUFDQSx5QkFBaUI7RUFDakIsZ0NBQXFCO0VBQ3JCLGtDQUEwQjtFQUMxQiw0Q0FBOEI7RUFDOUIsK0JBQXVCO0VBQ3ZCLDhCQUFzQjtFQUV0QixhQUFBO0VBQ0EsMkJBQW1CO0VBQ25CLHFDQUF1QjtFQUN2QixvQ0FBNEI7RUFDNUIsOENBQWdDO0VBQ2hDLGlDQUF5QjtFQUN6QixnQ0FBd0I7RUFFeEIsWUFBQTtFQUNBLDBCQUFrQjtFQUNsQixvQ0FBc0I7RUFDdEIsbUNBQTJCO0VBQzNCLHVDQUErQjtFQUMvQixnQ0FBd0I7RUFDeEIsK0JBQXVCLEVBQUEiLCJmaWxlIjoic3JjL3RoZW1lL3ZhcmlhYmxlcy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gSW9uaWMgVmFyaWFibGVzIGFuZCBUaGVtaW5nLiBGb3IgbW9yZSBpbmZvLCBwbGVhc2Ugc2VlOlxuLy8gaHR0cDovL2lvbmljZnJhbWV3b3JrLmNvbS9kb2NzL3RoZW1pbmcvXG5cbi8qKiBJb25pYyBDU1MgVmFyaWFibGVzICoqL1xuOnJvb3Qge1xuICAvKiogcHJpbWFyeSAqKi9cbiAgLS1pb24tY29sb3ItcHJpbWFyeTogIzM4ODBmZjtcbiAgLS1pb24tY29sb3ItcHJpbWFyeS1yZ2I6IDU2LCAxMjgsIDI1NTtcbiAgLS1pb24tY29sb3ItcHJpbWFyeS1jb250cmFzdDogI2ZmZmZmZjtcbiAgLS1pb24tY29sb3ItcHJpbWFyeS1jb250cmFzdC1yZ2I6IDI1NSwgMjU1LCAyNTU7XG4gIC0taW9uLWNvbG9yLXByaW1hcnktc2hhZGU6ICMzMTcxZTA7XG4gIC0taW9uLWNvbG9yLXByaW1hcnktdGludDogIzRjOGRmZjtcblxuICAvKiogc2Vjb25kYXJ5ICoqL1xuICAtLWlvbi1jb2xvci1zZWNvbmRhcnk6ICMwY2QxZTg7XG4gIC0taW9uLWNvbG9yLXNlY29uZGFyeS1yZ2I6IDEyLCAyMDksIDIzMjtcbiAgLS1pb24tY29sb3Itc2Vjb25kYXJ5LWNvbnRyYXN0OiAjZmZmZmZmO1xuICAtLWlvbi1jb2xvci1zZWNvbmRhcnktY29udHJhc3QtcmdiOiAyNTUsIDI1NSwgMjU1O1xuICAtLWlvbi1jb2xvci1zZWNvbmRhcnktc2hhZGU6ICMwYmI4Y2M7XG4gIC0taW9uLWNvbG9yLXNlY29uZGFyeS10aW50OiAjMjRkNmVhO1xuXG4gIC8qKiB0ZXJ0aWFyeSAqKi9cbiAgLS1pb24tY29sb3ItdGVydGlhcnk6ICM3MDQ0ZmY7XG4gIC0taW9uLWNvbG9yLXRlcnRpYXJ5LXJnYjogMTEyLCA2OCwgMjU1O1xuICAtLWlvbi1jb2xvci10ZXJ0aWFyeS1jb250cmFzdDogI2ZmZmZmZjtcbiAgLS1pb24tY29sb3ItdGVydGlhcnktY29udHJhc3QtcmdiOiAyNTUsIDI1NSwgMjU1O1xuICAtLWlvbi1jb2xvci10ZXJ0aWFyeS1zaGFkZTogIzYzM2NlMDtcbiAgLS1pb24tY29sb3ItdGVydGlhcnktdGludDogIzdlNTdmZjtcblxuICAvKiogc3VjY2VzcyAqKi9cbiAgLS1pb24tY29sb3Itc3VjY2VzczogIzEwZGM2MDtcbiAgLS1pb24tY29sb3Itc3VjY2Vzcy1yZ2I6IDE2LCAyMjAsIDk2O1xuICAtLWlvbi1jb2xvci1zdWNjZXNzLWNvbnRyYXN0OiAjZmZmZmZmO1xuICAtLWlvbi1jb2xvci1zdWNjZXNzLWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcbiAgLS1pb24tY29sb3Itc3VjY2Vzcy1zaGFkZTogIzBlYzI1NDtcbiAgLS1pb24tY29sb3Itc3VjY2Vzcy10aW50OiAjMjhlMDcwO1xuXG4gIC8qKiB3YXJuaW5nICoqL1xuICAtLWlvbi1jb2xvci13YXJuaW5nOiAjZmZjZTAwO1xuICAtLWlvbi1jb2xvci13YXJuaW5nLXJnYjogMjU1LCAyMDYsIDA7XG4gIC0taW9uLWNvbG9yLXdhcm5pbmctY29udHJhc3Q6ICNmZmZmZmY7XG4gIC0taW9uLWNvbG9yLXdhcm5pbmctY29udHJhc3QtcmdiOiAyNTUsIDI1NSwgMjU1O1xuICAtLWlvbi1jb2xvci13YXJuaW5nLXNoYWRlOiAjZTBiNTAwO1xuICAtLWlvbi1jb2xvci13YXJuaW5nLXRpbnQ6ICNmZmQzMWE7XG5cbiAgLyoqIGRhbmdlciAqKi9cbiAgLS1pb24tY29sb3ItZGFuZ2VyOiAjZjA0MTQxO1xuICAtLWlvbi1jb2xvci1kYW5nZXItcmdiOiAyNDUsIDYxLCA2MTtcbiAgLS1pb24tY29sb3ItZGFuZ2VyLWNvbnRyYXN0OiAjZmZmZmZmO1xuICAtLWlvbi1jb2xvci1kYW5nZXItY29udHJhc3QtcmdiOiAyNTUsIDI1NSwgMjU1O1xuICAtLWlvbi1jb2xvci1kYW5nZXItc2hhZGU6ICNkMzM5Mzk7XG4gIC0taW9uLWNvbG9yLWRhbmdlci10aW50OiAjZjI1NDU0O1xuXG4gIC8qKiBkYXJrICoqL1xuICAtLWlvbi1jb2xvci1kYXJrOiAjMjIyNDI4O1xuICAtLWlvbi1jb2xvci1kYXJrLXJnYjogMzQsIDM0LCAzNDtcbiAgLS1pb24tY29sb3ItZGFyay1jb250cmFzdDogI2ZmZmZmZjtcbiAgLS1pb24tY29sb3ItZGFyay1jb250cmFzdC1yZ2I6IDI1NSwgMjU1LCAyNTU7XG4gIC0taW9uLWNvbG9yLWRhcmstc2hhZGU6ICMxZTIwMjM7XG4gIC0taW9uLWNvbG9yLWRhcmstdGludDogIzM4M2EzZTtcblxuICAvKiogbWVkaXVtICoqL1xuICAtLWlvbi1jb2xvci1tZWRpdW06ICM5ODlhYTI7XG4gIC0taW9uLWNvbG9yLW1lZGl1bS1yZ2I6IDE1MiwgMTU0LCAxNjI7XG4gIC0taW9uLWNvbG9yLW1lZGl1bS1jb250cmFzdDogI2ZmZmZmZjtcbiAgLS1pb24tY29sb3ItbWVkaXVtLWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcbiAgLS1pb24tY29sb3ItbWVkaXVtLXNoYWRlOiAjODY4ODhmO1xuICAtLWlvbi1jb2xvci1tZWRpdW0tdGludDogI2EyYTRhYjtcblxuICAvKiogbGlnaHQgKiovXG4gIC0taW9uLWNvbG9yLWxpZ2h0OiAjZjRmNWY4O1xuICAtLWlvbi1jb2xvci1saWdodC1yZ2I6IDI0NCwgMjQ0LCAyNDQ7XG4gIC0taW9uLWNvbG9yLWxpZ2h0LWNvbnRyYXN0OiAjMDAwMDAwO1xuICAtLWlvbi1jb2xvci1saWdodC1jb250cmFzdC1yZ2I6IDAsIDAsIDA7XG4gIC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlOiAjZDdkOGRhO1xuICAtLWlvbi1jb2xvci1saWdodC10aW50OiAjZjVmNmY5O1xufVxuIl19 */", '', '']]

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/global.scss":
/*!*************************!*\
  !*** ./src/global.scss ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!../node_modules/postcss-loader/src??embedded!../node_modules/sass-loader/lib/loader.js??ref--14-3!./global.scss */ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/global.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./src/theme/variables.scss":
/*!**********************************!*\
  !*** ./src/theme/variables.scss ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!../../node_modules/postcss-loader/src??embedded!../../node_modules/sass-loader/lib/loader.js??ref--14-3!./variables.scss */ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/theme/variables.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ 2:
/*!**********************************************************!*\
  !*** multi ./src/theme/variables.scss ./src/global.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/anonymous/Desktop/Work/malomatia/apps/gama-crm-ionic/src/theme/variables.scss */"./src/theme/variables.scss");
module.exports = __webpack_require__(/*! /Users/anonymous/Desktop/Work/malomatia/apps/gama-crm-ionic/src/global.scss */"./src/global.scss");


/***/ })

},[[2,"runtime"]]]);
//# sourceMappingURL=styles.js.map