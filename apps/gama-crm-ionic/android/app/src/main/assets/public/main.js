(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "../../libs/core/base/base-component.ts":
/*!********************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/core/base/base-component.ts ***!
  \********************************************************************************/
/*! exports provided: BaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseComponent", function() { return BaseComponent; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "../../node_modules/rxjs/_esm5/index.js");
// libs

var BaseComponent = /** @class */ (function () {
    function BaseComponent() {
    }
    Object.defineProperty(BaseComponent.prototype, "destroy$", {
        get: function () {
            if (!this._destroy$) {
                // Perf optimization:
                // since this is likely used as base component everywhere
                // only construct a Subject instance if actually used
                this._destroy$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
            }
            return this._destroy$;
        },
        enumerable: true,
        configurable: true
    });
    BaseComponent.prototype.ngOnDestroy = function () {
        if (this._destroy$) {
            this._destroy$.next(true);
            this._destroy$.complete();
        }
    };
    return BaseComponent;
}());



/***/ }),

/***/ "../../libs/core/base/index.ts":
/*!***********************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/core/base/index.ts ***!
  \***********************************************************************/
/*! exports provided: BaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base-component */ "../../libs/core/base/base-component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BaseComponent", function() { return _base_component__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]; });




/***/ }),

/***/ "../../libs/core/core.module.ts":
/*!************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/core/core.module.ts ***!
  \************************************************************************/
/*! exports provided: BASE_PROVIDERS, CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BASE_PROVIDERS", function() { return BASE_PROVIDERS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "../../node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _nrwl_nx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @nrwl/nx */ "../../node_modules/@nrwl/nx/esm5/nrwl-nx.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "../../node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _malomatia_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @malomatia/utils */ "../../libs/utils/index.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./environments/environment */ "../../libs/core/environments/environment.ts");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services */ "../../libs/core/services/index.ts");
/* harmony import */ var _services_log_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/log.service */ "../../libs/core/services/log.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


// libs



// app



/**
 * DEBUGGING
 */
_services_log_service__WEBPACK_IMPORTED_MODULE_7__["LogService"].DEBUG.LEVEL_4 = !_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].production;
var BASE_PROVIDERS = _services__WEBPACK_IMPORTED_MODULE_6__["CORE_PROVIDERS"].concat([
    {
        provide: _angular_common__WEBPACK_IMPORTED_MODULE_1__["APP_BASE_HREF"],
        useValue: '/'
    }
]);
var CoreModule = /** @class */ (function () {
    function CoreModule(parentModule, lang, translate) {
        Object(_malomatia_utils__WEBPACK_IMPORTED_MODULE_4__["throwIfAlreadyLoaded"])(parentModule, 'CoreModule');
        // ensure default platform language is set
        translate.use(lang);
    }
    CoreModule_1 = CoreModule;
    // configuredProviders: *required to configure WindowService and others per platform
    CoreModule.forRoot = function (configuredProviders) {
        return {
            ngModule: CoreModule_1,
            providers: BASE_PROVIDERS.concat(configuredProviders)
        };
    };
    var CoreModule_1;
    CoreModule = CoreModule_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _nrwl_nx__WEBPACK_IMPORTED_MODULE_2__["NxModule"].forRoot()]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["SkipSelf"])()),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_services__WEBPACK_IMPORTED_MODULE_6__["PlatformLanguageToken"])),
        __metadata("design:paramtypes", [CoreModule, String, _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"]])
    ], CoreModule);
    return CoreModule;
}());



/***/ }),

/***/ "../../libs/core/environments/environment.ts":
/*!*************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/core/environments/environment.ts ***!
  \*************************************************************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: false,
    api_url: 'http://127.0.0.1:4000',
    baseRoutePath: ''
};


/***/ }),

/***/ "../../libs/core/index.ts":
/*!******************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/core/index.ts ***!
  \******************************************************************/
/*! exports provided: CoreModule, BaseComponent, environment, CORE_PROVIDERS, LogService, WindowService, PlatformLanguageToken, PlatformWindowToken */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base */ "../../libs/core/base/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BaseComponent", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]; });

/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "../../libs/core/environments/environment.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"]; });

/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services */ "../../libs/core/services/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "CORE_PROVIDERS", function() { return _services__WEBPACK_IMPORTED_MODULE_2__["CORE_PROVIDERS"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LogService", function() { return _services__WEBPACK_IMPORTED_MODULE_2__["LogService"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "WindowService", function() { return _services__WEBPACK_IMPORTED_MODULE_2__["WindowService"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PlatformLanguageToken", function() { return _services__WEBPACK_IMPORTED_MODULE_2__["PlatformLanguageToken"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PlatformWindowToken", function() { return _services__WEBPACK_IMPORTED_MODULE_2__["PlatformWindowToken"]; });

/* harmony import */ var _core_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./core.module */ "../../libs/core/core.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return _core_module__WEBPACK_IMPORTED_MODULE_3__["CoreModule"]; });







/***/ }),

/***/ "../../libs/core/services/index.ts":
/*!***************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/core/services/index.ts ***!
  \***************************************************************************/
/*! exports provided: CORE_PROVIDERS, LogService, WindowService, PlatformLanguageToken, PlatformWindowToken */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CORE_PROVIDERS", function() { return CORE_PROVIDERS; });
/* harmony import */ var _log_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./log.service */ "../../libs/core/services/log.service.ts");
/* harmony import */ var _window_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./window.service */ "../../libs/core/services/window.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LogService", function() { return _log_service__WEBPACK_IMPORTED_MODULE_0__["LogService"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "WindowService", function() { return _window_service__WEBPACK_IMPORTED_MODULE_1__["WindowService"]; });

/* harmony import */ var _tokens__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tokens */ "../../libs/core/services/tokens.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PlatformLanguageToken", function() { return _tokens__WEBPACK_IMPORTED_MODULE_2__["PlatformLanguageToken"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PlatformWindowToken", function() { return _tokens__WEBPACK_IMPORTED_MODULE_2__["PlatformWindowToken"]; });



var CORE_PROVIDERS = [_log_service__WEBPACK_IMPORTED_MODULE_0__["LogService"], _window_service__WEBPACK_IMPORTED_MODULE_1__["WindowService"]];





/***/ }),

/***/ "../../libs/core/services/log.service.ts":
/*!*********************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/core/services/log.service.ts ***!
  \*********************************************************************************/
/*! exports provided: LogService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogService", function() { return LogService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// angular

var LogService = /** @class */ (function () {
    function LogService() {
    }
    LogService_1 = LogService;
    // info (extra messages like analytics)
    // use LEVEL_5 to see only these
    LogService.prototype.info = function () {
        var msg = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            msg[_i] = arguments[_i];
        }
        if (LogService_1.DEBUG.LEVEL_5 || LogService_1.DEBUG.LEVEL_4) {
            // extra messages
            console.info(msg);
        }
    };
    // debug (standard output)
    LogService.prototype.debug = function () {
        var msg = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            msg[_i] = arguments[_i];
        }
        if (LogService_1.DEBUG.LEVEL_4 || LogService_1.DEBUG.LEVEL_3) {
            // console.debug does not work on {N} apps... use `log`
            console.log(msg);
        }
    };
    // error
    LogService.prototype.error = function () {
        var err = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            err[_i] = arguments[_i];
        }
        if (LogService_1.DEBUG.LEVEL_4 ||
            LogService_1.DEBUG.LEVEL_3 ||
            LogService_1.DEBUG.LEVEL_2) {
            console.error(err);
        }
    };
    // warn
    LogService.prototype.warn = function () {
        var warn = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            warn[_i] = arguments[_i];
        }
        if (LogService_1.DEBUG.LEVEL_4 ||
            LogService_1.DEBUG.LEVEL_3 ||
            LogService_1.DEBUG.LEVEL_1) {
            console.warn(warn);
        }
    };
    var LogService_1;
    LogService.DEBUG = {
        LEVEL_1: false,
        LEVEL_2: false,
        LEVEL_3: false,
        LEVEL_4: false,
        LEVEL_5: false // just info (excluding all else)
    };
    LogService = LogService_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], LogService);
    return LogService;
}());



/***/ }),

/***/ "../../libs/core/services/tokens.ts":
/*!****************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/core/services/tokens.ts ***!
  \****************************************************************************/
/*! exports provided: PlatformLanguageToken, PlatformWindowToken */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlatformLanguageToken", function() { return PlatformLanguageToken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlatformWindowToken", function() { return PlatformWindowToken; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");

/**
 * Various InjectionTokens shared across all platforms
 * Always suffix with 'Token' for clarity and consistency
 */
var PlatformLanguageToken = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('PlatformLanguageToken');
var PlatformWindowToken = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('PlatformWindowToken');


/***/ }),

/***/ "../../libs/core/services/window.service.ts":
/*!************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/core/services/window.service.ts ***!
  \************************************************************************************/
/*! exports provided: WindowService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WindowService", function() { return WindowService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _malomatia_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @malomatia/utils */ "../../libs/utils/index.ts");
/* harmony import */ var _tokens__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tokens */ "../../libs/core/services/tokens.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
// angular

// app


var WindowService = /** @class */ (function () {
    function WindowService(_platformWindow) {
        this._platformWindow = _platformWindow;
    }
    Object.defineProperty(WindowService.prototype, "navigator", {
        get: function () {
            return this._platformWindow.navigator;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WindowService.prototype, "location", {
        get: function () {
            return this._platformWindow.location;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WindowService.prototype, "process", {
        get: function () {
            return this._platformWindow.process;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WindowService.prototype, "require", {
        get: function () {
            return this._platformWindow.require;
        },
        enumerable: true,
        configurable: true
    });
    WindowService.prototype.alert = function (msg) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var result = _this._platformWindow.alert(msg);
            if (Object(_malomatia_utils__WEBPACK_IMPORTED_MODULE_1__["isObject"])(result) && result.then) {
                // console.log('WindowService -- using result.then promise');
                result.then(resolve, reject);
            }
            else {
                resolve();
            }
        });
    };
    WindowService.prototype.confirm = function (msg, action /* used for fancyalerts on mobile*/) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var result = _this._platformWindow.confirm(msg, Object(_malomatia_utils__WEBPACK_IMPORTED_MODULE_1__["isNativeScript"])() ? action : undefined);
            if (Object(_malomatia_utils__WEBPACK_IMPORTED_MODULE_1__["isObject"])(result) && result.then) {
                result.then(resolve, reject);
            }
            else if (result) {
                resolve();
            }
            else {
                reject();
            }
        });
    };
    WindowService.prototype.setTimeout = function (handler, timeout) {
        return this._platformWindow.setTimeout(handler, timeout);
    };
    WindowService.prototype.clearTimeout = function (timeoutId) {
        return this._platformWindow.clearTimeout(timeoutId);
    };
    WindowService.prototype.setInterval = function (handler, ms) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        return this._platformWindow.setInterval(handler, ms, args);
    };
    WindowService.prototype.clearInterval = function (intervalId) {
        return this._platformWindow.clearInterval(intervalId);
    };
    WindowService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_tokens__WEBPACK_IMPORTED_MODULE_2__["PlatformWindowToken"])),
        __metadata("design:paramtypes", [Object])
    ], WindowService);
    return WindowService;
}());



/***/ }),

/***/ "../../libs/features/index.ts":
/*!**********************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/index.ts ***!
  \**********************************************************************/
/*! exports provided: UISharedModule, routeBase, routeItems, ITEM_PROVIDERS, ItemDetailBaseComponent, ItemsBaseComponent, Item, ItemService, HeaderBaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _items__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./items */ "../../libs/features/items/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "routeBase", function() { return _items__WEBPACK_IMPORTED_MODULE_0__["routeBase"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "routeItems", function() { return _items__WEBPACK_IMPORTED_MODULE_0__["routeItems"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ITEM_PROVIDERS", function() { return _items__WEBPACK_IMPORTED_MODULE_0__["ITEM_PROVIDERS"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemDetailBaseComponent", function() { return _items__WEBPACK_IMPORTED_MODULE_0__["ItemDetailBaseComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemsBaseComponent", function() { return _items__WEBPACK_IMPORTED_MODULE_0__["ItemsBaseComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Item", function() { return _items__WEBPACK_IMPORTED_MODULE_0__["Item"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemService", function() { return _items__WEBPACK_IMPORTED_MODULE_0__["ItemService"]; });

/* harmony import */ var _ui__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ui */ "../../libs/features/ui/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UISharedModule", function() { return _ui__WEBPACK_IMPORTED_MODULE_1__["UISharedModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HeaderBaseComponent", function() { return _ui__WEBPACK_IMPORTED_MODULE_1__["HeaderBaseComponent"]; });





/***/ }),

/***/ "../../libs/features/items/base/index.ts":
/*!*********************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/items/base/index.ts ***!
  \*********************************************************************************/
/*! exports provided: ItemDetailBaseComponent, ItemsBaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _item_detail_base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./item-detail.base-component */ "../../libs/features/items/base/item-detail.base-component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemDetailBaseComponent", function() { return _item_detail_base_component__WEBPACK_IMPORTED_MODULE_0__["ItemDetailBaseComponent"]; });

/* harmony import */ var _items_base_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./items.base-component */ "../../libs/features/items/base/items.base-component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemsBaseComponent", function() { return _items_base_component__WEBPACK_IMPORTED_MODULE_1__["ItemsBaseComponent"]; });





/***/ }),

/***/ "../../libs/features/items/base/item-detail.base-component.ts":
/*!******************************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/items/base/item-detail.base-component.ts ***!
  \******************************************************************************************************/
/*! exports provided: ItemDetailBaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemDetailBaseComponent", function() { return ItemDetailBaseComponent; });
/* harmony import */ var _malomatia_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @malomatia/core */ "../../libs/core/index.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// libs

var ItemDetailBaseComponent = /** @class */ (function (_super) {
    __extends(ItemDetailBaseComponent, _super);
    function ItemDetailBaseComponent(log, itemService, route) {
        var _this = _super.call(this) || this;
        _this.log = log;
        _this.itemService = itemService;
        _this.route = route;
        return _this;
    }
    ItemDetailBaseComponent.prototype.ngOnInit = function () {
        var id = +this.route.snapshot.params['id'];
        this.log.debug('ItemDetailBaseComponent ngOnInit id:', id);
        this.item = this.itemService.getItem(id);
    };
    return ItemDetailBaseComponent;
}(_malomatia_core__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]));



/***/ }),

/***/ "../../libs/features/items/base/items.base-component.ts":
/*!************************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/items/base/items.base-component.ts ***!
  \************************************************************************************************/
/*! exports provided: ItemsBaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemsBaseComponent", function() { return ItemsBaseComponent; });
/* harmony import */ var _malomatia_core_base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @malomatia/core/base */ "../../libs/core/base/index.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// libs

var ItemsBaseComponent = /** @class */ (function (_super) {
    __extends(ItemsBaseComponent, _super);
    function ItemsBaseComponent(log, itemService) {
        var _this = _super.call(this) || this;
        _this.log = log;
        _this.itemService = itemService;
        return _this;
    }
    ItemsBaseComponent.prototype.ngOnInit = function () {
        this.log.debug('ItemsBaseComponent ngOnInit');
        this.items = this.itemService.getItems();
    };
    return ItemsBaseComponent;
}(_malomatia_core_base__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]));



/***/ }),

/***/ "../../libs/features/items/index.ts":
/*!****************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/items/index.ts ***!
  \****************************************************************************/
/*! exports provided: routeBase, routeItems, ITEM_PROVIDERS, ItemDetailBaseComponent, ItemsBaseComponent, Item, ItemService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base */ "../../libs/features/items/base/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemDetailBaseComponent", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["ItemDetailBaseComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemsBaseComponent", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["ItemsBaseComponent"]; });

/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./models */ "../../libs/features/items/models/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Item", function() { return _models__WEBPACK_IMPORTED_MODULE_1__["Item"]; });

/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./routes */ "../../libs/features/items/routes.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "routeBase", function() { return _routes__WEBPACK_IMPORTED_MODULE_2__["routeBase"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "routeItems", function() { return _routes__WEBPACK_IMPORTED_MODULE_2__["routeItems"]; });

/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services */ "../../libs/features/items/services/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ITEM_PROVIDERS", function() { return _services__WEBPACK_IMPORTED_MODULE_3__["ITEM_PROVIDERS"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemService", function() { return _services__WEBPACK_IMPORTED_MODULE_3__["ItemService"]; });







/***/ }),

/***/ "../../libs/features/items/models/index.ts":
/*!***********************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/items/models/index.ts ***!
  \***********************************************************************************/
/*! exports provided: Item */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _item_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./item.model */ "../../libs/features/items/models/item.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Item", function() { return _item_model__WEBPACK_IMPORTED_MODULE_0__["Item"]; });




/***/ }),

/***/ "../../libs/features/items/models/item.model.ts":
/*!****************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/items/models/item.model.ts ***!
  \****************************************************************************************/
/*! exports provided: Item */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Item", function() { return Item; });
var Item = /** @class */ (function () {
    function Item() {
    }
    return Item;
}());



/***/ }),

/***/ "../../libs/features/items/routes.ts":
/*!*****************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/items/routes.ts ***!
  \*****************************************************************************/
/*! exports provided: routeBase, routeItems */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routeBase", function() { return routeBase; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routeItems", function() { return routeItems; });
/* harmony import */ var _malomatia_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @malomatia/core */ "../../libs/core/index.ts");

var homePath = "/" + _malomatia_core__WEBPACK_IMPORTED_MODULE_0__["environment"].baseRoutePath;
function routeBase(lazyLoad, additional, redirectTo) {
    if (additional === void 0) { additional = []; }
    if (redirectTo === void 0) { redirectTo = homePath; }
    return [
        {
            path: _malomatia_core__WEBPACK_IMPORTED_MODULE_0__["environment"].baseRoutePath,
            loadChildren: lazyLoad.base
        }
    ].concat(additional);
}
function routeItems(index, detail) {
    return [
        {
            path: '',
            component: index
        },
        {
            path: ':id',
            component: detail
        }
    ];
}


/***/ }),

/***/ "../../libs/features/items/services/index.ts":
/*!*************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/items/services/index.ts ***!
  \*************************************************************************************/
/*! exports provided: ITEM_PROVIDERS, ItemService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ITEM_PROVIDERS", function() { return ITEM_PROVIDERS; });
/* harmony import */ var _item_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./item.service */ "../../libs/features/items/services/item.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemService", function() { return _item_service__WEBPACK_IMPORTED_MODULE_0__["ItemService"]; });


var ITEM_PROVIDERS = [_item_service__WEBPACK_IMPORTED_MODULE_0__["ItemService"]];



/***/ }),

/***/ "../../libs/features/items/services/item.service.ts":
/*!********************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/items/services/item.service.ts ***!
  \********************************************************************************************/
/*! exports provided: ItemService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemService", function() { return ItemService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * This is organized in this module just for good housekeeping
 * However it is provided via the CoreModule to ensure it's
 * a singleton the entire app can use.
 * If your module service is *only* used in this module
 * you can have the service provided by this module.
 * In this case however, we want this service to be a true singleton
 * which can be injected into any component/service anywhere and
 * it will be the same instance therefore this is provided by the CoreModule.
 */
var ItemService = /** @class */ (function () {
    function ItemService() {
        this.items = new Array({ id: 1, name: 'Ter Stegen', role: 'Goalkeeper' }, { id: 3, name: 'Piqué', role: 'Defender' }, { id: 4, name: 'I. Rakitic', role: 'Midfielder' }, { id: 5, name: 'Sergio', role: 'Midfielder' }, { id: 6, name: 'Denis Suárez', role: 'Midfielder' }, { id: 7, name: 'Arda', role: 'Midfielder' }, { id: 8, name: 'A. Iniesta', role: 'Midfielder' }, { id: 9, name: 'Suárez', role: 'Forward' }, { id: 10, name: 'Messi', role: 'Forward' }, { id: 11, name: 'Neymar', role: 'Forward' }, { id: 12, name: 'Rafinha', role: 'Midfielder' }, { id: 13, name: 'Cillessen', role: 'Goalkeeper' }, { id: 14, name: 'Mascherano', role: 'Defender' }, { id: 17, name: 'Paco Alcácer', role: 'Forward' }, { id: 18, name: 'Jordi Alba', role: 'Defender' }, { id: 19, name: 'Digne', role: 'Defender' }, { id: 20, name: 'Sergi Roberto', role: 'Midfielder' }, { id: 21, name: 'André Gomes', role: 'Midfielder' }, { id: 22, name: 'Aleix Vidal', role: 'Midfielder' }, { id: 23, name: 'Umtiti', role: 'Defender' }, { id: 24, name: 'Mathieu', role: 'Defender' }, { id: 25, name: 'Masip', role: 'Goalkeeper' });
    }
    ItemService.prototype.getItems = function () {
        return this.items;
    };
    ItemService.prototype.getItem = function (id) {
        return this.items.filter(function (item) { return item.id === id; })[0];
    };
    ItemService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], ItemService);
    return ItemService;
}());



/***/ }),

/***/ "../../libs/features/ui/base/header.base-component.ts":
/*!**********************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/ui/base/header.base-component.ts ***!
  \**********************************************************************************************/
/*! exports provided: HeaderBaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderBaseComponent", function() { return HeaderBaseComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _malomatia_core_base__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @malomatia/core/base */ "../../libs/core/base/index.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// libs

var HeaderBaseComponent = /** @class */ (function (_super) {
    __extends(HeaderBaseComponent, _super);
    function HeaderBaseComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.tappedRight = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        return _this;
    }
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], HeaderBaseComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], HeaderBaseComponent.prototype, "rightButton", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], HeaderBaseComponent.prototype, "tappedRight", void 0);
    return HeaderBaseComponent;
}(_malomatia_core_base__WEBPACK_IMPORTED_MODULE_1__["BaseComponent"]));



/***/ }),

/***/ "../../libs/features/ui/base/index.ts":
/*!******************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/ui/base/index.ts ***!
  \******************************************************************************/
/*! exports provided: HeaderBaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _header_base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header.base-component */ "../../libs/features/ui/base/header.base-component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HeaderBaseComponent", function() { return _header_base_component__WEBPACK_IMPORTED_MODULE_0__["HeaderBaseComponent"]; });




/***/ }),

/***/ "../../libs/features/ui/index.ts":
/*!*************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/ui/index.ts ***!
  \*************************************************************************/
/*! exports provided: UISharedModule, HeaderBaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base */ "../../libs/features/ui/base/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HeaderBaseComponent", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["HeaderBaseComponent"]; });

/* harmony import */ var _ui_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ui.module */ "../../libs/features/ui/ui.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UISharedModule", function() { return _ui_module__WEBPACK_IMPORTED_MODULE_1__["UISharedModule"]; });





/***/ }),

/***/ "../../libs/features/ui/pipes/date-order.pipe.ts":
/*!*****************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/ui/pipes/date-order.pipe.ts ***!
  \*****************************************************************************************/
/*! exports provided: DateOrderPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateOrderPipe", function() { return DateOrderPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DateOrderPipe = /** @class */ (function () {
    function DateOrderPipe() {
    }
    DateOrderPipe.prototype.transform = function (value, sortBy) {
        if (value) {
            return value.sort(function (a, b) {
                if (!a[sortBy]) {
                    throw new Error("Incorrect orderByDate property");
                }
                var dateA = new Date(a[sortBy]).getTime();
                var dateB = new Date(b[sortBy]).getTime();
                return dateB - dateA;
            });
        }
    };
    DateOrderPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'orderByDate',
            pure: true
        })
    ], DateOrderPipe);
    return DateOrderPipe;
}());



/***/ }),

/***/ "../../libs/features/ui/pipes/index.ts":
/*!*******************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/ui/pipes/index.ts ***!
  \*******************************************************************************/
/*! exports provided: PIPES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PIPES", function() { return PIPES; });
/* harmony import */ var _date_order_pipe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./date-order.pipe */ "../../libs/features/ui/pipes/date-order.pipe.ts");

var PIPES = [_date_order_pipe__WEBPACK_IMPORTED_MODULE_0__["DateOrderPipe"]];


/***/ }),

/***/ "../../libs/features/ui/ui.module.ts":
/*!*****************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/features/ui/ui.module.ts ***!
  \*****************************************************************************/
/*! exports provided: UISharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UISharedModule", function() { return UISharedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngx-translate/core */ "../../node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _pipes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pipes */ "../../libs/features/ui/pipes/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MODULES = [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslateModule"]];
var UISharedModule = /** @class */ (function () {
    function UISharedModule() {
    }
    UISharedModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: MODULES.slice(),
            declarations: _pipes__WEBPACK_IMPORTED_MODULE_2__["PIPES"].slice(),
            exports: MODULES.concat(_pipes__WEBPACK_IMPORTED_MODULE_2__["PIPES"])
        })
    ], UISharedModule);
    return UISharedModule;
}());



/***/ }),

/***/ "../../libs/utils/angular.ts":
/*!*********************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/utils/angular.ts ***!
  \*********************************************************************/
/*! exports provided: throwIfAlreadyLoaded */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "throwIfAlreadyLoaded", function() { return throwIfAlreadyLoaded; });
function throwIfAlreadyLoaded(parentModule, moduleName) {
    if (parentModule) {
        throw new Error(moduleName + " has already been loaded. Import " + moduleName + " in the AppModule only.");
    }
}


/***/ }),

/***/ "../../libs/utils/index.ts":
/*!*******************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/utils/index.ts ***!
  \*******************************************************************/
/*! exports provided: throwIfAlreadyLoaded, isString, isObject, isIOS, isAndroid, isNativeScript, isElectron */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./angular */ "../../libs/utils/angular.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "throwIfAlreadyLoaded", function() { return _angular__WEBPACK_IMPORTED_MODULE_0__["throwIfAlreadyLoaded"]; });

/* harmony import */ var _objects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./objects */ "../../libs/utils/objects.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isString", function() { return _objects__WEBPACK_IMPORTED_MODULE_1__["isString"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isObject", function() { return _objects__WEBPACK_IMPORTED_MODULE_1__["isObject"]; });

/* harmony import */ var _platform__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./platform */ "../../libs/utils/platform.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isIOS", function() { return _platform__WEBPACK_IMPORTED_MODULE_2__["isIOS"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isAndroid", function() { return _platform__WEBPACK_IMPORTED_MODULE_2__["isAndroid"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isNativeScript", function() { return _platform__WEBPACK_IMPORTED_MODULE_2__["isNativeScript"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isElectron", function() { return _platform__WEBPACK_IMPORTED_MODULE_2__["isElectron"]; });






/***/ }),

/***/ "../../libs/utils/objects.ts":
/*!*********************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/utils/objects.ts ***!
  \*********************************************************************/
/*! exports provided: isString, isObject */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isString", function() { return isString; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isObject", function() { return isObject; });
var isString = function (arg) {
    return typeof arg === 'string';
};
var isObject = function (arg) {
    return arg && typeof arg === 'object';
};


/***/ }),

/***/ "../../libs/utils/platform.ts":
/*!**********************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/libs/utils/platform.ts ***!
  \**********************************************************************/
/*! exports provided: isIOS, isAndroid, isNativeScript, isElectron */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isIOS", function() { return isIOS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isAndroid", function() { return isAndroid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNativeScript", function() { return isNativeScript; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isElectron", function() { return isElectron; });
/**
 * NativeScript helpers
 */
/**
 * Determine if running on native iOS mobile app
 */
function isIOS() {
    return typeof NSObject !== 'undefined' && typeof NSString !== 'undefined';
}
/**
 * Determine if running on native Android mobile app
 */
function isAndroid() {
    return typeof android !== 'undefined' && typeof java !== 'undefined';
}
/**
 * Determine if running on native iOS or Android mobile app
 */
function isNativeScript() {
    return isIOS() || isAndroid();
}
/**
 * Electron helpers
 */
function isElectron() {
    return typeof window !== 'undefined' && window.process && window.process.type;
}


/***/ }),

/***/ "../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$":
/*!***************************************************************************************************************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ namespace object ***!
  \***************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./0owmtgfs.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/0owmtgfs.entry.js",
		"common",
		58
	],
	"./0owmtgfs.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/0owmtgfs.sc.entry.js",
		"common",
		59
	],
	"./0utrggve.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/0utrggve.entry.js",
		"common",
		60
	],
	"./0utrggve.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/0utrggve.sc.entry.js",
		"common",
		61
	],
	"./1kttiagf.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/1kttiagf.entry.js",
		0,
		"common",
		132
	],
	"./1kttiagf.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/1kttiagf.sc.entry.js",
		0,
		"common",
		133
	],
	"./2g1gy9f9.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/2g1gy9f9.entry.js",
		"common",
		10
	],
	"./2g1gy9f9.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/2g1gy9f9.sc.entry.js",
		"common",
		11
	],
	"./4m739wpj.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/4m739wpj.entry.js",
		"common",
		62
	],
	"./4m739wpj.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/4m739wpj.sc.entry.js",
		"common",
		63
	],
	"./5ey3bs99.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/5ey3bs99.entry.js",
		"common",
		12
	],
	"./5ey3bs99.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/5ey3bs99.sc.entry.js",
		"common",
		13
	],
	"./5qlzjcgv.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/5qlzjcgv.entry.js",
		0,
		"common",
		134
	],
	"./5qlzjcgv.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/5qlzjcgv.sc.entry.js",
		0,
		"common",
		135
	],
	"./5u5c8wcw.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/5u5c8wcw.entry.js",
		0,
		"common",
		136
	],
	"./5u5c8wcw.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/5u5c8wcw.sc.entry.js",
		0,
		"common",
		137
	],
	"./6eqoprbr.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/6eqoprbr.entry.js",
		0,
		"common",
		138
	],
	"./6eqoprbr.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/6eqoprbr.sc.entry.js",
		0,
		"common",
		139
	],
	"./7lrb3ory.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/7lrb3ory.entry.js",
		0,
		"common",
		114
	],
	"./7lrb3ory.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/7lrb3ory.sc.entry.js",
		0,
		"common",
		115
	],
	"./8q1e6dus.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/8q1e6dus.entry.js",
		"common",
		14
	],
	"./8q1e6dus.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/8q1e6dus.sc.entry.js",
		"common",
		15
	],
	"./9i747kfk.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/9i747kfk.entry.js",
		2,
		"common",
		140
	],
	"./9i747kfk.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/9i747kfk.sc.entry.js",
		2,
		"common",
		141
	],
	"./adaxrxoq.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/adaxrxoq.entry.js",
		"common",
		116
	],
	"./adaxrxoq.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/adaxrxoq.sc.entry.js",
		"common",
		117
	],
	"./afjpklm4.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/afjpklm4.entry.js",
		"common",
		64
	],
	"./afjpklm4.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/afjpklm4.sc.entry.js",
		"common",
		65
	],
	"./ao2edhxl.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/ao2edhxl.entry.js",
		"common",
		16
	],
	"./ao2edhxl.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/ao2edhxl.sc.entry.js",
		"common",
		17
	],
	"./av1nxhcg.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/av1nxhcg.entry.js",
		"common",
		18
	],
	"./av1nxhcg.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/av1nxhcg.sc.entry.js",
		"common",
		19
	],
	"./b9hbg5md.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/b9hbg5md.entry.js",
		"common",
		66
	],
	"./b9hbg5md.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/b9hbg5md.sc.entry.js",
		"common",
		67
	],
	"./bneiwm8s.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/bneiwm8s.entry.js",
		"common",
		20
	],
	"./bneiwm8s.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/bneiwm8s.sc.entry.js",
		"common",
		21
	],
	"./bvwzwweh.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/bvwzwweh.entry.js",
		"common",
		22
	],
	"./bvwzwweh.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/bvwzwweh.sc.entry.js",
		"common",
		23
	],
	"./bzgyi6uy.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/bzgyi6uy.entry.js",
		"common",
		24
	],
	"./bzgyi6uy.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/bzgyi6uy.sc.entry.js",
		"common",
		25
	],
	"./c2kiol1t.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/c2kiol1t.entry.js",
		"common",
		26
	],
	"./c2kiol1t.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/c2kiol1t.sc.entry.js",
		"common",
		27
	],
	"./c3nbktys.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/c3nbktys.entry.js",
		0,
		"common",
		142
	],
	"./c3nbktys.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/c3nbktys.sc.entry.js",
		0,
		"common",
		143
	],
	"./c3xilup3.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/c3xilup3.entry.js",
		"common",
		28
	],
	"./c3xilup3.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/c3xilup3.sc.entry.js",
		"common",
		29
	],
	"./ccuursht.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/ccuursht.entry.js",
		2,
		"common",
		144
	],
	"./ccuursht.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/ccuursht.sc.entry.js",
		2,
		"common",
		145
	],
	"./coytbtgb.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/coytbtgb.entry.js",
		"common",
		76
	],
	"./coytbtgb.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/coytbtgb.sc.entry.js",
		"common",
		77
	],
	"./dznymaqz.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/dznymaqz.entry.js",
		"common",
		78
	],
	"./dznymaqz.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/dznymaqz.sc.entry.js",
		"common",
		79
	],
	"./fazrg12f.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/fazrg12f.entry.js",
		0,
		"common",
		148
	],
	"./fazrg12f.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/fazrg12f.sc.entry.js",
		0,
		"common",
		149
	],
	"./fcbdrndu.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/fcbdrndu.entry.js",
		"common",
		80
	],
	"./fcbdrndu.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/fcbdrndu.sc.entry.js",
		"common",
		81
	],
	"./ffukzwt6.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/ffukzwt6.entry.js",
		"common",
		124
	],
	"./ffukzwt6.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/ffukzwt6.sc.entry.js",
		"common",
		125
	],
	"./fki3pb3x.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/fki3pb3x.entry.js",
		0,
		"common",
		150
	],
	"./fki3pb3x.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/fki3pb3x.sc.entry.js",
		0,
		"common",
		151
	],
	"./fokfxvfn.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/fokfxvfn.entry.js",
		"common",
		68
	],
	"./fokfxvfn.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/fokfxvfn.sc.entry.js",
		"common",
		69
	],
	"./gwiqb6zv.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/gwiqb6zv.entry.js",
		"common",
		30
	],
	"./gwiqb6zv.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/gwiqb6zv.sc.entry.js",
		"common",
		31
	],
	"./hs4xwlox.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/hs4xwlox.entry.js",
		"common",
		32
	],
	"./hs4xwlox.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/hs4xwlox.sc.entry.js",
		"common",
		33
	],
	"./jdcptvrs.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/jdcptvrs.entry.js",
		"common",
		82
	],
	"./jdcptvrs.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/jdcptvrs.sc.entry.js",
		"common",
		83
	],
	"./jpkvsu5y.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/jpkvsu5y.entry.js",
		"common",
		126
	],
	"./jpkvsu5y.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/jpkvsu5y.sc.entry.js",
		"common",
		127
	],
	"./jtkjzkgg.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/jtkjzkgg.entry.js",
		"common",
		34
	],
	"./jtkjzkgg.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/jtkjzkgg.sc.entry.js",
		"common",
		35
	],
	"./jwqvpjte.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/jwqvpjte.entry.js",
		"common",
		84
	],
	"./jwqvpjte.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/jwqvpjte.sc.entry.js",
		"common",
		85
	],
	"./l1m0sgjq.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/l1m0sgjq.entry.js",
		"common",
		86
	],
	"./l1m0sgjq.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/l1m0sgjq.sc.entry.js",
		"common",
		87
	],
	"./ly8zbpmk.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/ly8zbpmk.entry.js",
		"common",
		36
	],
	"./ly8zbpmk.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/ly8zbpmk.sc.entry.js",
		"common",
		37
	],
	"./mlczeont.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/mlczeont.entry.js",
		"common",
		70
	],
	"./mlczeont.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/mlczeont.sc.entry.js",
		"common",
		71
	],
	"./mnim3usc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/mnim3usc.entry.js",
		"common",
		38
	],
	"./mnim3usc.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/mnim3usc.sc.entry.js",
		"common",
		39
	],
	"./mxzbuihf.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/mxzbuihf.entry.js",
		0,
		"common",
		152
	],
	"./mxzbuihf.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/mxzbuihf.sc.entry.js",
		0,
		"common",
		153
	],
	"./n5wnzrch.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/n5wnzrch.entry.js",
		"common",
		72
	],
	"./n5wnzrch.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/n5wnzrch.sc.entry.js",
		"common",
		73
	],
	"./nr6wcehx.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/nr6wcehx.entry.js",
		"common",
		40
	],
	"./nr6wcehx.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/nr6wcehx.sc.entry.js",
		"common",
		41
	],
	"./o2g4txhh.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/o2g4txhh.entry.js",
		"common",
		88
	],
	"./o2g4txhh.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/o2g4txhh.sc.entry.js",
		"common",
		89
	],
	"./oboc8zd4.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/oboc8zd4.entry.js",
		"common",
		90
	],
	"./oboc8zd4.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/oboc8zd4.sc.entry.js",
		"common",
		91
	],
	"./od5ko9fd.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/od5ko9fd.entry.js",
		0,
		"common",
		118
	],
	"./od5ko9fd.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/od5ko9fd.sc.entry.js",
		0,
		"common",
		119
	],
	"./odqmlmdd.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/odqmlmdd.entry.js",
		"common",
		42
	],
	"./odqmlmdd.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/odqmlmdd.sc.entry.js",
		"common",
		43
	],
	"./okpgrmbb.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/okpgrmbb.entry.js",
		"common",
		44
	],
	"./okpgrmbb.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/okpgrmbb.sc.entry.js",
		"common",
		45
	],
	"./qvwswew4.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/qvwswew4.entry.js",
		"common",
		120
	],
	"./qvwswew4.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/qvwswew4.sc.entry.js",
		"common",
		121
	],
	"./rri2tloh.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/rri2tloh.entry.js",
		"common",
		92
	],
	"./rri2tloh.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/rri2tloh.sc.entry.js",
		"common",
		93
	],
	"./soeaphrm.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/soeaphrm.entry.js",
		0,
		"common",
		154
	],
	"./soeaphrm.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/soeaphrm.sc.entry.js",
		0,
		"common",
		155
	],
	"./sqd5wawk.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/sqd5wawk.entry.js",
		0,
		"common",
		156
	],
	"./sqd5wawk.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/sqd5wawk.sc.entry.js",
		0,
		"common",
		157
	],
	"./t547wlk7.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/t547wlk7.entry.js",
		"common",
		122
	],
	"./t547wlk7.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/t547wlk7.sc.entry.js",
		"common",
		123
	],
	"./tab7npn8.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/tab7npn8.entry.js",
		"common",
		94
	],
	"./tab7npn8.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/tab7npn8.sc.entry.js",
		"common",
		95
	],
	"./tj5rszjq.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/tj5rszjq.entry.js",
		"common",
		74
	],
	"./tj5rszjq.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/tj5rszjq.sc.entry.js",
		"common",
		75
	],
	"./tpeqvzxx.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/tpeqvzxx.entry.js",
		"common",
		128
	],
	"./tpeqvzxx.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/tpeqvzxx.sc.entry.js",
		"common",
		129
	],
	"./tqgphjq7.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/tqgphjq7.entry.js",
		"common",
		46
	],
	"./tqgphjq7.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/tqgphjq7.sc.entry.js",
		"common",
		47
	],
	"./tylmm2yl.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/tylmm2yl.entry.js",
		"common",
		96
	],
	"./tylmm2yl.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/tylmm2yl.sc.entry.js",
		"common",
		97
	],
	"./uegz8gm3.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/uegz8gm3.entry.js",
		"common",
		98
	],
	"./uegz8gm3.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/uegz8gm3.sc.entry.js",
		"common",
		99
	],
	"./uetn90ud.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/uetn90ud.entry.js",
		"common",
		100
	],
	"./uetn90ud.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/uetn90ud.sc.entry.js",
		"common",
		101
	],
	"./vbsrxypz.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/vbsrxypz.entry.js",
		158
	],
	"./vbsrxypz.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/vbsrxypz.sc.entry.js",
		159
	],
	"./wajpsmly.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/wajpsmly.entry.js",
		"common",
		48
	],
	"./wajpsmly.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/wajpsmly.sc.entry.js",
		"common",
		49
	],
	"./wdewkb4s.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/wdewkb4s.entry.js",
		0,
		"common",
		160
	],
	"./wdewkb4s.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/wdewkb4s.sc.entry.js",
		0,
		"common",
		161
	],
	"./wsfvc8rr.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/wsfvc8rr.entry.js",
		"common",
		50
	],
	"./wsfvc8rr.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/wsfvc8rr.sc.entry.js",
		"common",
		51
	],
	"./wvqczsjg.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/wvqczsjg.entry.js",
		"common",
		52
	],
	"./wvqczsjg.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/wvqczsjg.sc.entry.js",
		"common",
		53
	],
	"./x4ue4dpx.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/x4ue4dpx.entry.js",
		0,
		"common",
		162
	],
	"./x4ue4dpx.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/x4ue4dpx.sc.entry.js",
		0,
		"common",
		163
	],
	"./xfbndl84.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/xfbndl84.entry.js",
		"common",
		54
	],
	"./xfbndl84.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/xfbndl84.sc.entry.js",
		"common",
		55
	],
	"./xgnma4yj.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/xgnma4yj.entry.js",
		"common",
		102
	],
	"./xgnma4yj.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/xgnma4yj.sc.entry.js",
		"common",
		103
	],
	"./xrxaow8a.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/xrxaow8a.entry.js",
		"common",
		104
	],
	"./xrxaow8a.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/xrxaow8a.sc.entry.js",
		"common",
		105
	],
	"./ycyyhg01.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/ycyyhg01.entry.js",
		"common",
		106
	],
	"./ycyyhg01.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/ycyyhg01.sc.entry.js",
		"common",
		107
	],
	"./ye5age0r.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/ye5age0r.entry.js",
		164
	],
	"./ye5age0r.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/ye5age0r.sc.entry.js",
		165
	],
	"./yurs1umq.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/yurs1umq.entry.js",
		"common",
		108
	],
	"./yurs1umq.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/yurs1umq.sc.entry.js",
		"common",
		109
	],
	"./yz37im3e.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/yz37im3e.entry.js",
		"common",
		110
	],
	"./yz37im3e.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/yz37im3e.sc.entry.js",
		"common",
		111
	],
	"./z9nt6ntd.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/z9nt6ntd.entry.js",
		"common",
		130
	],
	"./z9nt6ntd.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/z9nt6ntd.sc.entry.js",
		"common",
		131
	],
	"./zdhyxh0f.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/zdhyxh0f.entry.js",
		"common",
		112
	],
	"./zdhyxh0f.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/zdhyxh0f.sc.entry.js",
		"common",
		113
	],
	"./zykaqnfi.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/zykaqnfi.entry.js",
		"common",
		56
	],
	"./zykaqnfi.sc.entry.js": [
		"../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build/zykaqnfi.sc.entry.js",
		"common",
		57
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "../../node_modules/@ionic/angular/node_modules/@ionic/core/dist/esm/es5/build lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "../../xplat/ionic/core/core.module.ts":
/*!*******************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/ionic/core/core.module.ts ***!
  \*******************************************************************************/
/*! exports provided: LibIonicCoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LibIonicCoreModule", function() { return LibIonicCoreModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "../../node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _malomatia_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @malomatia/utils */ "../../libs/utils/index.ts");
/* harmony import */ var _malomatia_web__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @malomatia/web */ "../../xplat/web/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var LibIonicCoreModule = /** @class */ (function () {
    function LibIonicCoreModule(parentModule) {
        Object(_malomatia_utils__WEBPACK_IMPORTED_MODULE_2__["throwIfAlreadyLoaded"])(parentModule, 'LibIonicCoreModule');
    }
    LibIonicCoreModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_malomatia_web__WEBPACK_IMPORTED_MODULE_3__["LibCoreModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"].forRoot()]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["SkipSelf"])()),
        __metadata("design:paramtypes", [LibIonicCoreModule])
    ], LibIonicCoreModule);
    return LibIonicCoreModule;
}());



/***/ }),

/***/ "../../xplat/ionic/core/index.ts":
/*!*************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/ionic/core/index.ts ***!
  \*************************************************************************/
/*! exports provided: LibIonicCoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core.module */ "../../xplat/ionic/core/core.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LibIonicCoreModule", function() { return _core_module__WEBPACK_IMPORTED_MODULE_0__["LibIonicCoreModule"]; });




/***/ }),

/***/ "../../xplat/ionic/features/index.ts":
/*!*****************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/ionic/features/index.ts ***!
  \*****************************************************************************/
/*! exports provided: UIModule, UI_COMPONENTS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ui__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ui */ "../../xplat/ionic/features/ui/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UIModule", function() { return _ui__WEBPACK_IMPORTED_MODULE_0__["UIModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UI_COMPONENTS", function() { return _ui__WEBPACK_IMPORTED_MODULE_0__["UI_COMPONENTS"]; });




/***/ }),

/***/ "../../xplat/ionic/features/ui/components/header/header.component.html":
/*!***************************************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/ionic/features/ui/components/header/header.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title> {{ title }} </ion-title>\n  </ion-toolbar>\n</ion-header>\n"

/***/ }),

/***/ "../../xplat/ionic/features/ui/components/header/header.component.ts":
/*!*************************************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/ionic/features/ui/components/header/header.component.ts ***!
  \*************************************************************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _malomatia_features__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @malomatia/features */ "../../libs/features/index.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var HeaderComponent = /** @class */ (function (_super) {
    __extends(HeaderComponent, _super);
    function HeaderComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'lib-ion-header',
            template: __webpack_require__(/*! ./header.component.html */ "../../xplat/ionic/features/ui/components/header/header.component.html")
        })
    ], HeaderComponent);
    return HeaderComponent;
}(_malomatia_features__WEBPACK_IMPORTED_MODULE_1__["HeaderBaseComponent"]));



/***/ }),

/***/ "../../xplat/ionic/features/ui/components/index.ts":
/*!*******************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/ionic/features/ui/components/index.ts ***!
  \*******************************************************************************************/
/*! exports provided: UI_COMPONENTS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UI_COMPONENTS", function() { return UI_COMPONENTS; });
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header/header.component */ "../../xplat/ionic/features/ui/components/header/header.component.ts");

var UI_COMPONENTS = [_header_header_component__WEBPACK_IMPORTED_MODULE_0__["HeaderComponent"]];


/***/ }),

/***/ "../../xplat/ionic/features/ui/index.ts":
/*!********************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/ionic/features/ui/index.ts ***!
  \********************************************************************************/
/*! exports provided: UIModule, UI_COMPONENTS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components */ "../../xplat/ionic/features/ui/components/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UI_COMPONENTS", function() { return _components__WEBPACK_IMPORTED_MODULE_0__["UI_COMPONENTS"]; });

/* harmony import */ var _ui_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ui.module */ "../../xplat/ionic/features/ui/ui.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UIModule", function() { return _ui_module__WEBPACK_IMPORTED_MODULE_1__["UIModule"]; });





/***/ }),

/***/ "../../xplat/ionic/features/ui/ui.module.ts":
/*!************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/ionic/features/ui/ui.module.ts ***!
  \************************************************************************************/
/*! exports provided: UIModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UIModule", function() { return UIModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "../../node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _malomatia_web__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @malomatia/web */ "../../xplat/web/index.ts");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components */ "../../xplat/ionic/features/ui/components/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MODULES = [_malomatia_web__WEBPACK_IMPORTED_MODULE_2__["UIModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"]];
var UIModule = /** @class */ (function () {
    function UIModule() {
    }
    UIModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: MODULES.slice(),
            declarations: _components__WEBPACK_IMPORTED_MODULE_3__["UI_COMPONENTS"].slice(),
            exports: MODULES.concat(_components__WEBPACK_IMPORTED_MODULE_3__["UI_COMPONENTS"]),
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], UIModule);
    return UIModule;
}());



/***/ }),

/***/ "../../xplat/ionic/index.ts":
/*!********************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/ionic/index.ts ***!
  \********************************************************************/
/*! exports provided: LibIonicCoreModule, UIModule, UI_COMPONENTS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core */ "../../xplat/ionic/core/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LibIonicCoreModule", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["LibIonicCoreModule"]; });

/* harmony import */ var _features__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./features */ "../../xplat/ionic/features/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UIModule", function() { return _features__WEBPACK_IMPORTED_MODULE_1__["UIModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UI_COMPONENTS", function() { return _features__WEBPACK_IMPORTED_MODULE_1__["UI_COMPONENTS"]; });





/***/ }),

/***/ "../../xplat/web/core/base/app.base-component.ts":
/*!*****************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/core/base/app.base-component.ts ***!
  \*****************************************************************************************/
/*! exports provided: AppBaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppBaseComponent", function() { return AppBaseComponent; });
/* harmony import */ var _malomatia_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @malomatia/core */ "../../libs/core/index.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// libs

var AppBaseComponent = /** @class */ (function (_super) {
    __extends(AppBaseComponent, _super);
    function AppBaseComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AppBaseComponent;
}(_malomatia_core__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]));



/***/ }),

/***/ "../../xplat/web/core/base/index.ts":
/*!****************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/core/base/index.ts ***!
  \****************************************************************************/
/*! exports provided: AppBaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _app_base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.base-component */ "../../xplat/web/core/base/app.base-component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AppBaseComponent", function() { return _app_base_component__WEBPACK_IMPORTED_MODULE_0__["AppBaseComponent"]; });




/***/ }),

/***/ "../../xplat/web/core/core.module.ts":
/*!*****************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/core/core.module.ts ***!
  \*****************************************************************************/
/*! exports provided: winFactory, platformLangFactory, createTranslateLoader, LibCoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "winFactory", function() { return winFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "platformLangFactory", function() { return platformLangFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createTranslateLoader", function() { return createTranslateLoader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LibCoreModule", function() { return LibCoreModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "../../node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "../../node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/http-loader */ "../../node_modules/@ngx-translate/http-loader/fesm5/ngx-translate-http-loader.js");
/* harmony import */ var _malomatia_utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @malomatia/utils */ "../../libs/utils/index.ts");
/* harmony import */ var _malomatia_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @malomatia/core */ "../../libs/core/index.ts");
/* harmony import */ var _malomatia_features__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @malomatia/features */ "../../libs/features/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



// libs





// bring in custom web services here...
// factories
function winFactory() {
    return window;
}
function platformLangFactory() {
    var browserLang = window.navigator.language || 'en'; // fallback English
    // browser language has 2 codes, ex: 'en-US'
    return browserLang.split('-')[0];
}
function createTranslateLoader(http) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_4__["TranslateHttpLoader"](http, "./assets/i18n/", '.json');
}
var LibCoreModule = /** @class */ (function () {
    function LibCoreModule(parentModule) {
        Object(_malomatia_utils__WEBPACK_IMPORTED_MODULE_5__["throwIfAlreadyLoaded"])(parentModule, 'LibCoreModule');
    }
    LibCoreModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _malomatia_core__WEBPACK_IMPORTED_MODULE_6__["CoreModule"].forRoot([
                    {
                        provide: _malomatia_core__WEBPACK_IMPORTED_MODULE_6__["PlatformLanguageToken"],
                        useFactory: platformLangFactory
                    },
                    {
                        provide: _malomatia_core__WEBPACK_IMPORTED_MODULE_6__["PlatformWindowToken"],
                        useFactory: winFactory
                    }
                ]),
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateLoader"],
                        useFactory: createTranslateLoader,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]]
                    }
                })
            ],
            providers: _malomatia_features__WEBPACK_IMPORTED_MODULE_7__["ITEM_PROVIDERS"].slice()
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["SkipSelf"])()),
        __metadata("design:paramtypes", [LibCoreModule])
    ], LibCoreModule);
    return LibCoreModule;
}());



/***/ }),

/***/ "../../xplat/web/core/index.ts":
/*!***********************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/core/index.ts ***!
  \***********************************************************************/
/*! exports provided: LibCoreModule, AppBaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base */ "../../xplat/web/core/base/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AppBaseComponent", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["AppBaseComponent"]; });

/* harmony import */ var _core_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./core.module */ "../../xplat/web/core/core.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LibCoreModule", function() { return _core_module__WEBPACK_IMPORTED_MODULE_1__["LibCoreModule"]; });





/***/ }),

/***/ "../../xplat/web/features/index.ts":
/*!***************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/features/index.ts ***!
  \***************************************************************************/
/*! exports provided: ItemsModule, UIModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _items__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./items */ "../../xplat/web/features/items/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemsModule", function() { return _items__WEBPACK_IMPORTED_MODULE_0__["ItemsModule"]; });

/* harmony import */ var _ui__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ui */ "../../xplat/web/features/ui/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UIModule", function() { return _ui__WEBPACK_IMPORTED_MODULE_1__["UIModule"]; });





/***/ }),

/***/ "../../xplat/web/features/items/components/index.ts":
/*!********************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/features/items/components/index.ts ***!
  \********************************************************************************************/
/*! exports provided: ITEM_COMPONENTS, ItemDetailComponent, ItemsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ITEM_COMPONENTS", function() { return ITEM_COMPONENTS; });
/* harmony import */ var _item_detail_item_detail_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./item-detail/item-detail.component */ "../../xplat/web/features/items/components/item-detail/item-detail.component.ts");
/* harmony import */ var _items_items_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./items/items.component */ "../../xplat/web/features/items/components/items/items.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemDetailComponent", function() { return _item_detail_item_detail_component__WEBPACK_IMPORTED_MODULE_0__["ItemDetailComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemsComponent", function() { return _items_items_component__WEBPACK_IMPORTED_MODULE_1__["ItemsComponent"]; });



var ITEM_COMPONENTS = [_item_detail_item_detail_component__WEBPACK_IMPORTED_MODULE_0__["ItemDetailComponent"], _items_items_component__WEBPACK_IMPORTED_MODULE_1__["ItemsComponent"]];




/***/ }),

/***/ "../../xplat/web/features/items/components/item-detail/item-detail.component.html":
/*!**************************************************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/features/items/components/item-detail/item-detail.component.html ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"p-x-20\">\n  <p>An example detail view using Angular routing...</p>\n  <h1>{{ item.name }}</h1>\n  <p>{{ item.role }}</p>\n</div>\n"

/***/ }),

/***/ "../../xplat/web/features/items/components/item-detail/item-detail.component.ts":
/*!************************************************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/features/items/components/item-detail/item-detail.component.ts ***!
  \************************************************************************************************************************/
/*! exports provided: ItemDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemDetailComponent", function() { return ItemDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _malomatia_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @malomatia/core */ "../../libs/core/index.ts");
/* harmony import */ var _malomatia_features__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @malomatia/features */ "../../libs/features/index.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// libs


var ItemDetailComponent = /** @class */ (function (_super) {
    __extends(ItemDetailComponent, _super);
    function ItemDetailComponent(log, itemService, route) {
        return _super.call(this, log, itemService, route) || this;
    }
    ItemDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'lib-item-detail',
            template: __webpack_require__(/*! ./item-detail.component.html */ "../../xplat/web/features/items/components/item-detail/item-detail.component.html")
        }),
        __metadata("design:paramtypes", [_malomatia_core__WEBPACK_IMPORTED_MODULE_2__["LogService"],
            _malomatia_features__WEBPACK_IMPORTED_MODULE_3__["ItemService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], ItemDetailComponent);
    return ItemDetailComponent;
}(_malomatia_features__WEBPACK_IMPORTED_MODULE_3__["ItemDetailBaseComponent"]));



/***/ }),

/***/ "../../xplat/web/features/items/components/items/items.component.html":
/*!**************************************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/features/items/components/items/items.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"p-x-20\">\n  <div style=\"text-align:center\">\n    <h2>Welcome to an Angular CLI app built with Nrwl Nx and xplat!</h2>\n    <img width=\"100\" src=\"assets/nx-logo.png\" />\n    <span style=\"position: relative;top: -28px;margin: 10px;\">+</span>\n    <img width=\"120\" src=\"assets/xplat.png\" />\n  </div>\n\n  <h2>Nx</h2>\n\n  An open source toolkit for enterprise Angular applications. Nx is designed to\n  help you create and build enterprise grade Angular applications. It provides\n  an opinionated approach to application project structure and patterns.\n\n  <h3>Quick Start & Documentation</h3>\n\n  <a href=\"https://nrwl.io/nx\"\n    >Watch a 5-minute video on how to get started with Nx.</a\n  >\n\n  <h1>{{ 'hello' | translate }}</h1>\n  <h3>Try things out</h3>\n\n  <a href=\"https://nstudio.io/xplat\">Learn more about xplat.</a>\n\n  <ul>\n    <li *ngFor=\"let item of items\">\n      <a [routerLink]=\"['/', item.id]\">{{ item.name }}</a>\n    </li>\n  </ul>\n</div>\n"

/***/ }),

/***/ "../../xplat/web/features/items/components/items/items.component.ts":
/*!************************************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/features/items/components/items/items.component.ts ***!
  \************************************************************************************************************/
/*! exports provided: ItemsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemsComponent", function() { return ItemsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _malomatia_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @malomatia/core */ "../../libs/core/index.ts");
/* harmony import */ var _malomatia_features__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @malomatia/features */ "../../libs/features/index.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// libs


var ItemsComponent = /** @class */ (function (_super) {
    __extends(ItemsComponent, _super);
    function ItemsComponent(log, itemService) {
        return _super.call(this, log, itemService) || this;
    }
    ItemsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'lib-items',
            template: __webpack_require__(/*! ./items.component.html */ "../../xplat/web/features/items/components/items/items.component.html")
        }),
        __metadata("design:paramtypes", [_malomatia_core__WEBPACK_IMPORTED_MODULE_1__["LogService"], _malomatia_features__WEBPACK_IMPORTED_MODULE_2__["ItemService"]])
    ], ItemsComponent);
    return ItemsComponent;
}(_malomatia_features__WEBPACK_IMPORTED_MODULE_2__["ItemsBaseComponent"]));



/***/ }),

/***/ "../../xplat/web/features/items/index.ts":
/*!*********************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/features/items/index.ts ***!
  \*********************************************************************************/
/*! exports provided: ItemsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _items_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./items.module */ "../../xplat/web/features/items/items.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemsModule", function() { return _items_module__WEBPACK_IMPORTED_MODULE_0__["ItemsModule"]; });




/***/ }),

/***/ "../../xplat/web/features/items/items.module.ts":
/*!****************************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/features/items/items.module.ts ***!
  \****************************************************************************************/
/*! exports provided: ItemsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemsModule", function() { return ItemsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _malomatia_features__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @malomatia/features */ "../../libs/features/index.ts");
/* harmony import */ var _ui_ui_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ui/ui.module */ "../../xplat/web/features/ui/ui.module.ts");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components */ "../../xplat/web/features/items/components/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


// libs


// app

var ItemsModule = /** @class */ (function () {
    function ItemsModule() {
    }
    ItemsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _ui_ui_module__WEBPACK_IMPORTED_MODULE_3__["UIModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(Object(_malomatia_features__WEBPACK_IMPORTED_MODULE_2__["routeItems"])(_components__WEBPACK_IMPORTED_MODULE_4__["ItemsComponent"], _components__WEBPACK_IMPORTED_MODULE_4__["ItemDetailComponent"]))
            ],
            declarations: _components__WEBPACK_IMPORTED_MODULE_4__["ITEM_COMPONENTS"].slice(),
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["NO_ERRORS_SCHEMA"]]
        })
    ], ItemsModule);
    return ItemsModule;
}());



/***/ }),

/***/ "../../xplat/web/features/ui/index.ts":
/*!******************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/features/ui/index.ts ***!
  \******************************************************************************/
/*! exports provided: UIModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ui_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ui.module */ "../../xplat/web/features/ui/ui.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UIModule", function() { return _ui_module__WEBPACK_IMPORTED_MODULE_0__["UIModule"]; });




/***/ }),

/***/ "../../xplat/web/features/ui/ui.module.ts":
/*!**********************************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/features/ui/ui.module.ts ***!
  \**********************************************************************************/
/*! exports provided: UIModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UIModule", function() { return UIModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "../../node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "../../node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _malomatia_features__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @malomatia/features */ "../../libs/features/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// libs

var MODULES = [
    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
    _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
    _malomatia_features__WEBPACK_IMPORTED_MODULE_4__["UISharedModule"]
];
var UIModule = /** @class */ (function () {
    function UIModule() {
    }
    UIModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: MODULES.slice(),
            exports: MODULES.slice()
        })
    ], UIModule);
    return UIModule;
}());



/***/ }),

/***/ "../../xplat/web/index.ts":
/*!******************************************************************!*\
  !*** /Users/anonymous/Desktop/Work/malomatia/xplat/web/index.ts ***!
  \******************************************************************/
/*! exports provided: LibCoreModule, ItemsModule, UIModule, AppBaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core */ "../../xplat/web/core/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LibCoreModule", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["LibCoreModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AppBaseComponent", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["AppBaseComponent"]; });

/* harmony import */ var _features__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./features */ "../../xplat/web/features/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ItemsModule", function() { return _features__WEBPACK_IMPORTED_MODULE_1__["ItemsModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UIModule", function() { return _features__WEBPACK_IMPORTED_MODULE_1__["UIModule"]; });





/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./pages/home/home.module": [
		"./src/app/pages/home/home.module.ts",
		"pages-home-home-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: routes, AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', loadChildren: './pages/home/home.module#HomeModule' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-app> <ion-router-outlet></ion-router-outlet> </ion-app>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "../../node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StatusBar = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"].StatusBar;
var AppComponent = /** @class */ (function () {
    function AppComponent(platform) {
        this.platform = platform;
        this.initializeApp();
    }
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            if (_this.platform.is('capacitor')) {
                StatusBar.setStyle({
                    style: _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["StatusBarStyle"].Dark
                });
            }
        });
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'lib-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html")
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "../../node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _features_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./features/shared/shared.module */ "./src/app/features/shared/shared.module.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_core_core_module__WEBPACK_IMPORTED_MODULE_3__["CoreModule"], _features_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"]],
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]],
            providers: [{ provide: _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicRouteStrategy"] }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/core/core.module.ts":
/*!*************************************!*\
  !*** ./src/app/core/core.module.ts ***!
  \*************************************/
/*! exports provided: CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _malomatia_ionic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @malomatia/ionic */ "../../xplat/ionic/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// libs

var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    CoreModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_malomatia_ionic__WEBPACK_IMPORTED_MODULE_1__["LibIonicCoreModule"]]
        })
    ], CoreModule);
    return CoreModule;
}());



/***/ }),

/***/ "./src/app/features/shared/shared.module.ts":
/*!**************************************************!*\
  !*** ./src/app/features/shared/shared.module.ts ***!
  \**************************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _malomatia_ionic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @malomatia/ionic */ "../../xplat/ionic/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// xplat

var MODULES = [_malomatia_ionic__WEBPACK_IMPORTED_MODULE_1__["UIModule"]];
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: MODULES.slice(),
            exports: MODULES.slice()
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "../../node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])()
    .bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/anonymous/Desktop/Work/malomatia/apps/gama-crm-ionic/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map