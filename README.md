# Malomatia

This project was originally made by [Ahmed Saleh](ahsaleh@malomatia.com) to deal Malomatia projects as a monorepo as a solution of sharing most of the code between projects.

## Quick Start & Documentation
Take a look at scripts property in the root package.json

## Generate your first application

Run `ng generate app myapp` to generate an application.

## Development server

Run `ng serve --project=myapp` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name --project=myapp` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build --project=myapp` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Cypress](http://www.cypress.io/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

To get more help regarding the project structure or source code, feal free to [contact me](ahsaleh@malomatia.com).
